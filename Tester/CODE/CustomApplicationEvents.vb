﻿Namespace My

    Partial Friend Class MyApplication

        ''' <summary>
        ''' Sets the broadcast level.
        ''' </summary>
    ''' <param name="value">The value.</param>
        Public Shared Sub BroadcastLevelSetter(ByVal value As TraceEventType)
            My.Settings.BroadcastLevel = value
        End Sub

        ''' <summary>Builds the default caption.</summary>
        Friend Function BuildDefaultCaption() As String

            Dim suffix As New System.Text.StringBuilder
            suffix.Append(" ")
            Return ApplicationInfo.BuildDefaultCaption(suffix.ToString)

        End Function

        ''' <summary>
        ''' Destroys objects for this project.
        ''' </summary>
    Friend Sub Destroy()
            MySplashScreen.Close()
            MySplashScreen.Dispose()
            Me.SplashScreen = Nothing
        End Sub

        ''' <summary>Instantiates the application to its known state.
        ''' </summary>

    ''' <returns>True if success or false if requesting to terminate.</returns>
            <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")>
        Private Function tryInitializeKnownState() As Boolean

            Dim passed As Boolean

            Try

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting

                ' show status
                If Debugger.IsAttached Then
                    MySplashScreen.LogSplashMessage(TraceEventType.Verbose, "APPLICATION IS INITIALIZING. DESIGN MODE.")
                Else
                    MySplashScreen.LogSplashMessage(TraceEventType.Verbose, "APPLICATION IS INITIALIZING. RUNTIME MODE.")
                End If

                'SplashScreen1.LogSplashMessage(TraceEventType.Verbose, "Initializing Common Library")
                'Common.My.MyLibrary.Default.InitializeKnownState(Debugger.IsAttached)

                'SplashScreen1.LogSplashMessage(TraceEventType.Verbose, "Allowing library use of splash screen")
                ' Library.SplashScreen1.SplashScreen = SplashScreen1.SplashScreen

                passed = True
                Return passed

            Catch ex As Exception

                ' Turn off the hourglass
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

                passed = False
                Throw

            Finally

                ' Turn off the hourglass
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

                If Not passed Then
                    Try
                        Me.Destroy()
                    Finally
                    End Try
                End If

            End Try

        End Function

        ''' <summary>
        ''' Processes the shut down.
        ''' </summary>
    Private Sub processShutDown()

            ' flush all message logs
            My.Application.Log.DefaultFileLogWriter.Flush()

            If My.Application.SaveMySettingsOnExit Then

                ' TesterInfoPublisher.Default.SaveSettings()

            End If

            ' dispose of all libraries
            ' Legacy.GpibBoardManager.DisposeBoard()

        End Sub

        ''' <summary>
        ''' Processes the startup.
        ''' Sets teh event arguments <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs.Cancel">cancel</see> value if failed.
        ''' </summary>
    ''' <param name="e">The <see cref="Microsoft.VisualBasic.ApplicationServices.StartupEventArgs" /> instance containing the event data.</param>
        Private Sub ProcessStartup(ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupEventArgs)

            e.Cancel = False
            If Not e.Cancel Then
                MySplashScreen.LogSplashMessage(TraceEventType.Information, "PARSING COMMAND LINE")
                e.Cancel = Not CommandLineInfo.TryParseCommandLine(e.CommandLine)
            End If
            'If not e.Cancel  Then
            'SplashScreen1.LogSplashMessage(TraceEventType.Information, "Reading settings")
            'passed = passed AndAlso TesterInfoPublisher.Default.readSettings
            'End If
            If Not e.Cancel Then
                MySplashScreen.LogSplashMessage(TraceEventType.Information, "INITIALIZING KNOWN STATE")
                e.Cancel = Not Me.tryInitializeKnownState()
            End If

        End Sub

        ''' <summary>
        ''' Sets the trace level.
        ''' </summary>
    ''' <param name="value">The value.</param>
        Public Shared Sub TraceLevelSetter(ByVal value As TraceEventType)
            My.Settings.TraceLevel = value
        End Sub

        ''' <summary>
        ''' Sets the visual styles, text display styles, and current principal for the main application thread (if the application uses Windows authentication), and initializes the splash screen, if defined.
        ''' Replaces the default trace listener with the modified listener.
        ''' Updates the minimum splash screen display time.
        ''' </summary>
    ''' <param name="commandLineArgs">A <see cref="T:System.Collections.ObjectModel.ReadOnlyCollection`1" /> of String, containing the command-line arguments as strings for the current application.</param><returns>
        ''' A <see cref="T:System.Boolean" /> indicating if application startup should continue.
        ''' </returns>
        Protected Overrides Function OnInitialize(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            TraceLogInfo.ReplaceDefaultTraceListener()
            BroadcastInfo.BroadcastLevel = My.Settings.BroadcastLevel

            ' Set the display time to value from the settings class.
            Me.MinimumSplashScreenDisplayTime = My.Settings.MinimumSplashScreenDisplayMilliseconds

            Return MyBase.OnInitialize(commandLineArgs)

        End Function

    End Class

End Namespace

