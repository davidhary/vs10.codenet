''' <summary>Includes code for Switchboard Form, which serves as a switchboard for this program.</summary>
''' <license>
''' (c) 2008 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="02/11/08" by="David" revision="2.0.2963.x">
''' Created
''' </history>
Public Class Switchboard

#Region " CONSTRUCTORS  and  DESTRUCTORS "

  Public Sub New()
    MyBase.New()

    ' Initialize user components that might be affected by resize or paint actions

    ' This method is required by the Windows Form Designer.
    InitializeComponent()

    ' Add any initialization after the InitializeComponent() call

  End Sub

#Region " Unused "
#If False Then
  ''' <summary>Cleans up managed components.</summary>
  ''' <remarks>Use this method to reclaim managed resources used by this class.</remarks>
  Private Sub onDisposeManagedResources()

  End Sub

  ''' <summary>Cleans up unmanaged components.</summary>
  ''' <remarks>Use this method to reclaim unmanaged resources used by this class.</remarks>
  Private Sub onDisposeUnmanagedResources()

  End Sub

#End If
#End Region

#End Region

#Region " FORM EVENT HANDLERS "

  ''' <summary>Occurs after the form is closed.</summary>
  ''' <param name="sender"><see cref="System.Object"/> instance of this 
  '''   <see cref="System.Windows.Forms.Form"/></param>
  ''' <param name="e"><see cref="System.EventArgs"/></param>
  ''' <remarks>This event is a notification that the form has already gone away before
  ''' control is returned to the calling method (in case of a modal form).  Use this
  ''' method to delete any temporary files that were created or dispose of any objects
  ''' not disposed with the closing event.
  ''' </remarks>
    Private Sub form_Closed(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Closed

    End Sub

  ''' <summary>Occurs before the form is closed</summary>
  ''' <param name="sender"><see cref="System.Object"/> instance of this 
  '''   <see cref="System.Windows.Forms.Form"/></param>
  ''' <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
  ''' <remarks>Use this method to optionally cancel the closing of the form.
  ''' Because the form is not yet closed at this point, this is also the best 
  ''' place to serialize a form's visible properties, such as size and 
  ''' location. Finally, dispose of any form level objects especially those that
  ''' might needs access to the form and thus should not be terminated after the
  ''' form closed.
  ''' </remarks>
    Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        ' disable the timer if any
        ' actionTimer.Enabled = False
        My.Application.DoEvents()

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            ' terminate form-level objects
            ' Me.terminateObjects()
        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

  ''' <summary>Occurs when the form is loaded.</summary>
  ''' <param name="sender"><see cref="System.Object"/> instance of this 
  '''   <see cref="System.Windows.Forms.Form"/></param>
  ''' <param name="e"><see cref="System.EventArgs"/></param>
  ''' <remarks>Use this method for doing any final initialization right before 
  '''   the form is shown.  This is a good place to change the Visible and
  '''   ShowInTaskbar properties to start the form as hidden.  
  '''   Starting a form as hidden is useful for forms that need to be running but that
  '''   should not show themselves right away, such as forms with a notify icon in the
  '''   task bar.</remarks>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects
            Me.instantiateObjects()

            ' center the form
            Me.CenterToScreen()

            ' turn on the loaded flag
            '      loaded = True

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

  ''' <summary>
  ''' Does all the post processing after all the form controls are rendered as the user expects them.
  ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> 
  Private Sub form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

    Windows.Forms.Application.DoEvents()

    Try

      Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

      Me.Text = ApplicationInfo.BuildDefaultCaption(": SWITCH BOARD")

      ' initializeUserInterface()

      Windows.Forms.Application.DoEvents()

    Catch ex As Exception

      ExceptionDisplay.ProcessException(ex, "Exception occurred showing the form")

    Finally

      Me.Cursor = System.Windows.Forms.Cursors.Default

    End Try

  End Sub

#End Region

#Region " PRIVATE  and  PROTECTED "

  ''' <summary>Enumerates the action options.</summary>
  Private Enum ActionOption
    <System.ComponentModel.Description("Single Code Net Device")> SingleCodeNet
    <System.ComponentModel.Description("Dual Code Net Device")> DualCodeNet
  End Enum

  ''' <summary>Initializes the class objects.</summary>
  ''' <remarks>Called from the form load method to instantiate 
  '''   module-level objects.</remarks>
  Private Sub instantiateObjects()

    ' populate the action list
    Me.populateActiveList()

  End Sub

  ''' <summary>Populates the list of options in the action combo box.</summary>
  ''' <remarks>It seems that out enumerated list does not work very well with
  '''   this list.</remarks>
  ''' <history>
  ''' 	[david] 	11/8/2004	Created
  ''' </history>
  Private Sub populateActiveList()

    ' set the action list
    Me.applicationsListBox.Items.Clear()
    Me.applicationsListBox.DataSource = isr.Core.EnumExtensions.ValueDescriptionPairs(GetType(ActionOption))
    Me.applicationsListBox.ValueMember = "Key"
    Me.applicationsListBox.DisplayMember = "Value"

  End Sub

  ''' <summary>
  ''' Gets the selected action.
  ''' </summary>
      Private ReadOnly Property selectedAction() As ActionOption
    Get
      Return CType(CType(Me.applicationsListBox.SelectedItem, System.Collections.Generic.KeyValuePair(Of [Enum], String)).Key, ActionOption)
    End Get
  End Property

#Region " UNUSED "
#If False Then
  ''' <summary>Initializes the user interface and tool tips.</summary>
  ''' <remarks>Call this method from the form load method to set the user interface.</remarks>
  Private Sub initializeUserInterface()
    '    tipsToolTip.SetToolTip(Me.txtDuration, "Enter count-down duration in seconds")
    '    tipsToolTip.SetToolTip(Me.exitButton, "Click to exit")
  End Sub

  ''' <summary>Terminates and disposes of class-level objects.</summary>
  ''' <remarks>Called from the form Closing method.</remarks>
  Private Sub terminateObjects()
  End Sub

#End If
#End Region

#End Region

#Region " CONTROL EVENT HANDLERS "

  ''' <summary>Closes the form and exits the application.</summary>
    Private Sub exitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles exitButton.Click

        ' validate controls manually.
        If Not isr.Core.ControlExtensions.ValidateControls(Me) Then
            ' indicate that the modal dialog is still running
            Me.DialogResult = DialogResult.None
        Else
            Me.Close()
        End If

    End Sub

  ''' <summary>Open selected items.</summary>
  Private Sub openButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles openButton.Click
    Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
    Select Case Me.selectedAction
      Case ActionOption.SingleCodeNet
        Dim codeNet As New isr.CodeNet.CodeNetDevice()
        codeNet.ShowPortPanel(Me)
      Case ActionOption.DualCodeNet
        Dim codeNet1 As New isr.CodeNet.CodeNetDevice()
        codeNet1.ShowPortPanel(Me)
        Dim codeNet2 As New isr.CodeNet.CodeNetDevice()
        codeNet2.ShowPortPanel(Me)
    End Select
    Me.Cursor = System.Windows.Forms.Cursors.Default
  End Sub

#End Region

End Class