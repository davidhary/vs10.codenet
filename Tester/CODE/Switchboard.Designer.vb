<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class Switchboard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> 
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> 
    Private Sub InitializeComponent()
    Me.components = New System.ComponentModel.Container
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Switchboard))
    Me.applicationsListBoxLabel = New System.Windows.Forms.Label
    Me.openButton = New System.Windows.Forms.Button
    Me.exitButton = New System.Windows.Forms.Button
    Me.applicationsListBox = New System.Windows.Forms.ListBox
    Me.tipsTooltip = New System.Windows.Forms.ToolTip(Me.components)
    Me.SuspendLayout()
    '
    'applicationsListBoxLabel
    '
    Me.applicationsListBoxLabel.Location = New System.Drawing.Point(13, 9)
    Me.applicationsListBoxLabel.Name = "applicationsListBoxLabel"
    Me.applicationsListBoxLabel.Size = New System.Drawing.Size(296, 16)
    Me.applicationsListBoxLabel.TabIndex = 15
    Me.applicationsListBoxLabel.Text = "Select an item from the list and click Open"
    '
    'openButton
    '
    Me.openButton.Location = New System.Drawing.Point(355, 29)
    Me.openButton.Name = "openButton"
    Me.openButton.Size = New System.Drawing.Size(75, 23)
    Me.openButton.TabIndex = 13
    Me.openButton.Text = "&Open..."
    '
    'exitButton
    '
    Me.exitButton.Location = New System.Drawing.Point(355, 126)
    Me.exitButton.Name = "exitButton"
    Me.exitButton.Size = New System.Drawing.Size(75, 23)
    Me.exitButton.TabIndex = 12
    Me.exitButton.Text = "E&xit"
    '
    'applicationsListBox
    '
    Me.applicationsListBox.Location = New System.Drawing.Point(11, 29)
    Me.applicationsListBox.Name = "applicationsListBox"
    Me.applicationsListBox.Size = New System.Drawing.Size(328, 121)
    Me.applicationsListBox.TabIndex = 14
    Me.tipsTooltip.SetToolTip(Me.applicationsListBox, "Select an application to test")
    '
    'Switchboard
    '
    Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
    Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
    Me.ClientSize = New System.Drawing.Size(440, 166)
    Me.Controls.Add(Me.applicationsListBoxLabel)
    Me.Controls.Add(Me.openButton)
    Me.Controls.Add(Me.exitButton)
    Me.Controls.Add(Me.applicationsListBox)
    Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
    Me.Name = "Switchboard"
    Me.Text = "Switchboard"
    Me.ResumeLayout(False)

  End Sub
  Private WithEvents applicationsListBoxLabel As System.Windows.Forms.Label
  Private WithEvents openButton As System.Windows.Forms.Button
  Private WithEvents exitButton As System.Windows.Forms.Button
  Private WithEvents applicationsListBox As System.Windows.Forms.ListBox
  Private WithEvents tipsTooltip As System.Windows.Forms.ToolTip
End Class
