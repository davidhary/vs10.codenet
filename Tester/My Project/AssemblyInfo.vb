Imports System.Reflection
Imports System.Runtime.CompilerServices
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

<Assembly: AssemblyTitle("Code Net Library Tester")> 
<Assembly: AssemblyDescription("Tester for the ISR Code Net Library")> 
<Assembly: AssemblyProduct("CodeNet.Library.Tester.2013")> 
<Assembly: CLSCompliant(True)> 

' Disable accessibility of an individual managed type or member, or of all types within an assembly, to COM.
<Assembly: System.Runtime.InteropServices.ComVisible(False)> 
