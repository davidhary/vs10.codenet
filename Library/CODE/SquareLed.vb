''' <summary>Defines a simple LED display with a caption
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="09/05/2006" by="David" revision="2.0.2439.x">
''' Created
''' </history>
<System.ComponentModel.Description("Squared Led Control"), 
    System.Drawing.ToolboxBitmap(GetType(SquareLed))> 
Public Class SquareLed
    Inherits System.Windows.Forms.UserControl

#Region " APPEARANCE "

    Private _backgroundColor As Integer
    <System.ComponentModel.Browsable(True), 
      System.ComponentModel.Category("Appearance"), 
      System.ComponentModel.Description("Background Color.")> 
    Public Property BackgroundColor() As Integer
        Get

            Return Me._backgroundColor

        End Get
        Set(ByVal Value As Integer)

            If Value <> backgroundColor Then
                Me._backgroundColor = Value
                BackColor = System.Drawing.ColorTranslator.FromOle(Value)
            End If

        End Set
    End Property

    Private _caption As String
    <System.ComponentModel.Browsable(True), 
      System.ComponentModel.Category("Appearance"), 
      System.ComponentModel.Description("Caption.")> 
    Public Property Caption() As String
        Get
            Return Me._caption
        End Get
        Set(ByVal Value As String)

            If Value <> caption Then
                Me._caption = Value
                captionLabel.Text = Caption
            End If

        End Set
    End Property

    Private _indicatorColor As Integer
    <System.ComponentModel.Browsable(True), 
      System.ComponentModel.Category("Appearance"), 
      System.ComponentModel.Description("Indicator color.")> 
    Public Property IndicatorColor() As Integer
        Get

            Return Me._indicatorColor

        End Get
        Set(ByVal Value As Integer)

            If Value <> indicatorColor Then
                Me._indicatorColor = Value
                indicatorLabel.BackColor = System.Drawing.ColorTranslator.FromOle(Me._indicatorColor)
            End If

        End Set
    End Property

    Private _state As IndicatorState
    <System.ComponentModel.Browsable(True), 
      System.ComponentModel.Category("Appearance"), 
      System.ComponentModel.Description("Is On.")> 
    Public Property IsOn() As Boolean
        Get
            Return Me._state = IndicatorState.On
        End Get
        Set(ByVal Value As Boolean)

            If Value <> Me.IsOn Then

                If Value Then
                    Me.State = IndicatorState.On
                Else
                    Me.State = IndicatorState.Off
                End If

            End If

        End Set
    End Property

    Private _noColor As Integer
    <System.ComponentModel.Browsable(True), 
      System.ComponentModel.Category("Appearance"), 
      System.ComponentModel.Description("No Color.")> 
    Public Property NoColor() As Integer
        Get

            Return Me._noColor

        End Get
        Set(ByVal Value As Integer)

            Me._noColor = Value

        End Set
    End Property

    Private _offColor As Integer
    <System.ComponentModel.Browsable(True), 
      System.ComponentModel.Category("Appearance"), 
      System.ComponentModel.Description("Off Color.")> 
    Public Property OffColor() As Integer
        Get

            Return Me._offColor

        End Get
        Set(ByVal Value As Integer)

            Me._offColor = Value

        End Set
    End Property

    Private _onColor As Integer
    <System.ComponentModel.Browsable(True), 
      System.ComponentModel.Category("Appearance"), 
      System.ComponentModel.Description("On Color.")> 
    Public Property OnColor() As Integer
        Get

            Return Me._onColor

        End Get
        Set(ByVal Value As Integer)

            If Value <> onColor Then
                Me._onColor = Value
            End If

        End Set
    End Property

    ''' <summary>Sets the indicator color based on the state.
    ''' </summary>
    <System.ComponentModel.Browsable(True), 
      System.ComponentModel.Category("Appearance"), 
      System.ComponentModel.Description("Current State.")> 
    Public Property State() As IndicatorState
        Get
            Return Me._state
        End Get
        Set(ByVal Value As IndicatorState)

            If Value <> state Then
                Me._state = Value
                If Me.Visible Then

                    Select Case True
                        Case Value = IndicatorState.None
                            Me.IndicatorColor = Me.NoColor
                        Case Value = IndicatorState.On
                            Me.IndicatorColor = Me.OnColor
                        Case Value = IndicatorState.Off
                            Me.IndicatorColor = Me.OffColor
                    End Select

                End If

            End If

        End Set
    End Property

#End Region

#Region " EVENTS HANDLERS "

    ''' <summary>Occurs when the user clicks the indicator.
    ''' </summary>
    Public Shadows Event Click As EventHandler(Of EventArgs)

    ''' <summary>
    ''' Raises the click event.
    ''' </summary>
      Private Sub indicatorLabel_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles indicatorLabel.Click

        Dim evt As EventHandler(Of EventArgs) = Me.ClickEvent
        If evt IsNot Nothing Then evt.Invoke(Me, System.EventArgs.Empty)

    End Sub

    ''' <summary>
    ''' Position the controls.
    ''' </summary>
      Private Sub SquareLed_Resize(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Resize

        captionLabel.SetBounds((Width - captionLabel.Width) \ 2, 0, Width, captionLabel.Height, Windows.Forms.BoundsSpecified.All)
        indicatorLabel.SetBounds((Width - indicatorLabel.Width) \ 2, Height - indicatorLabel.Height, 0, 0, Windows.Forms.BoundsSpecified.X Or Windows.Forms.BoundsSpecified.Y)

    End Sub

#End Region

End Class

''' <summary>Enumerates the indicator states.
''' </summary>
Public Enum IndicatorState
    <System.ComponentModel.Description("None")> None
    <System.ComponentModel.Description("On")> [On]
    <System.ComponentModel.Description("Off")> [Off]
End Enum

