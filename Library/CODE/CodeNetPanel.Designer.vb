<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class CodeNetPanel
#Region "Windows Form Designer generated code "
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            Me.onDisposeManagedResources()
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Public _toolTip As System.Windows.Forms.ToolTip
    Public WithEvents _actionsSendMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _actionsBar1MenuItem As System.Windows.Forms.ToolStripSeparator
    Public WithEvents _actionsClearMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _actionsBar2MenuItem As System.Windows.Forms.ToolStripSeparator
    Public WithEvents _actionsExitMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _actionsMenu As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _openPortMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _closePortMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _clearPortMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _portMenu As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _showPortEventsMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _showReceivedDataMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _optionsBar1MenuItem As System.Windows.Forms.ToolStripSeparator
    Public WithEvents _autoLoadDefaultSettingsMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _autoLoadRxSettingsMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _optionsBar2MenuItem As System.Windows.Forms.ToolStripSeparator
    Public WithEvents _interpretTxAsHexMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _optionsMenu As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _applyNewSettingsMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _applyRxSettingsMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _settingsBar1MenuItem As System.Windows.Forms.ToolStripSeparator
    Public WithEvents _saveActualsAsDefaultsMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _displayDefaultsMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _displayActualsMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _settingsBar2MenuItem As System.Windows.Forms.ToolStripSeparator
    Public WithEvents _applyPacketSettingsMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _settingsMenu As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _selectAccutrackerMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _selectDghSensorsMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _selectDominoAmjetMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _devicesMenu As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _readDghDataMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _readAndZeroDghMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _dghDeviceMenu As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _dominoIdentifyMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _dominoGetConfigMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _dominoGetStatusMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _dominoSaveMessage1MenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _dominoSelectMessage1MenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _dominoEnableHead1MenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _dominoPrintGoMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _dominoDeviceMenu1 As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _accutrackerStartMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _accutrackerReadMenuItem As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _accutrackerDevceMenu As System.Windows.Forms.ToolStripMenuItem
    Public WithEvents _mainMenu As System.Windows.Forms.MenuStrip
    Public WithEvents _statusPanel As System.Windows.Forms.ToolStripStatusLabel
    Public WithEvents _timePanel As System.Windows.Forms.ToolStripStatusLabel
    Public WithEvents _statePanel As System.Windows.Forms.ToolStripStatusLabel
    Public WithEvents _statusBar As System.Windows.Forms.StatusStrip
    Public WithEvents _portOpenIndicator As SquareLed
    Public WithEvents _cdIndicator As SquareLed
    Public WithEvents _dsrIndicator As SquareLed
    Public WithEvents _dtrIndicator As SquareLed
    Public WithEvents _rtsIndicator As SquareLed
    Public WithEvents _ctsIndicator As SquareLed
    Public WithEvents _xonIndicator As SquareLed
    Public WithEvents _xoffIndicator1 As SquareLed
    Public WithEvents _rxIndicator As SquareLed
    Public WithEvents _txIndicator As SquareLed
    Public WithEvents _notAckIndicator As SquareLed
    Public WithEvents _terminalTextBox As System.Windows.Forms.TextBox
    Public WithEvents _sendTextBox As System.Windows.Forms.TextBox
    Public WithEvents _receiveTextBox As System.Windows.Forms.TextBox
    Public WithEvents _sendTextBoxLabel As System.Windows.Forms.Label
    Public WithEvents _receiveTextBoxLabel As System.Windows.Forms.Label
    Public WithEvents _dataTabPage As System.Windows.Forms.TabPage
    Public WithEvents _endOfPacketByte1TextBox As System.Windows.Forms.TextBox
    Public WithEvents _endPacketByte2TextBox As System.Windows.Forms.TextBox
    Public WithEvents _eotSubstitueTextBox As System.Windows.Forms.TextBox
    Public WithEvents _startPacketByteTextBox As System.Windows.Forms.TextBox
    Public WithEvents _acknowledgeByteTextBox As System.Windows.Forms.TextBox
    Public WithEvents _notAcknowledgeByteTextBox As System.Windows.Forms.TextBox
    Public WithEvents _nakMessageLengthTextBox As System.Windows.Forms.TextBox
    Public WithEvents _packetLengthTextBox As System.Windows.Forms.TextBox
    Public WithEvents _replaceEotChecklBox As System.Windows.Forms.CheckBox
    Public WithEvents _enquiryByteTextBox As System.Windows.Forms.TextBox
    Public WithEvents _endOfTextByteTextBox As System.Windows.Forms.TextBox
    Public WithEvents _startOfTextByteTextBox As System.Windows.Forms.TextBox
    Public WithEvents EndOfPacketByte1TextBoxLabel As System.Windows.Forms.Label
    Public WithEvents EndOfPacketByte2TextBoxLabel As System.Windows.Forms.Label
    Public WithEvents EotSubstitueTextBoxLabel As System.Windows.Forms.Label
    Public WithEvents startOfPacketByteTextBoxLabel As System.Windows.Forms.Label
    Public WithEvents AcknowledgeByteTextBoxLabel As System.Windows.Forms.Label
    Public WithEvents NotAcknowledgeByteTextBoxLabel As System.Windows.Forms.Label
    Public WithEvents NakMessageLengthTextBoxLabel As System.Windows.Forms.Label
    Public WithEvents PacketLengthTextBoxLabel As System.Windows.Forms.Label
    Public WithEvents PacketActualLabel As System.Windows.Forms.Label
    Public WithEvents PacketRecLabel As System.Windows.Forms.Label
    Public WithEvents EndOfPacketByte1RecLabel As System.Windows.Forms.Label
    Public WithEvents EndOfPacketByte2RecLabel As System.Windows.Forms.Label
    Public WithEvents ReplaceEotRecLabel As System.Windows.Forms.Label
    Public WithEvents EotSubstitueRecLabel As System.Windows.Forms.Label
    Public WithEvents startOfPacketByteRecLabel As System.Windows.Forms.Label
    Public WithEvents AcknowledgeByteRecLabel As System.Windows.Forms.Label
    Public WithEvents NotAcknowledgeByteRecLabel As System.Windows.Forms.Label
    Public WithEvents NakMessageLengthRecLabel As System.Windows.Forms.Label
    Public WithEvents PacketLengthRecLabel As System.Windows.Forms.Label
    Public WithEvents EndOfPacketByte1InfoLabel As System.Windows.Forms.Label
    Public WithEvents EndOfPacketByte2InfoLabel As System.Windows.Forms.Label
    Public WithEvents startOfPacketByteInfoLabel As System.Windows.Forms.Label
    Public WithEvents AcknowledgeByteInfoLabel As System.Windows.Forms.Label
    Public WithEvents EnquiryByteRecLabel As System.Windows.Forms.Label
    Public WithEvents EndOfTextByteRecLabel As System.Windows.Forms.Label
    Public WithEvents StartOfTextByteRecLabel As System.Windows.Forms.Label
    Public WithEvents EnquiryByteTextBoxLabel As System.Windows.Forms.Label
    Public WithEvents EndOfTextByteTextBoxLabel As System.Windows.Forms.Label
    Public WithEvents StartOfTextByteTextBoxLabel As System.Windows.Forms.Label
    Public WithEvents _packetSettingsFrame As System.Windows.Forms.GroupBox
    Public WithEvents _packetSettingsTabPage As System.Windows.Forms.TabPage
    Public WithEvents _txThresholdTextBox As System.Windows.Forms.TextBox
    Public WithEvents _portSettingsTextBox As System.Windows.Forms.TextBox
    Public WithEvents _rxThresholdTextBox As System.Windows.Forms.TextBox
    Public WithEvents _parityReplaceCharTextBox As System.Windows.Forms.TextBox
    Public WithEvents _writeBufferSizeTextBox As System.Windows.Forms.TextBox
    Public WithEvents _inputLengthTextBox As System.Windows.Forms.TextBox
    Public WithEvents _inputBufferSizeTextBox As System.Windows.Forms.TextBox
    Public WithEvents _portNumberTextBox As System.Windows.Forms.TextBox
    Public WithEvents _handshakingComboBox As System.Windows.Forms.ComboBox
    Public WithEvents _InputModeComboBox As System.Windows.Forms.ComboBox
    Public WithEvents _dtrEnableToggle As System.Windows.Forms.CheckBox
    Public WithEvents _discardNullToggle As System.Windows.Forms.CheckBox
    Public WithEvents _rtsEnableToggle As System.Windows.Forms.CheckBox
    Public WithEvents _recommendedTxThresholdLabel As System.Windows.Forms.Label
    Public WithEvents _recommendedCommSettingsLabel As System.Windows.Forms.Label
    Public WithEvents _recommendedRtsEnableLabel As System.Windows.Forms.Label
    Public WithEvents _recommendedRxThresholdLabel As System.Windows.Forms.Label
    Public WithEvents _recommendedParityReplaceCharLabel As System.Windows.Forms.Label
    Public WithEvents _recommendedWriteBufferSizeLabel As System.Windows.Forms.Label
    Public WithEvents _recommendedDiscardNullLabel As System.Windows.Forms.Label
    Public WithEvents _recommendedInputModeLabel As System.Windows.Forms.Label
    Public WithEvents _recommendedInputLengthLabel As System.Windows.Forms.Label
    Public WithEvents __recommendedInputBufferSizeLabel As System.Windows.Forms.Label
    Public WithEvents __recommendedHandshakeLabel As System.Windows.Forms.Label
    Public WithEvents __recommendedDtrEnableLabel As System.Windows.Forms.Label
    Public WithEvents _recommendedPortNumberLabel As System.Windows.Forms.Label
    Public WithEvents RecommendedSettingTextBox As System.Windows.Forms.Label
    Public WithEvents _actualSettingsTextBoxLabel As System.Windows.Forms.Label
    Public WithEvents TxThresholdTextBoxLabel As System.Windows.Forms.Label
    Public WithEvents CommSettingsTextBoxLabel As System.Windows.Forms.Label
    Public WithEvents RxThresholdTextBoxLabel As System.Windows.Forms.Label
    Public WithEvents ParityReplaceCharTextBoxlabel As System.Windows.Forms.Label
    Public WithEvents WriteBufferSizeTextBoxLabel As System.Windows.Forms.Label
    Public WithEvents InputModeComboBoxLabel As System.Windows.Forms.Label
    Public WithEvents _inputLengthTextBoxLabel As System.Windows.Forms.Label
    Public WithEvents InputBufferSizeTextBoxLabel As System.Windows.Forms.Label
    Public WithEvents _handshakeComboBoxLabel As System.Windows.Forms.Label
    Public WithEvents _commPortNumberTextBoxlabel As System.Windows.Forms.Label
    Public WithEvents _settingsFrame As System.Windows.Forms.GroupBox
    Public WithEvents _portSettingsTabPage As System.Windows.Forms.TabPage
    Public WithEvents _tabControl As System.Windows.Forms.TabControl
    Public WithEvents _updateTimer As System.Windows.Forms.Timer
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="Domino-Amjet")> <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="Comm")> <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId:="Accutracker")>
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CodeNetPanel))
        Me._toolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._sendTextBox = New System.Windows.Forms.TextBox
        Me._receiveTextBox = New System.Windows.Forms.TextBox
        Me._portOpenIndicator = New isr.CodeNet.SquareLed
        Me._cdIndicator = New isr.CodeNet.SquareLed
        Me._dsrIndicator = New isr.CodeNet.SquareLed
        Me._dtrIndicator = New isr.CodeNet.SquareLed
        Me._rtsIndicator = New isr.CodeNet.SquareLed
        Me._ctsIndicator = New isr.CodeNet.SquareLed
        Me._xonIndicator = New isr.CodeNet.SquareLed
        Me._xoffIndicator1 = New isr.CodeNet.SquareLed
        Me._rxIndicator = New isr.CodeNet.SquareLed
        Me._txIndicator = New isr.CodeNet.SquareLed
        Me._notAckIndicator = New isr.CodeNet.SquareLed
        Me._mainMenu = New System.Windows.Forms.MenuStrip
        Me._actionsMenu = New System.Windows.Forms.ToolStripMenuItem
        Me._actionsSendMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._actionsBar1MenuItem = New System.Windows.Forms.ToolStripSeparator
        Me._actionsClearMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._actionsBar2MenuItem = New System.Windows.Forms.ToolStripSeparator
        Me._actionsExitMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._portMenu = New System.Windows.Forms.ToolStripMenuItem
        Me._openPortMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._closePortMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._clearPortMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._optionsMenu = New System.Windows.Forms.ToolStripMenuItem
        Me._showPortEventsMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._showReceivedDataMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._optionsBar1MenuItem = New System.Windows.Forms.ToolStripSeparator
        Me._autoLoadDefaultSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._autoLoadRxSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._optionsBar2MenuItem = New System.Windows.Forms.ToolStripSeparator
        Me._interpretTxAsHexMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._settingsMenu = New System.Windows.Forms.ToolStripMenuItem
        Me._applyNewSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._applyRxSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._settingsBar1MenuItem = New System.Windows.Forms.ToolStripSeparator
        Me._saveActualsAsDefaultsMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._displayDefaultsMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._displayActualsMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._settingsBar2MenuItem = New System.Windows.Forms.ToolStripSeparator
        Me._applyPacketSettingsMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._devicesMenu = New System.Windows.Forms.ToolStripMenuItem
        Me._selectAccutrackerMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._selectDghSensorsMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._selectDominoAmjetMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._dghDeviceMenu = New System.Windows.Forms.ToolStripMenuItem
        Me._readDghDataMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._readAndZeroDghMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._dominoDeviceMenu1 = New System.Windows.Forms.ToolStripMenuItem
        Me._dominoIdentifyMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._dominoGetConfigMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._dominoGetStatusMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._dominoSaveMessage1MenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._dominoSelectMessage1MenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._dominoEnableHead1MenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._dominoPrintGoMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._accutrackerDevceMenu = New System.Windows.Forms.ToolStripMenuItem
        Me._accutrackerStartMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._accutrackerReadMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me._statusBar = New System.Windows.Forms.StatusStrip
        Me._statusPanel = New System.Windows.Forms.ToolStripStatusLabel
        Me._timePanel = New System.Windows.Forms.ToolStripStatusLabel
        Me._statePanel = New System.Windows.Forms.ToolStripStatusLabel
        Me._tabControl = New System.Windows.Forms.TabControl
        Me._dataTabPage = New System.Windows.Forms.TabPage
        Me._dataLayout = New System.Windows.Forms.TableLayoutPanel
        Me._terminalTextBox = New System.Windows.Forms.TextBox
        Me._txPanel = New System.Windows.Forms.Panel
        Me._sendTextBoxLabel = New System.Windows.Forms.Label
        Me._rxPanel = New System.Windows.Forms.Panel
        Me._receiveTextBoxLabel = New System.Windows.Forms.Label
        Me._packetSettingsTabPage = New System.Windows.Forms.TabPage
        Me._packetSettingsLayout = New System.Windows.Forms.TableLayoutPanel
        Me._packetSettingsFrame = New System.Windows.Forms.GroupBox
        Me._endOfPacketByte1TextBox = New System.Windows.Forms.TextBox
        Me._endPacketByte2TextBox = New System.Windows.Forms.TextBox
        Me._eotSubstitueTextBox = New System.Windows.Forms.TextBox
        Me._startPacketByteTextBox = New System.Windows.Forms.TextBox
        Me._acknowledgeByteTextBox = New System.Windows.Forms.TextBox
        Me._notAcknowledgeByteTextBox = New System.Windows.Forms.TextBox
        Me._nakMessageLengthTextBox = New System.Windows.Forms.TextBox
        Me._packetLengthTextBox = New System.Windows.Forms.TextBox
        Me._replaceEotChecklBox = New System.Windows.Forms.CheckBox
        Me._enquiryByteTextBox = New System.Windows.Forms.TextBox
        Me._endOfTextByteTextBox = New System.Windows.Forms.TextBox
        Me._startOfTextByteTextBox = New System.Windows.Forms.TextBox
        Me.EndOfPacketByte1TextBoxLabel = New System.Windows.Forms.Label
        Me.EndOfPacketByte2TextBoxLabel = New System.Windows.Forms.Label
        Me.EotSubstitueTextBoxLabel = New System.Windows.Forms.Label
        Me.startOfPacketByteTextBoxLabel = New System.Windows.Forms.Label
        Me.AcknowledgeByteTextBoxLabel = New System.Windows.Forms.Label
        Me.NotAcknowledgeByteTextBoxLabel = New System.Windows.Forms.Label
        Me.NakMessageLengthTextBoxLabel = New System.Windows.Forms.Label
        Me.PacketLengthTextBoxLabel = New System.Windows.Forms.Label
        Me.PacketActualLabel = New System.Windows.Forms.Label
        Me.PacketRecLabel = New System.Windows.Forms.Label
        Me.EndOfPacketByte1RecLabel = New System.Windows.Forms.Label
        Me.EndOfPacketByte2RecLabel = New System.Windows.Forms.Label
        Me.ReplaceEotRecLabel = New System.Windows.Forms.Label
        Me.EotSubstitueRecLabel = New System.Windows.Forms.Label
        Me.startOfPacketByteRecLabel = New System.Windows.Forms.Label
        Me.AcknowledgeByteRecLabel = New System.Windows.Forms.Label
        Me.NotAcknowledgeByteRecLabel = New System.Windows.Forms.Label
        Me.NakMessageLengthRecLabel = New System.Windows.Forms.Label
        Me.PacketLengthRecLabel = New System.Windows.Forms.Label
        Me.EndOfPacketByte1InfoLabel = New System.Windows.Forms.Label
        Me.EndOfPacketByte2InfoLabel = New System.Windows.Forms.Label
        Me.startOfPacketByteInfoLabel = New System.Windows.Forms.Label
        Me.AcknowledgeByteInfoLabel = New System.Windows.Forms.Label
        Me.EnquiryByteRecLabel = New System.Windows.Forms.Label
        Me.EndOfTextByteRecLabel = New System.Windows.Forms.Label
        Me.StartOfTextByteRecLabel = New System.Windows.Forms.Label
        Me.EnquiryByteTextBoxLabel = New System.Windows.Forms.Label
        Me.EndOfTextByteTextBoxLabel = New System.Windows.Forms.Label
        Me.StartOfTextByteTextBoxLabel = New System.Windows.Forms.Label
        Me._portSettingsTabPage = New System.Windows.Forms.TabPage
        Me._portSettingsLayout = New System.Windows.Forms.TableLayoutPanel
        Me._settingsFrame = New System.Windows.Forms.GroupBox
        Me._txThresholdTextBox = New System.Windows.Forms.TextBox
        Me._portSettingsTextBox = New System.Windows.Forms.TextBox
        Me._rxThresholdTextBox = New System.Windows.Forms.TextBox
        Me._parityReplaceCharTextBox = New System.Windows.Forms.TextBox
        Me._writeBufferSizeTextBox = New System.Windows.Forms.TextBox
        Me._inputLengthTextBox = New System.Windows.Forms.TextBox
        Me._inputBufferSizeTextBox = New System.Windows.Forms.TextBox
        Me._portNumberTextBox = New System.Windows.Forms.TextBox
        Me._handshakingComboBox = New System.Windows.Forms.ComboBox
        Me._InputModeComboBox = New System.Windows.Forms.ComboBox
        Me._dtrEnableToggle = New System.Windows.Forms.CheckBox
        Me._discardNullToggle = New System.Windows.Forms.CheckBox
        Me._rtsEnableToggle = New System.Windows.Forms.CheckBox
        Me._recommendedTxThresholdLabel = New System.Windows.Forms.Label
        Me._recommendedCommSettingsLabel = New System.Windows.Forms.Label
        Me._recommendedRtsEnableLabel = New System.Windows.Forms.Label
        Me._recommendedRxThresholdLabel = New System.Windows.Forms.Label
        Me._recommendedParityReplaceCharLabel = New System.Windows.Forms.Label
        Me._recommendedWriteBufferSizeLabel = New System.Windows.Forms.Label
        Me._recommendedDiscardNullLabel = New System.Windows.Forms.Label
        Me._recommendedInputModeLabel = New System.Windows.Forms.Label
        Me._recommendedInputLengthLabel = New System.Windows.Forms.Label
        Me.__recommendedInputBufferSizeLabel = New System.Windows.Forms.Label
        Me.__recommendedHandshakeLabel = New System.Windows.Forms.Label
        Me.__recommendedDtrEnableLabel = New System.Windows.Forms.Label
        Me._recommendedPortNumberLabel = New System.Windows.Forms.Label
        Me.RecommendedSettingTextBox = New System.Windows.Forms.Label
        Me._actualSettingsTextBoxLabel = New System.Windows.Forms.Label
        Me.TxThresholdTextBoxLabel = New System.Windows.Forms.Label
        Me.CommSettingsTextBoxLabel = New System.Windows.Forms.Label
        Me.RxThresholdTextBoxLabel = New System.Windows.Forms.Label
        Me.ParityReplaceCharTextBoxlabel = New System.Windows.Forms.Label
        Me.WriteBufferSizeTextBoxLabel = New System.Windows.Forms.Label
        Me.InputModeComboBoxLabel = New System.Windows.Forms.Label
        Me._inputLengthTextBoxLabel = New System.Windows.Forms.Label
        Me.InputBufferSizeTextBoxLabel = New System.Windows.Forms.Label
        Me._handshakeComboBoxLabel = New System.Windows.Forms.Label
        Me._commPortNumberTextBoxlabel = New System.Windows.Forms.Label
        Me._MessagesTabPage = New System.Windows.Forms.TabPage
        Me._updateTimer = New System.Windows.Forms.Timer(Me.components)
        Me._indicatorsLayout = New System.Windows.Forms.TableLayoutPanel
        Me._messagesListBox = New System.Windows.Forms.ListBox
        Me._mainMenu.SuspendLayout()
        Me._statusBar.SuspendLayout()
        Me._tabControl.SuspendLayout()
        Me._dataTabPage.SuspendLayout()
        Me._dataLayout.SuspendLayout()
        Me._txPanel.SuspendLayout()
        Me._rxPanel.SuspendLayout()
        Me._packetSettingsTabPage.SuspendLayout()
        Me._packetSettingsLayout.SuspendLayout()
        Me._packetSettingsFrame.SuspendLayout()
        Me._portSettingsTabPage.SuspendLayout()
        Me._portSettingsLayout.SuspendLayout()
        Me._settingsFrame.SuspendLayout()
        Me._MessagesTabPage.SuspendLayout()
        Me._indicatorsLayout.SuspendLayout()
        Me.SuspendLayout()
        '
        '_sendTextBox
        '
        Me._sendTextBox.AcceptsReturn = True
        Me._sendTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                        System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._sendTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._sendTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._sendTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._sendTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._sendTextBox.Location = New System.Drawing.Point(22, 1)
        Me._sendTextBox.MaxLength = 0
        Me._sendTextBox.Name = "_sendTextBox"
        Me._sendTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._sendTextBox.Size = New System.Drawing.Size(456, 20)
        Me._sendTextBox.TabIndex = 3
        Me._sendTextBox.Text = "Enter the string to send here"
        Me._toolTip.SetToolTip(Me._sendTextBox, "Enter the string to send here")
        '
        '_receiveTextBox
        '
        Me._receiveTextBox.AcceptsReturn = True
        Me._receiveTextBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) Or
                                           System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._receiveTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._receiveTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._receiveTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._receiveTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._receiveTextBox.Location = New System.Drawing.Point(22, 1)
        Me._receiveTextBox.MaxLength = 255
        Me._receiveTextBox.Multiline = True
        Me._receiveTextBox.Name = "_receiveTextBox"
        Me._receiveTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._receiveTextBox.Size = New System.Drawing.Size(456, 19)
        Me._receiveTextBox.TabIndex = 2
        Me._toolTip.SetToolTip(Me._receiveTextBox, "Received data since last send")
        '
        '_portOpenIndicator
        '
        Me._portOpenIndicator.BackgroundColor = 0
        Me._portOpenIndicator.Caption = "OPEN"
        Me._portOpenIndicator.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._portOpenIndicator.ForeColor = System.Drawing.Color.Black
        Me._portOpenIndicator.IndicatorColor = 0
        Me._portOpenIndicator.IsOn = False
        Me._portOpenIndicator.Location = New System.Drawing.Point(8, 3)
        Me._portOpenIndicator.Name = "_portOpenIndicator"
        Me._portOpenIndicator.NoColor = 0
        Me._portOpenIndicator.OffColor = 0
        Me._portOpenIndicator.OnColor = 0
        Me._portOpenIndicator.Size = New System.Drawing.Size(36, 29)
        Me._portOpenIndicator.State = isr.CodeNet.IndicatorState.Off
        Me._portOpenIndicator.TabIndex = 89
        Me._toolTip.SetToolTip(Me._portOpenIndicator, "Port open or closed status")
        '
        '_cdIndicator
        '
        Me._cdIndicator.BackgroundColor = 0
        Me._cdIndicator.Caption = "CD"
        Me._cdIndicator.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._cdIndicator.IndicatorColor = 0
        Me._cdIndicator.IsOn = False
        Me._cdIndicator.Location = New System.Drawing.Point(59, 3)
        Me._cdIndicator.Name = "_cdIndicator"
        Me._cdIndicator.NoColor = 0
        Me._cdIndicator.OffColor = 0
        Me._cdIndicator.OnColor = 0
        Me._cdIndicator.Size = New System.Drawing.Size(24, 29)
        Me._cdIndicator.State = isr.CodeNet.IndicatorState.None
        Me._cdIndicator.TabIndex = 91
        Me._toolTip.SetToolTip(Me._cdIndicator, "Carrier Detect status")
        '
        '_dsrIndicator
        '
        Me._dsrIndicator.BackgroundColor = 0
        Me._dsrIndicator.Caption = "DSR"
        Me._dsrIndicator.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._dsrIndicator.IndicatorColor = 0
        Me._dsrIndicator.IsOn = False
        Me._dsrIndicator.Location = New System.Drawing.Point(98, 3)
        Me._dsrIndicator.Name = "_dsrIndicator"
        Me._dsrIndicator.NoColor = 0
        Me._dsrIndicator.OffColor = 0
        Me._dsrIndicator.OnColor = 0
        Me._dsrIndicator.Size = New System.Drawing.Size(33, 29)
        Me._dsrIndicator.State = isr.CodeNet.IndicatorState.None
        Me._dsrIndicator.TabIndex = 92
        Me._toolTip.SetToolTip(Me._dsrIndicator, "DSR Status")
        '
        '_dtrIndicator
        '
        Me._dtrIndicator.BackgroundColor = 0
        Me._dtrIndicator.Caption = "DTR"
        Me._dtrIndicator.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._dtrIndicator.IndicatorColor = 0
        Me._dtrIndicator.IsOn = False
        Me._dtrIndicator.Location = New System.Drawing.Point(137, 3)
        Me._dtrIndicator.Name = "_dtrIndicator"
        Me._dtrIndicator.NoColor = 0
        Me._dtrIndicator.OffColor = 0
        Me._dtrIndicator.OnColor = 0
        Me._dtrIndicator.Size = New System.Drawing.Size(33, 29)
        Me._dtrIndicator.State = isr.CodeNet.IndicatorState.None
        Me._dtrIndicator.TabIndex = 93
        Me._toolTip.SetToolTip(Me._dtrIndicator, "DTR Status")
        '
        '_rtsIndicator
        '
        Me._rtsIndicator.BackgroundColor = 0
        Me._rtsIndicator.Caption = "RTS"
        Me._rtsIndicator.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._rtsIndicator.IndicatorColor = 0
        Me._rtsIndicator.IsOn = False
        Me._rtsIndicator.Location = New System.Drawing.Point(185, 3)
        Me._rtsIndicator.Name = "_rtsIndicator"
        Me._rtsIndicator.NoColor = 0
        Me._rtsIndicator.OffColor = 0
        Me._rtsIndicator.OnColor = 0
        Me._rtsIndicator.Size = New System.Drawing.Size(33, 29)
        Me._rtsIndicator.State = isr.CodeNet.IndicatorState.None
        Me._rtsIndicator.TabIndex = 94
        Me._toolTip.SetToolTip(Me._rtsIndicator, "RTS Status: ")
        '
        '_ctsIndicator
        '
        Me._ctsIndicator.BackgroundColor = 0
        Me._ctsIndicator.Caption = "CTS"
        Me._ctsIndicator.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._ctsIndicator.IndicatorColor = 0
        Me._ctsIndicator.IsOn = False
        Me._ctsIndicator.Location = New System.Drawing.Point(224, 3)
        Me._ctsIndicator.Name = "_ctsIndicator"
        Me._ctsIndicator.NoColor = 0
        Me._ctsIndicator.OffColor = 0
        Me._ctsIndicator.OnColor = 0
        Me._ctsIndicator.Size = New System.Drawing.Size(33, 29)
        Me._ctsIndicator.State = isr.CodeNet.IndicatorState.None
        Me._ctsIndicator.TabIndex = 95
        Me._toolTip.SetToolTip(Me._ctsIndicator, "CTS Status: ")
        '
        '_xonIndicator
        '
        Me._xonIndicator.BackgroundColor = 0
        Me._xonIndicator.Caption = "XON"
        Me._xonIndicator.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._xonIndicator.IndicatorColor = 0
        Me._xonIndicator.IsOn = False
        Me._xonIndicator.Location = New System.Drawing.Point(272, 3)
        Me._xonIndicator.Name = "_xonIndicator"
        Me._xonIndicator.NoColor = 0
        Me._xonIndicator.OffColor = 0
        Me._xonIndicator.OnColor = 0
        Me._xonIndicator.Size = New System.Drawing.Size(33, 29)
        Me._xonIndicator.State = isr.CodeNet.IndicatorState.None
        Me._xonIndicator.TabIndex = 96
        Me._toolTip.SetToolTip(Me._xonIndicator, "XON Status")
        '
        '_xoffIndicator1
        '
        Me._xoffIndicator1.BackgroundColor = 0
        Me._xoffIndicator1.Caption = "XOFF"
        Me._xoffIndicator1.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._xoffIndicator1.IndicatorColor = 0
        Me._xoffIndicator1.IsOn = False
        Me._xoffIndicator1.Location = New System.Drawing.Point(311, 3)
        Me._xoffIndicator1.Name = "_xoffIndicator1"
        Me._xoffIndicator1.NoColor = 0
        Me._xoffIndicator1.OffColor = 0
        Me._xoffIndicator1.OnColor = 0
        Me._xoffIndicator1.Size = New System.Drawing.Size(33, 29)
        Me._xoffIndicator1.State = isr.CodeNet.IndicatorState.None
        Me._xoffIndicator1.TabIndex = 97
        Me._toolTip.SetToolTip(Me._xoffIndicator1, "XOFF Status")
        '
        '_rxIndicator
        '
        Me._rxIndicator.BackgroundColor = 0
        Me._rxIndicator.Caption = "RX"
        Me._rxIndicator.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._rxIndicator.IndicatorColor = 0
        Me._rxIndicator.IsOn = False
        Me._rxIndicator.Location = New System.Drawing.Point(359, 3)
        Me._rxIndicator.Name = "_rxIndicator"
        Me._rxIndicator.NoColor = 0
        Me._rxIndicator.OffColor = 0
        Me._rxIndicator.OnColor = 0
        Me._rxIndicator.Size = New System.Drawing.Size(33, 29)
        Me._rxIndicator.State = isr.CodeNet.IndicatorState.None
        Me._rxIndicator.TabIndex = 98
        Me._toolTip.SetToolTip(Me._rxIndicator, "Receive Status")
        '
        '_txIndicator
        '
        Me._txIndicator.AllowDrop = True
        Me._txIndicator.BackgroundColor = 0
        Me._txIndicator.Caption = "TX"
        Me._txIndicator.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._txIndicator.IndicatorColor = 0
        Me._txIndicator.IsOn = False
        Me._txIndicator.Location = New System.Drawing.Point(398, 3)
        Me._txIndicator.Name = "_txIndicator"
        Me._txIndicator.NoColor = 0
        Me._txIndicator.OffColor = 0
        Me._txIndicator.OnColor = 0
        Me._txIndicator.Size = New System.Drawing.Size(33, 29)
        Me._txIndicator.State = isr.CodeNet.IndicatorState.None
        Me._txIndicator.TabIndex = 99
        Me._toolTip.SetToolTip(Me._txIndicator, "Transmit Status")
        '
        '_notAckIndicator
        '
        Me._notAckIndicator.BackgroundColor = 0
        Me._notAckIndicator.Caption = "NAK"
        Me._notAckIndicator.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._notAckIndicator.IndicatorColor = 0
        Me._notAckIndicator.IsOn = False
        Me._notAckIndicator.Location = New System.Drawing.Point(446, 3)
        Me._notAckIndicator.Name = "_notAckIndicator"
        Me._notAckIndicator.NoColor = 0
        Me._notAckIndicator.OffColor = 0
        Me._notAckIndicator.OnColor = 0
        Me._notAckIndicator.Size = New System.Drawing.Size(33, 29)
        Me._notAckIndicator.State = isr.CodeNet.IndicatorState.None
        Me._notAckIndicator.TabIndex = 100
        Me._toolTip.SetToolTip(Me._notAckIndicator, "Not acknowledged Status")
        '
        '_mainMenu
        '
        Me._mainMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._actionsMenu, Me._portMenu, Me._optionsMenu, Me._settingsMenu, Me._devicesMenu, Me._dghDeviceMenu, Me._dominoDeviceMenu1, Me._accutrackerDevceMenu})
        Me._mainMenu.Location = New System.Drawing.Point(0, 0)
        Me._mainMenu.Name = "_mainMenu"
        Me._mainMenu.Size = New System.Drawing.Size(493, 24)
        Me._mainMenu.TabIndex = 92
        '
        '_actionsMenu
        '
        Me._actionsMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._actionsSendMenuItem, Me._actionsBar1MenuItem, Me._actionsClearMenuItem, Me._actionsBar2MenuItem, Me._actionsExitMenuItem})
        Me._actionsMenu.Name = "_actionsMenu"
        Me._actionsMenu.Size = New System.Drawing.Size(54, 20)
        Me._actionsMenu.Text = "&Actions"
        '
        '_actionsSendMenuItem
        '
        Me._actionsSendMenuItem.Name = "_actionsSendMenuItem"
        Me._actionsSendMenuItem.Size = New System.Drawing.Size(125, 22)
        Me._actionsSendMenuItem.Text = "&Send"
        '
        '_actionsBar1MenuItem
        '
        Me._actionsBar1MenuItem.Name = "_actionsBar1MenuItem"
        Me._actionsBar1MenuItem.Size = New System.Drawing.Size(122, 6)
        '
        '_actionsClearMenuItem
        '
        Me._actionsClearMenuItem.Name = "_actionsClearMenuItem"
        Me._actionsClearMenuItem.Size = New System.Drawing.Size(125, 22)
        Me._actionsClearMenuItem.Text = "&Clear Data"
        '
        '_actionsBar2MenuItem
        '
        Me._actionsBar2MenuItem.Name = "_actionsBar2MenuItem"
        Me._actionsBar2MenuItem.Size = New System.Drawing.Size(122, 6)
        '
        '_actionsExitMenuItem
        '
        Me._actionsExitMenuItem.Name = "_actionsExitMenuItem"
        Me._actionsExitMenuItem.Size = New System.Drawing.Size(125, 22)
        Me._actionsExitMenuItem.Text = "E&xit"
        '
        '_portMenu
        '
        Me._portMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._openPortMenuItem, Me._closePortMenuItem, Me._clearPortMenuItem})
        Me._portMenu.Name = "_portMenu"
        Me._portMenu.Size = New System.Drawing.Size(71, 20)
        Me._portMenu.Text = "&Comm Port"
        '
        '_openPortMenuItem
        '
        Me._openPortMenuItem.Name = "_openPortMenuItem"
        Me._openPortMenuItem.Size = New System.Drawing.Size(123, 22)
        Me._openPortMenuItem.Text = "&Open Port"
        '
        '_closePortMenuItem
        '
        Me._closePortMenuItem.Name = "_closePortMenuItem"
        Me._closePortMenuItem.Size = New System.Drawing.Size(123, 22)
        Me._closePortMenuItem.Text = "&Close Port"
        '
        '_clearPortMenuItem
        '
        Me._clearPortMenuItem.Name = "_clearPortMenuItem"
        Me._clearPortMenuItem.Size = New System.Drawing.Size(123, 22)
        Me._clearPortMenuItem.Text = "C&lear Port"
        '
        '_optionsMenu
        '
        Me._optionsMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._showPortEventsMenuItem, Me._showReceivedDataMenuItem, Me._optionsBar1MenuItem, Me._autoLoadDefaultSettingsMenuItem, Me._autoLoadRxSettingsMenuItem, Me._optionsBar2MenuItem, Me._interpretTxAsHexMenuItem})
        Me._optionsMenu.Name = "_optionsMenu"
        Me._optionsMenu.Size = New System.Drawing.Size(56, 20)
        Me._optionsMenu.Text = "&Options"
        '
        '_showPortEventsMenuItem
        '
        Me._showPortEventsMenuItem.Name = "_showPortEventsMenuItem"
        Me._showPortEventsMenuItem.Size = New System.Drawing.Size(238, 22)
        Me._showPortEventsMenuItem.Text = "Show Port &Events"
        '
        '_showReceivedDataMenuItem
        '
        Me._showReceivedDataMenuItem.Checked = True
        Me._showReceivedDataMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me._showReceivedDataMenuItem.Name = "_showReceivedDataMenuItem"
        Me._showReceivedDataMenuItem.Size = New System.Drawing.Size(238, 22)
        Me._showReceivedDataMenuItem.Text = "Show Received &Data"
        '
        '_optionsBar1MenuItem
        '
        Me._optionsBar1MenuItem.Name = "_optionsBar1MenuItem"
        Me._optionsBar1MenuItem.Size = New System.Drawing.Size(235, 6)
        '
        '_autoLoadDefaultSettingsMenuItem
        '
        Me._autoLoadDefaultSettingsMenuItem.Name = "_autoLoadDefaultSettingsMenuItem"
        Me._autoLoadDefaultSettingsMenuItem.Size = New System.Drawing.Size(238, 22)
        Me._autoLoadDefaultSettingsMenuItem.Text = "Auto Load De&fault Settings"
        '
        '_autoLoadRxSettingsMenuItem
        '
        Me._autoLoadRxSettingsMenuItem.Name = "_autoLoadRxSettingsMenuItem"
        Me._autoLoadRxSettingsMenuItem.Size = New System.Drawing.Size(238, 22)
        Me._autoLoadRxSettingsMenuItem.Text = "Auto Load &Recommended Settings"
        '
        '_optionsBar2MenuItem
        '
        Me._optionsBar2MenuItem.Name = "_optionsBar2MenuItem"
        Me._optionsBar2MenuItem.Size = New System.Drawing.Size(235, 6)
        '
        '_interpretTxAsHexMenuItem
        '
        Me._interpretTxAsHexMenuItem.Name = "_interpretTxAsHexMenuItem"
        Me._interpretTxAsHexMenuItem.Size = New System.Drawing.Size(238, 22)
        Me._interpretTxAsHexMenuItem.Text = "&Interpret Tx as Ascii Hex Bytes "
        '
        '_settingsMenu
        '
        Me._settingsMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._applyNewSettingsMenuItem, Me._applyRxSettingsMenuItem, Me._settingsBar1MenuItem, Me._saveActualsAsDefaultsMenuItem, Me._displayDefaultsMenuItem, Me._displayActualsMenuItem, Me._settingsBar2MenuItem, Me._applyPacketSettingsMenuItem})
        Me._settingsMenu.Name = "_settingsMenu"
        Me._settingsMenu.Size = New System.Drawing.Size(58, 20)
        Me._settingsMenu.Text = "&Settings"
        '
        '_applyNewSettingsMenuItem
        '
        Me._applyNewSettingsMenuItem.Name = "_applyNewSettingsMenuItem"
        Me._applyNewSettingsMenuItem.Size = New System.Drawing.Size(189, 22)
        Me._applyNewSettingsMenuItem.Text = "&Apply New Settings"
        '
        '_applyRxSettingsMenuItem
        '
        Me._applyRxSettingsMenuItem.Name = "_applyRxSettingsMenuItem"
        Me._applyRxSettingsMenuItem.Size = New System.Drawing.Size(189, 22)
        Me._applyRxSettingsMenuItem.Text = "Apply &Recommended"
        '
        '_settingsBar1MenuItem
        '
        Me._settingsBar1MenuItem.Name = "_settingsBar1MenuItem"
        Me._settingsBar1MenuItem.Size = New System.Drawing.Size(186, 6)
        '
        '_saveActualsAsDefaultsMenuItem
        '
        Me._saveActualsAsDefaultsMenuItem.Name = "_saveActualsAsDefaultsMenuItem"
        Me._saveActualsAsDefaultsMenuItem.Size = New System.Drawing.Size(189, 22)
        Me._saveActualsAsDefaultsMenuItem.Text = "Save Actual &As Defaults"
        '
        '_displayDefaultsMenuItem
        '
        Me._displayDefaultsMenuItem.Name = "_displayDefaultsMenuItem"
        Me._displayDefaultsMenuItem.Size = New System.Drawing.Size(189, 22)
        Me._displayDefaultsMenuItem.Text = "Di&splay Defaults"
        '
        '_displayActualsMenuItem
        '
        Me._displayActualsMenuItem.Name = "_displayActualsMenuItem"
        Me._displayActualsMenuItem.Size = New System.Drawing.Size(189, 22)
        Me._displayActualsMenuItem.Text = "&Display Actual "
        '
        '_settingsBar2MenuItem
        '
        Me._settingsBar2MenuItem.Name = "_settingsBar2MenuItem"
        Me._settingsBar2MenuItem.Size = New System.Drawing.Size(186, 6)
        '
        '_applyPacketSettingsMenuItem
        '
        Me._applyPacketSettingsMenuItem.Name = "_applyPacketSettingsMenuItem"
        Me._applyPacketSettingsMenuItem.Size = New System.Drawing.Size(189, 22)
        Me._applyPacketSettingsMenuItem.Text = "Apply &Packet Settings"
        '
        '_devicesMenu
        '
        Me._devicesMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._selectAccutrackerMenuItem, Me._selectDghSensorsMenuItem, Me._selectDominoAmjetMenuItem})
        Me._devicesMenu.Name = "_devicesMenu"
        Me._devicesMenu.Size = New System.Drawing.Size(56, 20)
        Me._devicesMenu.Text = "&Devices"
        '
        '_selectAccutrackerMenuItem
        '
        Me._selectAccutrackerMenuItem.Name = "_selectAccutrackerMenuItem"
        Me._selectAccutrackerMenuItem.Size = New System.Drawing.Size(181, 22)
        Me._selectAccutrackerMenuItem.Text = "Acc&utracker II"
        '
        '_selectDghSensorsMenuItem
        '
        Me._selectDghSensorsMenuItem.Name = "_selectDghSensorsMenuItem"
        Me._selectDghSensorsMenuItem.Size = New System.Drawing.Size(181, 22)
        Me._selectDghSensorsMenuItem.Text = "&DGH Sensors"
        '
        '_selectDominoAmjetMenuItem
        '
        Me._selectDominoAmjetMenuItem.Name = "_selectDominoAmjetMenuItem"
        Me._selectDominoAmjetMenuItem.Size = New System.Drawing.Size(181, 22)
        Me._selectDominoAmjetMenuItem.Text = "D&omino-Amjet Printers"
        '
        '_dghDeviceMenu
        '
        Me._dghDeviceMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._readDghDataMenuItem, Me._readAndZeroDghMenuItem})
        Me._dghDeviceMenu.Name = "_dghDeviceMenu"
        Me._dghDeviceMenu.Size = New System.Drawing.Size(40, 20)
        Me._dghDeviceMenu.Text = "DG&H"
        Me._dghDeviceMenu.Visible = False
        '
        '_readDghDataMenuItem
        '
        Me._readDghDataMenuItem.Name = "_readDghDataMenuItem"
        Me._readDghDataMenuItem.Size = New System.Drawing.Size(125, 22)
        Me._readDghDataMenuItem.Text = "Read &Data"
        '
        '_readAndZeroDghMenuItem
        '
        Me._readAndZeroDghMenuItem.Name = "_readAndZeroDghMenuItem"
        Me._readAndZeroDghMenuItem.Size = New System.Drawing.Size(125, 22)
        Me._readAndZeroDghMenuItem.Text = "Read &Zero"
        '
        '_dominoDeviceMenu1
        '
        Me._dominoDeviceMenu1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._dominoIdentifyMenuItem, Me._dominoGetConfigMenuItem, Me._dominoGetStatusMenuItem, Me._dominoSaveMessage1MenuItem, Me._dominoSelectMessage1MenuItem, Me._dominoEnableHead1MenuItem, Me._dominoPrintGoMenuItem})
        Me._dominoDeviceMenu1.Name = "_dominoDeviceMenu1"
        Me._dominoDeviceMenu1.Size = New System.Drawing.Size(54, 20)
        Me._dominoDeviceMenu1.Text = "Domin&o"
        Me._dominoDeviceMenu1.Visible = False
        '
        '_dominoIdentifyMenuItem
        '
        Me._dominoIdentifyMenuItem.Name = "_dominoIdentifyMenuItem"
        Me._dominoIdentifyMenuItem.Size = New System.Drawing.Size(159, 22)
        Me._dominoIdentifyMenuItem.Text = "Get &Identity"
        '
        '_dominoGetConfigMenuItem
        '
        Me._dominoGetConfigMenuItem.Name = "_dominoGetConfigMenuItem"
        Me._dominoGetConfigMenuItem.Size = New System.Drawing.Size(159, 22)
        Me._dominoGetConfigMenuItem.Text = "Get &Configuration"
        '
        '_dominoGetStatusMenuItem
        '
        Me._dominoGetStatusMenuItem.Name = "_dominoGetStatusMenuItem"
        Me._dominoGetStatusMenuItem.Size = New System.Drawing.Size(159, 22)
        Me._dominoGetStatusMenuItem.Text = "Get Stat&us"
        '
        '_dominoSaveMessage1MenuItem
        '
        Me._dominoSaveMessage1MenuItem.Name = "_dominoSaveMessage1MenuItem"
        Me._dominoSaveMessage1MenuItem.Size = New System.Drawing.Size(159, 22)
        Me._dominoSaveMessage1MenuItem.Text = "&Save Message 1"
        '
        '_dominoSelectMessage1MenuItem
        '
        Me._dominoSelectMessage1MenuItem.Name = "_dominoSelectMessage1MenuItem"
        Me._dominoSelectMessage1MenuItem.Size = New System.Drawing.Size(159, 22)
        Me._dominoSelectMessage1MenuItem.Text = "Se&lect Message 1"
        '
        '_dominoEnableHead1MenuItem
        '
        Me._dominoEnableHead1MenuItem.Name = "_dominoEnableHead1MenuItem"
        Me._dominoEnableHead1MenuItem.Size = New System.Drawing.Size(159, 22)
        Me._dominoEnableHead1MenuItem.Text = "&Enable Head 1"
        '
        '_dominoPrintGoMenuItem
        '
        Me._dominoPrintGoMenuItem.Name = "_dominoPrintGoMenuItem"
        Me._dominoPrintGoMenuItem.Size = New System.Drawing.Size(159, 22)
        Me._dominoPrintGoMenuItem.Text = "&Print Go"
        '
        '_accutrackerDevceMenu
        '
        Me._accutrackerDevceMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me._accutrackerStartMenuItem, Me._accutrackerReadMenuItem})
        Me._accutrackerDevceMenu.Name = "_accutrackerDevceMenu"
        Me._accutrackerDevceMenu.Size = New System.Drawing.Size(87, 20)
        Me._accutrackerDevceMenu.Text = "Acc&utracker II"
        Me._accutrackerDevceMenu.Visible = False
        '
        '_accutrackerStartMenuItem
        '
        Me._accutrackerStartMenuItem.Name = "_accutrackerStartMenuItem"
        Me._accutrackerStartMenuItem.Size = New System.Drawing.Size(99, 22)
        Me._accutrackerStartMenuItem.Text = "&Start"
        '
        '_accutrackerReadMenuItem
        '
        Me._accutrackerReadMenuItem.Name = "_accutrackerReadMenuItem"
        Me._accutrackerReadMenuItem.Size = New System.Drawing.Size(99, 22)
        Me._accutrackerReadMenuItem.Text = "&Read"
        '
        '_statusBar
        '
        Me._statusBar.BackColor = System.Drawing.Color.Transparent
        Me._statusBar.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._statusBar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me._statusPanel, Me._timePanel, Me._statePanel})
        Me._statusBar.Location = New System.Drawing.Point(0, 505)
        Me._statusBar.Name = "_statusBar"
        Me._statusBar.Size = New System.Drawing.Size(493, 25)
        Me._statusBar.TabIndex = 90
        '
        '_statusPanel
        '
        Me._statusPanel.AutoSize = False
        Me._statusPanel.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) Or
                                              System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) Or
                                          System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me._statusPanel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter
        Me._statusPanel.Margin = New System.Windows.Forms.Padding(0)
        Me._statusPanel.Name = "_statusPanel"
        Me._statusPanel.Size = New System.Drawing.Size(409, 25)
        Me._statusPanel.Spring = True
        Me._statusPanel.Text = "Status"
        Me._statusPanel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me._statusPanel.ToolTipText = "Status"
        '
        '_timePanel
        '
        Me._timePanel.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) Or
                                            System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) Or
                                        System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me._timePanel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter
        Me._timePanel.Margin = New System.Windows.Forms.Padding(0)
        Me._timePanel.Name = "_timePanel"
        Me._timePanel.Size = New System.Drawing.Size(33, 25)
        Me._timePanel.Text = "Time"
        Me._timePanel.ToolTipText = "Time"
        '
        '_statePanel
        '
        Me._statePanel.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) Or
                                             System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) Or
                                         System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me._statePanel.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenOuter
        Me._statePanel.Margin = New System.Windows.Forms.Padding(0)
        Me._statePanel.Name = "_statePanel"
        Me._statePanel.Size = New System.Drawing.Size(36, 25)
        Me._statePanel.Text = "State"
        Me._statePanel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me._statePanel.ToolTipText = "State"
        '
        '_tabControl
        '
        Me._tabControl.Controls.Add(Me._dataTabPage)
        Me._tabControl.Controls.Add(Me._packetSettingsTabPage)
        Me._tabControl.Controls.Add(Me._portSettingsTabPage)
        Me._tabControl.Controls.Add(Me._MessagesTabPage)
        Me._tabControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me._tabControl.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._tabControl.ItemSize = New System.Drawing.Size(42, 18)
        Me._tabControl.Location = New System.Drawing.Point(0, 24)
        Me._tabControl.Name = "_tabControl"
        Me._tabControl.SelectedIndex = 0
        Me._tabControl.Size = New System.Drawing.Size(493, 445)
        Me._tabControl.TabIndex = 0
        '
        '_dataTabPage
        '
        Me._dataTabPage.Controls.Add(Me._dataLayout)
        Me._dataTabPage.Location = New System.Drawing.Point(4, 22)
        Me._dataTabPage.Name = "_dataTabPage"
        Me._dataTabPage.Size = New System.Drawing.Size(485, 419)
        Me._dataTabPage.TabIndex = 0
        Me._dataTabPage.Text = "DATA"
        Me._dataTabPage.UseVisualStyleBackColor = True
        '
        '_dataLayout
        '
        Me._dataLayout.ColumnCount = 1
        Me._dataLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._dataLayout.Controls.Add(Me._terminalTextBox, 0, 0)
        Me._dataLayout.Controls.Add(Me._txPanel, 0, 1)
        Me._dataLayout.Controls.Add(Me._rxPanel, 0, 2)
        Me._dataLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._dataLayout.Location = New System.Drawing.Point(0, 0)
        Me._dataLayout.Name = "_dataLayout"
        Me._dataLayout.RowCount = 3
        Me._dataLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._dataLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me._dataLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27.0!))
        Me._dataLayout.Size = New System.Drawing.Size(485, 419)
        Me._dataLayout.TabIndex = 4
        '
        '_terminalTextBox
        '
        Me._terminalTextBox.AcceptsReturn = True
        Me._terminalTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._terminalTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._terminalTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._terminalTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._terminalTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._terminalTextBox.Location = New System.Drawing.Point(3, 3)
        Me._terminalTextBox.MaxLength = 8192
        Me._terminalTextBox.Multiline = True
        Me._terminalTextBox.Name = "_terminalTextBox"
        Me._terminalTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._terminalTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._terminalTextBox.Size = New System.Drawing.Size(479, 359)
        Me._terminalTextBox.TabIndex = 4
        Me._terminalTextBox.WordWrap = False
        '
        '_txPanel
        '
        Me._txPanel.Controls.Add(Me._sendTextBox)
        Me._txPanel.Controls.Add(Me._sendTextBoxLabel)
        Me._txPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._txPanel.Location = New System.Drawing.Point(3, 368)
        Me._txPanel.Name = "_txPanel"
        Me._txPanel.Size = New System.Drawing.Size(479, 21)
        Me._txPanel.TabIndex = 2
        '
        '_sendTextBoxLabel
        '
        Me._sendTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._sendTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._sendTextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._sendTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._sendTextBoxLabel.Location = New System.Drawing.Point(1, 4)
        Me._sendTextBoxLabel.Name = "_sendTextBoxLabel"
        Me._sendTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._sendTextBoxLabel.Size = New System.Drawing.Size(20, 13)
        Me._sendTextBoxLabel.TabIndex = 6
        Me._sendTextBoxLabel.Text = "Tx: "
        Me._sendTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_rxPanel
        '
        Me._rxPanel.Controls.Add(Me._receiveTextBox)
        Me._rxPanel.Controls.Add(Me._receiveTextBoxLabel)
        Me._rxPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._rxPanel.Location = New System.Drawing.Point(3, 395)
        Me._rxPanel.Name = "_rxPanel"
        Me._rxPanel.Size = New System.Drawing.Size(479, 21)
        Me._rxPanel.TabIndex = 3
        '
        '_receiveTextBoxLabel
        '
        Me._receiveTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._receiveTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._receiveTextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._receiveTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._receiveTextBoxLabel.Location = New System.Drawing.Point(1, 4)
        Me._receiveTextBoxLabel.Name = "_receiveTextBoxLabel"
        Me._receiveTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._receiveTextBoxLabel.Size = New System.Drawing.Size(20, 13)
        Me._receiveTextBoxLabel.TabIndex = 5
        Me._receiveTextBoxLabel.Text = "Rx: "
        Me._receiveTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_packetSettingsTabPage
        '
        Me._packetSettingsTabPage.Controls.Add(Me._packetSettingsLayout)
        Me._packetSettingsTabPage.Location = New System.Drawing.Point(4, 22)
        Me._packetSettingsTabPage.Name = "_packetSettingsTabPage"
        Me._packetSettingsTabPage.Size = New System.Drawing.Size(485, 419)
        Me._packetSettingsTabPage.TabIndex = 1
        Me._packetSettingsTabPage.Text = "PACKET SETTINGS"
        Me._packetSettingsTabPage.UseVisualStyleBackColor = True
        '
        '_packetSettingsLayout
        '
        Me._packetSettingsLayout.ColumnCount = 3
        Me._packetSettingsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._packetSettingsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._packetSettingsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._packetSettingsLayout.Controls.Add(Me._packetSettingsFrame, 1, 1)
        Me._packetSettingsLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._packetSettingsLayout.Location = New System.Drawing.Point(0, 0)
        Me._packetSettingsLayout.Name = "_packetSettingsLayout"
        Me._packetSettingsLayout.RowCount = 3
        Me._packetSettingsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._packetSettingsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me._packetSettingsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._packetSettingsLayout.Size = New System.Drawing.Size(485, 419)
        Me._packetSettingsLayout.TabIndex = 8
        '
        '_packetSettingsFrame
        '
        Me._packetSettingsFrame.BackColor = System.Drawing.Color.Transparent
        Me._packetSettingsFrame.Controls.Add(Me._endOfPacketByte1TextBox)
        Me._packetSettingsFrame.Controls.Add(Me._endPacketByte2TextBox)
        Me._packetSettingsFrame.Controls.Add(Me._eotSubstitueTextBox)
        Me._packetSettingsFrame.Controls.Add(Me._startPacketByteTextBox)
        Me._packetSettingsFrame.Controls.Add(Me._acknowledgeByteTextBox)
        Me._packetSettingsFrame.Controls.Add(Me._notAcknowledgeByteTextBox)
        Me._packetSettingsFrame.Controls.Add(Me._nakMessageLengthTextBox)
        Me._packetSettingsFrame.Controls.Add(Me._packetLengthTextBox)
        Me._packetSettingsFrame.Controls.Add(Me._replaceEotChecklBox)
        Me._packetSettingsFrame.Controls.Add(Me._enquiryByteTextBox)
        Me._packetSettingsFrame.Controls.Add(Me._endOfTextByteTextBox)
        Me._packetSettingsFrame.Controls.Add(Me._startOfTextByteTextBox)
        Me._packetSettingsFrame.Controls.Add(Me.EndOfPacketByte1TextBoxLabel)
        Me._packetSettingsFrame.Controls.Add(Me.EndOfPacketByte2TextBoxLabel)
        Me._packetSettingsFrame.Controls.Add(Me.EotSubstitueTextBoxLabel)
        Me._packetSettingsFrame.Controls.Add(Me.startOfPacketByteTextBoxLabel)
        Me._packetSettingsFrame.Controls.Add(Me.AcknowledgeByteTextBoxLabel)
        Me._packetSettingsFrame.Controls.Add(Me.NotAcknowledgeByteTextBoxLabel)
        Me._packetSettingsFrame.Controls.Add(Me.NakMessageLengthTextBoxLabel)
        Me._packetSettingsFrame.Controls.Add(Me.PacketLengthTextBoxLabel)
        Me._packetSettingsFrame.Controls.Add(Me.PacketActualLabel)
        Me._packetSettingsFrame.Controls.Add(Me.PacketRecLabel)
        Me._packetSettingsFrame.Controls.Add(Me.EndOfPacketByte1RecLabel)
        Me._packetSettingsFrame.Controls.Add(Me.EndOfPacketByte2RecLabel)
        Me._packetSettingsFrame.Controls.Add(Me.ReplaceEotRecLabel)
        Me._packetSettingsFrame.Controls.Add(Me.EotSubstitueRecLabel)
        Me._packetSettingsFrame.Controls.Add(Me.startOfPacketByteRecLabel)
        Me._packetSettingsFrame.Controls.Add(Me.AcknowledgeByteRecLabel)
        Me._packetSettingsFrame.Controls.Add(Me.NotAcknowledgeByteRecLabel)
        Me._packetSettingsFrame.Controls.Add(Me.NakMessageLengthRecLabel)
        Me._packetSettingsFrame.Controls.Add(Me.PacketLengthRecLabel)
        Me._packetSettingsFrame.Controls.Add(Me.EndOfPacketByte1InfoLabel)
        Me._packetSettingsFrame.Controls.Add(Me.EndOfPacketByte2InfoLabel)
        Me._packetSettingsFrame.Controls.Add(Me.startOfPacketByteInfoLabel)
        Me._packetSettingsFrame.Controls.Add(Me.AcknowledgeByteInfoLabel)
        Me._packetSettingsFrame.Controls.Add(Me.EnquiryByteRecLabel)
        Me._packetSettingsFrame.Controls.Add(Me.EndOfTextByteRecLabel)
        Me._packetSettingsFrame.Controls.Add(Me.StartOfTextByteRecLabel)
        Me._packetSettingsFrame.Controls.Add(Me.EnquiryByteTextBoxLabel)
        Me._packetSettingsFrame.Controls.Add(Me.EndOfTextByteTextBoxLabel)
        Me._packetSettingsFrame.Controls.Add(Me.StartOfTextByteTextBoxLabel)
        Me._packetSettingsFrame.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._packetSettingsFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me._packetSettingsFrame.Location = New System.Drawing.Point(41, 51)
        Me._packetSettingsFrame.Name = "_packetSettingsFrame"
        Me._packetSettingsFrame.Padding = New System.Windows.Forms.Padding(0)
        Me._packetSettingsFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._packetSettingsFrame.Size = New System.Drawing.Size(403, 317)
        Me._packetSettingsFrame.TabIndex = 7
        Me._packetSettingsFrame.TabStop = False
        Me._packetSettingsFrame.Text = "Packet Settings"
        '
        '_endOfPacketByte1TextBox
        '
        Me._endOfPacketByte1TextBox.AcceptsReturn = True
        Me._endOfPacketByte1TextBox.BackColor = System.Drawing.SystemColors.Window
        Me._endOfPacketByte1TextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._endOfPacketByte1TextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._endOfPacketByte1TextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._endOfPacketByte1TextBox.Location = New System.Drawing.Point(157, 34)
        Me._endOfPacketByte1TextBox.MaxLength = 0
        Me._endOfPacketByte1TextBox.Name = "_endOfPacketByte1TextBox"
        Me._endOfPacketByte1TextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._endOfPacketByte1TextBox.Size = New System.Drawing.Size(100, 20)
        Me._endOfPacketByte1TextBox.TabIndex = 19
        '
        '_endPacketByte2TextBox
        '
        Me._endPacketByte2TextBox.AcceptsReturn = True
        Me._endPacketByte2TextBox.BackColor = System.Drawing.SystemColors.Window
        Me._endPacketByte2TextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._endPacketByte2TextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._endPacketByte2TextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._endPacketByte2TextBox.Location = New System.Drawing.Point(157, 54)
        Me._endPacketByte2TextBox.MaxLength = 0
        Me._endPacketByte2TextBox.Name = "_endPacketByte2TextBox"
        Me._endPacketByte2TextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._endPacketByte2TextBox.Size = New System.Drawing.Size(100, 20)
        Me._endPacketByte2TextBox.TabIndex = 18
        '
        '_eotSubstitueTextBox
        '
        Me._eotSubstitueTextBox.AcceptsReturn = True
        Me._eotSubstitueTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._eotSubstitueTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._eotSubstitueTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._eotSubstitueTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._eotSubstitueTextBox.Location = New System.Drawing.Point(157, 94)
        Me._eotSubstitueTextBox.MaxLength = 0
        Me._eotSubstitueTextBox.Name = "_eotSubstitueTextBox"
        Me._eotSubstitueTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._eotSubstitueTextBox.Size = New System.Drawing.Size(100, 20)
        Me._eotSubstitueTextBox.TabIndex = 17
        '
        '_startPacketByteTextBox
        '
        Me._startPacketByteTextBox.AcceptsReturn = True
        Me._startPacketByteTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._startPacketByteTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._startPacketByteTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._startPacketByteTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._startPacketByteTextBox.Location = New System.Drawing.Point(157, 114)
        Me._startPacketByteTextBox.MaxLength = 0
        Me._startPacketByteTextBox.Name = "_startPacketByteTextBox"
        Me._startPacketByteTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._startPacketByteTextBox.Size = New System.Drawing.Size(100, 20)
        Me._startPacketByteTextBox.TabIndex = 16
        '
        '_acknowledgeByteTextBox
        '
        Me._acknowledgeByteTextBox.AcceptsReturn = True
        Me._acknowledgeByteTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._acknowledgeByteTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._acknowledgeByteTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._acknowledgeByteTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._acknowledgeByteTextBox.Location = New System.Drawing.Point(157, 134)
        Me._acknowledgeByteTextBox.MaxLength = 0
        Me._acknowledgeByteTextBox.Name = "_acknowledgeByteTextBox"
        Me._acknowledgeByteTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._acknowledgeByteTextBox.Size = New System.Drawing.Size(100, 20)
        Me._acknowledgeByteTextBox.TabIndex = 15
        '
        '_notAcknowledgeByteTextBox
        '
        Me._notAcknowledgeByteTextBox.AcceptsReturn = True
        Me._notAcknowledgeByteTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._notAcknowledgeByteTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._notAcknowledgeByteTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._notAcknowledgeByteTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._notAcknowledgeByteTextBox.Location = New System.Drawing.Point(157, 154)
        Me._notAcknowledgeByteTextBox.MaxLength = 0
        Me._notAcknowledgeByteTextBox.Name = "_notAcknowledgeByteTextBox"
        Me._notAcknowledgeByteTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._notAcknowledgeByteTextBox.Size = New System.Drawing.Size(100, 20)
        Me._notAcknowledgeByteTextBox.TabIndex = 14
        '
        '_nakMessageLengthTextBox
        '
        Me._nakMessageLengthTextBox.AcceptsReturn = True
        Me._nakMessageLengthTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._nakMessageLengthTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._nakMessageLengthTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._nakMessageLengthTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._nakMessageLengthTextBox.Location = New System.Drawing.Point(157, 174)
        Me._nakMessageLengthTextBox.MaxLength = 0
        Me._nakMessageLengthTextBox.Name = "_nakMessageLengthTextBox"
        Me._nakMessageLengthTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._nakMessageLengthTextBox.Size = New System.Drawing.Size(100, 20)
        Me._nakMessageLengthTextBox.TabIndex = 13
        '
        '_packetLengthTextBox
        '
        Me._packetLengthTextBox.AcceptsReturn = True
        Me._packetLengthTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._packetLengthTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._packetLengthTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._packetLengthTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._packetLengthTextBox.Location = New System.Drawing.Point(157, 194)
        Me._packetLengthTextBox.MaxLength = 0
        Me._packetLengthTextBox.Name = "_packetLengthTextBox"
        Me._packetLengthTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._packetLengthTextBox.Size = New System.Drawing.Size(100, 20)
        Me._packetLengthTextBox.TabIndex = 12
        '
        '_replaceEotChecklBox
        '
        Me._replaceEotChecklBox.BackColor = System.Drawing.Color.Transparent
        Me._replaceEotChecklBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._replaceEotChecklBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._replaceEotChecklBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._replaceEotChecklBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._replaceEotChecklBox.Location = New System.Drawing.Point(105, 76)
        Me._replaceEotChecklBox.Name = "_replaceEotChecklBox"
        Me._replaceEotChecklBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._replaceEotChecklBox.Size = New System.Drawing.Size(66, 19)
        Me._replaceEotChecklBox.TabIndex = 11
        Me._replaceEotChecklBox.Text = "Replace EOT: "
        Me._replaceEotChecklBox.UseVisualStyleBackColor = False
        '
        '_enquiryByteTextBox
        '
        Me._enquiryByteTextBox.AcceptsReturn = True
        Me._enquiryByteTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._enquiryByteTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._enquiryByteTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._enquiryByteTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._enquiryByteTextBox.Location = New System.Drawing.Point(157, 254)
        Me._enquiryByteTextBox.MaxLength = 0
        Me._enquiryByteTextBox.Name = "_enquiryByteTextBox"
        Me._enquiryByteTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._enquiryByteTextBox.Size = New System.Drawing.Size(100, 20)
        Me._enquiryByteTextBox.TabIndex = 10
        '
        '_endOfTextByteTextBox
        '
        Me._endOfTextByteTextBox.AcceptsReturn = True
        Me._endOfTextByteTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._endOfTextByteTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._endOfTextByteTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._endOfTextByteTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._endOfTextByteTextBox.Location = New System.Drawing.Point(157, 234)
        Me._endOfTextByteTextBox.MaxLength = 0
        Me._endOfTextByteTextBox.Name = "_endOfTextByteTextBox"
        Me._endOfTextByteTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._endOfTextByteTextBox.Size = New System.Drawing.Size(100, 20)
        Me._endOfTextByteTextBox.TabIndex = 9
        '
        '_startOfTextByteTextBox
        '
        Me._startOfTextByteTextBox.AcceptsReturn = True
        Me._startOfTextByteTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._startOfTextByteTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._startOfTextByteTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._startOfTextByteTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._startOfTextByteTextBox.Location = New System.Drawing.Point(157, 214)
        Me._startOfTextByteTextBox.MaxLength = 0
        Me._startOfTextByteTextBox.Name = "_startOfTextByteTextBox"
        Me._startOfTextByteTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._startOfTextByteTextBox.Size = New System.Drawing.Size(100, 20)
        Me._startOfTextByteTextBox.TabIndex = 8
        '
        'EndOfPacketByte1TextBoxLabel
        '
        Me.EndOfPacketByte1TextBoxLabel.AutoSize = True
        Me.EndOfPacketByte1TextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me.EndOfPacketByte1TextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.EndOfPacketByte1TextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EndOfPacketByte1TextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.EndOfPacketByte1TextBoxLabel.Location = New System.Drawing.Point(47, 36)
        Me.EndOfPacketByte1TextBoxLabel.Name = "EndOfPacketByte1TextBoxLabel"
        Me.EndOfPacketByte1TextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.EndOfPacketByte1TextBoxLabel.Size = New System.Drawing.Size(109, 14)
        Me.EndOfPacketByte1TextBoxLabel.TabIndex = 48
        Me.EndOfPacketByte1TextBoxLabel.Text = "End of Packet byte 1:"
        Me.EndOfPacketByte1TextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'EndOfPacketByte2TextBoxLabel
        '
        Me.EndOfPacketByte2TextBoxLabel.AutoSize = True
        Me.EndOfPacketByte2TextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me.EndOfPacketByte2TextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.EndOfPacketByte2TextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EndOfPacketByte2TextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.EndOfPacketByte2TextBoxLabel.Location = New System.Drawing.Point(47, 56)
        Me.EndOfPacketByte2TextBoxLabel.Name = "EndOfPacketByte2TextBoxLabel"
        Me.EndOfPacketByte2TextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.EndOfPacketByte2TextBoxLabel.Size = New System.Drawing.Size(109, 14)
        Me.EndOfPacketByte2TextBoxLabel.TabIndex = 47
        Me.EndOfPacketByte2TextBoxLabel.Text = "End of Packet byte 2:"
        Me.EndOfPacketByte2TextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'EotSubstitueTextBoxLabel
        '
        Me.EotSubstitueTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me.EotSubstitueTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.EotSubstitueTextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EotSubstitueTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.EotSubstitueTextBoxLabel.Location = New System.Drawing.Point(43, 97)
        Me.EotSubstitueTextBoxLabel.Name = "EotSubstitueTextBoxLabel"
        Me.EotSubstitueTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.EotSubstitueTextBoxLabel.Size = New System.Drawing.Size(113, 13)
        Me.EotSubstitueTextBoxLabel.TabIndex = 46
        Me.EotSubstitueTextBoxLabel.Text = "EOT Substitute String:"
        Me.EotSubstitueTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'startOfPacketByteTextBoxLabel
        '
        Me.startOfPacketByteTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me.startOfPacketByteTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.startOfPacketByteTextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.startOfPacketByteTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.startOfPacketByteTextBoxLabel.Location = New System.Drawing.Point(48, 117)
        Me.startOfPacketByteTextBoxLabel.Name = "startOfPacketByteTextBoxLabel"
        Me.startOfPacketByteTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.startOfPacketByteTextBoxLabel.Size = New System.Drawing.Size(108, 13)
        Me.startOfPacketByteTextBoxLabel.TabIndex = 45
        Me.startOfPacketByteTextBoxLabel.Text = "Start of Packet Byte:"
        Me.startOfPacketByteTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'AcknowledgeByteTextBoxLabel
        '
        Me.AcknowledgeByteTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me.AcknowledgeByteTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.AcknowledgeByteTextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AcknowledgeByteTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.AcknowledgeByteTextBoxLabel.Location = New System.Drawing.Point(52, 137)
        Me.AcknowledgeByteTextBoxLabel.Name = "AcknowledgeByteTextBoxLabel"
        Me.AcknowledgeByteTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.AcknowledgeByteTextBoxLabel.Size = New System.Drawing.Size(104, 13)
        Me.AcknowledgeByteTextBoxLabel.TabIndex = 44
        Me.AcknowledgeByteTextBoxLabel.Text = "Acknowledge Byte:"
        Me.AcknowledgeByteTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'NotAcknowledgeByteTextBoxLabel
        '
        Me.NotAcknowledgeByteTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me.NotAcknowledgeByteTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.NotAcknowledgeByteTextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NotAcknowledgeByteTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.NotAcknowledgeByteTextBoxLabel.Location = New System.Drawing.Point(7, 157)
        Me.NotAcknowledgeByteTextBoxLabel.Name = "NotAcknowledgeByteTextBoxLabel"
        Me.NotAcknowledgeByteTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.NotAcknowledgeByteTextBoxLabel.Size = New System.Drawing.Size(149, 13)
        Me.NotAcknowledgeByteTextBoxLabel.TabIndex = 43
        Me.NotAcknowledgeByteTextBoxLabel.Text = "Negative Acknowledge Byte:"
        Me.NotAcknowledgeByteTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'NakMessageLengthTextBoxLabel
        '
        Me.NakMessageLengthTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me.NakMessageLengthTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.NakMessageLengthTextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NakMessageLengthTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.NakMessageLengthTextBoxLabel.Location = New System.Drawing.Point(19, 177)
        Me.NakMessageLengthTextBoxLabel.Name = "NakMessageLengthTextBoxLabel"
        Me.NakMessageLengthTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.NakMessageLengthTextBoxLabel.Size = New System.Drawing.Size(137, 13)
        Me.NakMessageLengthTextBoxLabel.TabIndex = 42
        Me.NakMessageLengthTextBoxLabel.Text = "NAK Message Length:"
        Me.NakMessageLengthTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PacketLengthTextBoxLabel
        '
        Me.PacketLengthTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me.PacketLengthTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.PacketLengthTextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PacketLengthTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.PacketLengthTextBoxLabel.Location = New System.Drawing.Point(75, 197)
        Me.PacketLengthTextBoxLabel.Name = "PacketLengthTextBoxLabel"
        Me.PacketLengthTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PacketLengthTextBoxLabel.Size = New System.Drawing.Size(81, 13)
        Me.PacketLengthTextBoxLabel.TabIndex = 41
        Me.PacketLengthTextBoxLabel.Text = "Packet Length:"
        Me.PacketLengthTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PacketActualLabel
        '
        Me.PacketActualLabel.AutoSize = True
        Me.PacketActualLabel.BackColor = System.Drawing.Color.Transparent
        Me.PacketActualLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.PacketActualLabel.Font = New System.Drawing.Font("Arial", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PacketActualLabel.ForeColor = System.Drawing.Color.Blue
        Me.PacketActualLabel.Location = New System.Drawing.Point(188, 18)
        Me.PacketActualLabel.Name = "PacketActualLabel"
        Me.PacketActualLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PacketActualLabel.Size = New System.Drawing.Size(41, 14)
        Me.PacketActualLabel.TabIndex = 40
        Me.PacketActualLabel.Text = "Actual"
        Me.PacketActualLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'PacketRecLabel
        '
        Me.PacketRecLabel.AutoSize = True
        Me.PacketRecLabel.BackColor = System.Drawing.Color.Transparent
        Me.PacketRecLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.PacketRecLabel.Font = New System.Drawing.Font("Arial", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PacketRecLabel.ForeColor = System.Drawing.Color.Blue
        Me.PacketRecLabel.Location = New System.Drawing.Point(261, 18)
        Me.PacketRecLabel.Name = "PacketRecLabel"
        Me.PacketRecLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PacketRecLabel.Size = New System.Drawing.Size(91, 14)
        Me.PacketRecLabel.TabIndex = 39
        Me.PacketRecLabel.Text = "Recommended"
        '
        'EndOfPacketByte1RecLabel
        '
        Me.EndOfPacketByte1RecLabel.AutoSize = True
        Me.EndOfPacketByte1RecLabel.BackColor = System.Drawing.Color.Transparent
        Me.EndOfPacketByte1RecLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.EndOfPacketByte1RecLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EndOfPacketByte1RecLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.EndOfPacketByte1RecLabel.Location = New System.Drawing.Point(262, 36)
        Me.EndOfPacketByte1RecLabel.Name = "EndOfPacketByte1RecLabel"
        Me.EndOfPacketByte1RecLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.EndOfPacketByte1RecLabel.Size = New System.Drawing.Size(19, 14)
        Me.EndOfPacketByte1RecLabel.TabIndex = 38
        Me.EndOfPacketByte1RecLabel.Text = "13"
        Me.EndOfPacketByte1RecLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EndOfPacketByte2RecLabel
        '
        Me.EndOfPacketByte2RecLabel.BackColor = System.Drawing.Color.Transparent
        Me.EndOfPacketByte2RecLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.EndOfPacketByte2RecLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EndOfPacketByte2RecLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.EndOfPacketByte2RecLabel.Location = New System.Drawing.Point(262, 57)
        Me.EndOfPacketByte2RecLabel.Name = "EndOfPacketByte2RecLabel"
        Me.EndOfPacketByte2RecLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.EndOfPacketByte2RecLabel.Size = New System.Drawing.Size(46, 13)
        Me.EndOfPacketByte2RecLabel.TabIndex = 37
        Me.EndOfPacketByte2RecLabel.Text = "10"
        Me.EndOfPacketByte2RecLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'ReplaceEotRecLabel
        '
        Me.ReplaceEotRecLabel.BackColor = System.Drawing.Color.Transparent
        Me.ReplaceEotRecLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.ReplaceEotRecLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ReplaceEotRecLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ReplaceEotRecLabel.Location = New System.Drawing.Point(262, 77)
        Me.ReplaceEotRecLabel.Name = "ReplaceEotRecLabel"
        Me.ReplaceEotRecLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ReplaceEotRecLabel.Size = New System.Drawing.Size(57, 13)
        Me.ReplaceEotRecLabel.TabIndex = 36
        Me.ReplaceEotRecLabel.Text = "Checked"
        Me.ReplaceEotRecLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EotSubstitueRecLabel
        '
        Me.EotSubstitueRecLabel.BackColor = System.Drawing.Color.Transparent
        Me.EotSubstitueRecLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.EotSubstitueRecLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EotSubstitueRecLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.EotSubstitueRecLabel.Location = New System.Drawing.Point(262, 97)
        Me.EotSubstitueRecLabel.Name = "EotSubstitueRecLabel"
        Me.EotSubstitueRecLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.EotSubstitueRecLabel.Size = New System.Drawing.Size(46, 13)
        Me.EotSubstitueRecLabel.TabIndex = 35
        Me.EotSubstitueRecLabel.Text = "Empty"
        Me.EotSubstitueRecLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'startOfPacketByteRecLabel
        '
        Me.startOfPacketByteRecLabel.BackColor = System.Drawing.Color.Transparent
        Me.startOfPacketByteRecLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.startOfPacketByteRecLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.startOfPacketByteRecLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.startOfPacketByteRecLabel.Location = New System.Drawing.Point(262, 117)
        Me.startOfPacketByteRecLabel.Name = "startOfPacketByteRecLabel"
        Me.startOfPacketByteRecLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.startOfPacketByteRecLabel.Size = New System.Drawing.Size(46, 13)
        Me.startOfPacketByteRecLabel.TabIndex = 34
        Me.startOfPacketByteRecLabel.Text = "27"
        Me.startOfPacketByteRecLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'AcknowledgeByteRecLabel
        '
        Me.AcknowledgeByteRecLabel.BackColor = System.Drawing.Color.Transparent
        Me.AcknowledgeByteRecLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.AcknowledgeByteRecLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AcknowledgeByteRecLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.AcknowledgeByteRecLabel.Location = New System.Drawing.Point(262, 137)
        Me.AcknowledgeByteRecLabel.Name = "AcknowledgeByteRecLabel"
        Me.AcknowledgeByteRecLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.AcknowledgeByteRecLabel.Size = New System.Drawing.Size(46, 13)
        Me.AcknowledgeByteRecLabel.TabIndex = 33
        Me.AcknowledgeByteRecLabel.Text = "6"
        Me.AcknowledgeByteRecLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'NotAcknowledgeByteRecLabel
        '
        Me.NotAcknowledgeByteRecLabel.BackColor = System.Drawing.Color.Transparent
        Me.NotAcknowledgeByteRecLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.NotAcknowledgeByteRecLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NotAcknowledgeByteRecLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.NotAcknowledgeByteRecLabel.Location = New System.Drawing.Point(262, 157)
        Me.NotAcknowledgeByteRecLabel.Name = "NotAcknowledgeByteRecLabel"
        Me.NotAcknowledgeByteRecLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.NotAcknowledgeByteRecLabel.Size = New System.Drawing.Size(46, 13)
        Me.NotAcknowledgeByteRecLabel.TabIndex = 32
        Me.NotAcknowledgeByteRecLabel.Text = "21"
        Me.NotAcknowledgeByteRecLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'NakMessageLengthRecLabel
        '
        Me.NakMessageLengthRecLabel.BackColor = System.Drawing.Color.Transparent
        Me.NakMessageLengthRecLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.NakMessageLengthRecLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.NakMessageLengthRecLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.NakMessageLengthRecLabel.Location = New System.Drawing.Point(262, 177)
        Me.NakMessageLengthRecLabel.Name = "NakMessageLengthRecLabel"
        Me.NakMessageLengthRecLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.NakMessageLengthRecLabel.Size = New System.Drawing.Size(46, 13)
        Me.NakMessageLengthRecLabel.TabIndex = 31
        Me.NakMessageLengthRecLabel.Text = "3"
        Me.NakMessageLengthRecLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'PacketLengthRecLabel
        '
        Me.PacketLengthRecLabel.BackColor = System.Drawing.Color.Transparent
        Me.PacketLengthRecLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.PacketLengthRecLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PacketLengthRecLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.PacketLengthRecLabel.Location = New System.Drawing.Point(262, 197)
        Me.PacketLengthRecLabel.Name = "PacketLengthRecLabel"
        Me.PacketLengthRecLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.PacketLengthRecLabel.Size = New System.Drawing.Size(46, 13)
        Me.PacketLengthRecLabel.TabIndex = 30
        Me.PacketLengthRecLabel.Text = "0"
        Me.PacketLengthRecLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EndOfPacketByte1InfoLabel
        '
        Me.EndOfPacketByte1InfoLabel.BackColor = System.Drawing.Color.Transparent
        Me.EndOfPacketByte1InfoLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.EndOfPacketByte1InfoLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EndOfPacketByte1InfoLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.EndOfPacketByte1InfoLabel.Location = New System.Drawing.Point(305, 37)
        Me.EndOfPacketByte1InfoLabel.Name = "EndOfPacketByte1InfoLabel"
        Me.EndOfPacketByte1InfoLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.EndOfPacketByte1InfoLabel.Size = New System.Drawing.Size(96, 13)
        Me.EndOfPacketByte1InfoLabel.TabIndex = 29
        Me.EndOfPacketByte1InfoLabel.Text = "e.g., CR or EOT"
        Me.EndOfPacketByte1InfoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EndOfPacketByte2InfoLabel
        '
        Me.EndOfPacketByte2InfoLabel.BackColor = System.Drawing.Color.Transparent
        Me.EndOfPacketByte2InfoLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.EndOfPacketByte2InfoLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EndOfPacketByte2InfoLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.EndOfPacketByte2InfoLabel.Location = New System.Drawing.Point(305, 57)
        Me.EndOfPacketByte2InfoLabel.Name = "EndOfPacketByte2InfoLabel"
        Me.EndOfPacketByte2InfoLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.EndOfPacketByte2InfoLabel.Size = New System.Drawing.Size(96, 13)
        Me.EndOfPacketByte2InfoLabel.TabIndex = 28
        Me.EndOfPacketByte2InfoLabel.Text = "e.g., LF"
        Me.EndOfPacketByte2InfoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'startOfPacketByteInfoLabel
        '
        Me.startOfPacketByteInfoLabel.BackColor = System.Drawing.Color.Transparent
        Me.startOfPacketByteInfoLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.startOfPacketByteInfoLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.startOfPacketByteInfoLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.startOfPacketByteInfoLabel.Location = New System.Drawing.Point(305, 117)
        Me.startOfPacketByteInfoLabel.Name = "startOfPacketByteInfoLabel"
        Me.startOfPacketByteInfoLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.startOfPacketByteInfoLabel.Size = New System.Drawing.Size(96, 13)
        Me.startOfPacketByteInfoLabel.TabIndex = 27
        Me.startOfPacketByteInfoLabel.Text = "e.g., ESC or STX"
        Me.startOfPacketByteInfoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'AcknowledgeByteInfoLabel
        '
        Me.AcknowledgeByteInfoLabel.BackColor = System.Drawing.Color.Transparent
        Me.AcknowledgeByteInfoLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.AcknowledgeByteInfoLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AcknowledgeByteInfoLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.AcknowledgeByteInfoLabel.Location = New System.Drawing.Point(305, 137)
        Me.AcknowledgeByteInfoLabel.Name = "AcknowledgeByteInfoLabel"
        Me.AcknowledgeByteInfoLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.AcknowledgeByteInfoLabel.Size = New System.Drawing.Size(96, 13)
        Me.AcknowledgeByteInfoLabel.TabIndex = 26
        Me.AcknowledgeByteInfoLabel.Text = "e.g., NAK or '*'"
        Me.AcknowledgeByteInfoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EnquiryByteRecLabel
        '
        Me.EnquiryByteRecLabel.BackColor = System.Drawing.Color.Transparent
        Me.EnquiryByteRecLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.EnquiryByteRecLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EnquiryByteRecLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.EnquiryByteRecLabel.Location = New System.Drawing.Point(262, 257)
        Me.EnquiryByteRecLabel.Name = "EnquiryByteRecLabel"
        Me.EnquiryByteRecLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.EnquiryByteRecLabel.Size = New System.Drawing.Size(46, 13)
        Me.EnquiryByteRecLabel.TabIndex = 25
        Me.EnquiryByteRecLabel.Text = "5"
        Me.EnquiryByteRecLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EndOfTextByteRecLabel
        '
        Me.EndOfTextByteRecLabel.BackColor = System.Drawing.Color.Transparent
        Me.EndOfTextByteRecLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.EndOfTextByteRecLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EndOfTextByteRecLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.EndOfTextByteRecLabel.Location = New System.Drawing.Point(262, 237)
        Me.EndOfTextByteRecLabel.Name = "EndOfTextByteRecLabel"
        Me.EndOfTextByteRecLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.EndOfTextByteRecLabel.Size = New System.Drawing.Size(46, 13)
        Me.EndOfTextByteRecLabel.TabIndex = 24
        Me.EndOfTextByteRecLabel.Text = "3"
        Me.EndOfTextByteRecLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'StartOfTextByteRecLabel
        '
        Me.StartOfTextByteRecLabel.BackColor = System.Drawing.Color.Transparent
        Me.StartOfTextByteRecLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.StartOfTextByteRecLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StartOfTextByteRecLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.StartOfTextByteRecLabel.Location = New System.Drawing.Point(262, 217)
        Me.StartOfTextByteRecLabel.Name = "StartOfTextByteRecLabel"
        Me.StartOfTextByteRecLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartOfTextByteRecLabel.Size = New System.Drawing.Size(46, 13)
        Me.StartOfTextByteRecLabel.TabIndex = 23
        Me.StartOfTextByteRecLabel.Text = "2"
        Me.StartOfTextByteRecLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'EnquiryByteTextBoxLabel
        '
        Me.EnquiryByteTextBoxLabel.AutoSize = True
        Me.EnquiryByteTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me.EnquiryByteTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.EnquiryByteTextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EnquiryByteTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.EnquiryByteTextBoxLabel.Location = New System.Drawing.Point(53, 257)
        Me.EnquiryByteTextBoxLabel.Name = "EnquiryByteTextBoxLabel"
        Me.EnquiryByteTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.EnquiryByteTextBoxLabel.Size = New System.Drawing.Size(103, 14)
        Me.EnquiryByteTextBoxLabel.TabIndex = 22
        Me.EnquiryByteTextBoxLabel.Text = "Enquiry Byte (ENQ):"
        Me.EnquiryByteTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'EndOfTextByteTextBoxLabel
        '
        Me.EndOfTextByteTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me.EndOfTextByteTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.EndOfTextByteTextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EndOfTextByteTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.EndOfTextByteTextBoxLabel.Location = New System.Drawing.Point(32, 237)
        Me.EndOfTextByteTextBoxLabel.Name = "EndOfTextByteTextBoxLabel"
        Me.EndOfTextByteTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.EndOfTextByteTextBoxLabel.Size = New System.Drawing.Size(124, 13)
        Me.EndOfTextByteTextBoxLabel.TabIndex = 21
        Me.EndOfTextByteTextBoxLabel.Text = "End of Text Byte (ETX):"
        Me.EndOfTextByteTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'StartOfTextByteTextBoxLabel
        '
        Me.StartOfTextByteTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me.StartOfTextByteTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.StartOfTextByteTextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.StartOfTextByteTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.StartOfTextByteTextBoxLabel.Location = New System.Drawing.Point(26, 217)
        Me.StartOfTextByteTextBoxLabel.Name = "StartOfTextByteTextBoxLabel"
        Me.StartOfTextByteTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartOfTextByteTextBoxLabel.Size = New System.Drawing.Size(130, 13)
        Me.StartOfTextByteTextBoxLabel.TabIndex = 20
        Me.StartOfTextByteTextBoxLabel.Text = "Start of Text Byte (STX):"
        Me.StartOfTextByteTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_portSettingsTabPage
        '
        Me._portSettingsTabPage.Controls.Add(Me._portSettingsLayout)
        Me._portSettingsTabPage.Location = New System.Drawing.Point(4, 22)
        Me._portSettingsTabPage.Name = "_portSettingsTabPage"
        Me._portSettingsTabPage.Size = New System.Drawing.Size(485, 419)
        Me._portSettingsTabPage.TabIndex = 2
        Me._portSettingsTabPage.Text = "PORT SETTINGS"
        Me._portSettingsTabPage.UseVisualStyleBackColor = True
        '
        '_portSettingsLayout
        '
        Me._portSettingsLayout.ColumnCount = 3
        Me._portSettingsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._portSettingsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._portSettingsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._portSettingsLayout.Controls.Add(Me._settingsFrame, 1, 1)
        Me._portSettingsLayout.Dock = System.Windows.Forms.DockStyle.Fill
        Me._portSettingsLayout.Location = New System.Drawing.Point(0, 0)
        Me._portSettingsLayout.Name = "_portSettingsLayout"
        Me._portSettingsLayout.RowCount = 3
        Me._portSettingsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._portSettingsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me._portSettingsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._portSettingsLayout.Size = New System.Drawing.Size(485, 419)
        Me._portSettingsLayout.TabIndex = 50
        '
        '_settingsFrame
        '
        Me._settingsFrame.BackColor = System.Drawing.Color.Transparent
        Me._settingsFrame.Controls.Add(Me._txThresholdTextBox)
        Me._settingsFrame.Controls.Add(Me._portSettingsTextBox)
        Me._settingsFrame.Controls.Add(Me._rxThresholdTextBox)
        Me._settingsFrame.Controls.Add(Me._parityReplaceCharTextBox)
        Me._settingsFrame.Controls.Add(Me._writeBufferSizeTextBox)
        Me._settingsFrame.Controls.Add(Me._inputLengthTextBox)
        Me._settingsFrame.Controls.Add(Me._inputBufferSizeTextBox)
        Me._settingsFrame.Controls.Add(Me._portNumberTextBox)
        Me._settingsFrame.Controls.Add(Me._handshakingComboBox)
        Me._settingsFrame.Controls.Add(Me._InputModeComboBox)
        Me._settingsFrame.Controls.Add(Me._dtrEnableToggle)
        Me._settingsFrame.Controls.Add(Me._discardNullToggle)
        Me._settingsFrame.Controls.Add(Me._rtsEnableToggle)
        Me._settingsFrame.Controls.Add(Me._recommendedTxThresholdLabel)
        Me._settingsFrame.Controls.Add(Me._recommendedCommSettingsLabel)
        Me._settingsFrame.Controls.Add(Me._recommendedRtsEnableLabel)
        Me._settingsFrame.Controls.Add(Me._recommendedRxThresholdLabel)
        Me._settingsFrame.Controls.Add(Me._recommendedParityReplaceCharLabel)
        Me._settingsFrame.Controls.Add(Me._recommendedWriteBufferSizeLabel)
        Me._settingsFrame.Controls.Add(Me._recommendedDiscardNullLabel)
        Me._settingsFrame.Controls.Add(Me._recommendedInputModeLabel)
        Me._settingsFrame.Controls.Add(Me._recommendedInputLengthLabel)
        Me._settingsFrame.Controls.Add(Me.__recommendedInputBufferSizeLabel)
        Me._settingsFrame.Controls.Add(Me.__recommendedHandshakeLabel)
        Me._settingsFrame.Controls.Add(Me.__recommendedDtrEnableLabel)
        Me._settingsFrame.Controls.Add(Me._recommendedPortNumberLabel)
        Me._settingsFrame.Controls.Add(Me.RecommendedSettingTextBox)
        Me._settingsFrame.Controls.Add(Me._actualSettingsTextBoxLabel)
        Me._settingsFrame.Controls.Add(Me.TxThresholdTextBoxLabel)
        Me._settingsFrame.Controls.Add(Me.CommSettingsTextBoxLabel)
        Me._settingsFrame.Controls.Add(Me.RxThresholdTextBoxLabel)
        Me._settingsFrame.Controls.Add(Me.ParityReplaceCharTextBoxlabel)
        Me._settingsFrame.Controls.Add(Me.WriteBufferSizeTextBoxLabel)
        Me._settingsFrame.Controls.Add(Me.InputModeComboBoxLabel)
        Me._settingsFrame.Controls.Add(Me._inputLengthTextBoxLabel)
        Me._settingsFrame.Controls.Add(Me.InputBufferSizeTextBoxLabel)
        Me._settingsFrame.Controls.Add(Me._handshakeComboBoxLabel)
        Me._settingsFrame.Controls.Add(Me._commPortNumberTextBoxlabel)
        Me._settingsFrame.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._settingsFrame.ForeColor = System.Drawing.SystemColors.ControlText
        Me._settingsFrame.Location = New System.Drawing.Point(41, 51)
        Me._settingsFrame.Name = "_settingsFrame"
        Me._settingsFrame.Padding = New System.Windows.Forms.Padding(0)
        Me._settingsFrame.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._settingsFrame.Size = New System.Drawing.Size(403, 317)
        Me._settingsFrame.TabIndex = 49
        Me._settingsFrame.TabStop = False
        Me._settingsFrame.Text = "Port Settings"
        '
        '_txThresholdTextBox
        '
        Me._txThresholdTextBox.AcceptsReturn = True
        Me._txThresholdTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._txThresholdTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._txThresholdTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._txThresholdTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._txThresholdTextBox.Location = New System.Drawing.Point(157, 282)
        Me._txThresholdTextBox.MaxLength = 0
        Me._txThresholdTextBox.Name = "_txThresholdTextBox"
        Me._txThresholdTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._txThresholdTextBox.Size = New System.Drawing.Size(100, 20)
        Me._txThresholdTextBox.TabIndex = 62
        '
        '_portSettingsTextBox
        '
        Me._portSettingsTextBox.AcceptsReturn = True
        Me._portSettingsTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._portSettingsTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._portSettingsTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._portSettingsTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._portSettingsTextBox.Location = New System.Drawing.Point(157, 261)
        Me._portSettingsTextBox.MaxLength = 0
        Me._portSettingsTextBox.Name = "_portSettingsTextBox"
        Me._portSettingsTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._portSettingsTextBox.Size = New System.Drawing.Size(100, 20)
        Me._portSettingsTextBox.TabIndex = 61
        '
        '_rxThresholdTextBox
        '
        Me._rxThresholdTextBox.AcceptsReturn = True
        Me._rxThresholdTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._rxThresholdTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._rxThresholdTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._rxThresholdTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._rxThresholdTextBox.Location = New System.Drawing.Point(157, 223)
        Me._rxThresholdTextBox.MaxLength = 0
        Me._rxThresholdTextBox.Name = "_rxThresholdTextBox"
        Me._rxThresholdTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._rxThresholdTextBox.Size = New System.Drawing.Size(100, 20)
        Me._rxThresholdTextBox.TabIndex = 60
        '
        '_parityReplaceCharTextBox
        '
        Me._parityReplaceCharTextBox.AcceptsReturn = True
        Me._parityReplaceCharTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._parityReplaceCharTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._parityReplaceCharTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._parityReplaceCharTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._parityReplaceCharTextBox.Location = New System.Drawing.Point(157, 202)
        Me._parityReplaceCharTextBox.MaxLength = 0
        Me._parityReplaceCharTextBox.Name = "_parityReplaceCharTextBox"
        Me._parityReplaceCharTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._parityReplaceCharTextBox.Size = New System.Drawing.Size(100, 20)
        Me._parityReplaceCharTextBox.TabIndex = 59
        '
        '_writeBufferSizeTextBox
        '
        Me._writeBufferSizeTextBox.AcceptsReturn = True
        Me._writeBufferSizeTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._writeBufferSizeTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._writeBufferSizeTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._writeBufferSizeTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._writeBufferSizeTextBox.Location = New System.Drawing.Point(157, 181)
        Me._writeBufferSizeTextBox.MaxLength = 0
        Me._writeBufferSizeTextBox.Name = "_writeBufferSizeTextBox"
        Me._writeBufferSizeTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._writeBufferSizeTextBox.Size = New System.Drawing.Size(100, 20)
        Me._writeBufferSizeTextBox.TabIndex = 58
        '
        '_inputLengthTextBox
        '
        Me._inputLengthTextBox.AcceptsReturn = True
        Me._inputLengthTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._inputLengthTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._inputLengthTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._inputLengthTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._inputLengthTextBox.Location = New System.Drawing.Point(157, 117)
        Me._inputLengthTextBox.MaxLength = 0
        Me._inputLengthTextBox.Name = "_inputLengthTextBox"
        Me._inputLengthTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._inputLengthTextBox.Size = New System.Drawing.Size(100, 20)
        Me._inputLengthTextBox.TabIndex = 57
        '
        '_inputBufferSizeTextBox
        '
        Me._inputBufferSizeTextBox.AcceptsReturn = True
        Me._inputBufferSizeTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._inputBufferSizeTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._inputBufferSizeTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._inputBufferSizeTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._inputBufferSizeTextBox.Location = New System.Drawing.Point(157, 96)
        Me._inputBufferSizeTextBox.MaxLength = 0
        Me._inputBufferSizeTextBox.Name = "_inputBufferSizeTextBox"
        Me._inputBufferSizeTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._inputBufferSizeTextBox.Size = New System.Drawing.Size(100, 20)
        Me._inputBufferSizeTextBox.TabIndex = 56
        '
        '_portNumberTextBox
        '
        Me._portNumberTextBox.AcceptsReturn = True
        Me._portNumberTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._portNumberTextBox.Cursor = System.Windows.Forms.Cursors.IBeam
        Me._portNumberTextBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._portNumberTextBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._portNumberTextBox.Location = New System.Drawing.Point(157, 32)
        Me._portNumberTextBox.MaxLength = 0
        Me._portNumberTextBox.Name = "_portNumberTextBox"
        Me._portNumberTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._portNumberTextBox.Size = New System.Drawing.Size(100, 20)
        Me._portNumberTextBox.TabIndex = 55
        '
        '_handshakingComboBox
        '
        Me._handshakingComboBox.BackColor = System.Drawing.SystemColors.Window
        Me._handshakingComboBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._handshakingComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._handshakingComboBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._handshakingComboBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._handshakingComboBox.Items.AddRange(New Object() {"0 - None", "1 - Xon/Xoff", "2 - RTS/CTS", "3 - RTS/CTS and Xon/Xoff"})
        Me._handshakingComboBox.Location = New System.Drawing.Point(157, 73)
        Me._handshakingComboBox.Name = "_handshakingComboBox"
        Me._handshakingComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._handshakingComboBox.Size = New System.Drawing.Size(101, 22)
        Me._handshakingComboBox.TabIndex = 54
        '
        '_InputModeComboBox
        '
        Me._InputModeComboBox.BackColor = System.Drawing.SystemColors.Window
        Me._InputModeComboBox.Cursor = System.Windows.Forms.Cursors.Default
        Me._InputModeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me._InputModeComboBox.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._InputModeComboBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me._InputModeComboBox.Items.AddRange(New Object() {"0 - Text", "1 - Binary"})
        Me._InputModeComboBox.Location = New System.Drawing.Point(157, 138)
        Me._InputModeComboBox.Name = "_InputModeComboBox"
        Me._InputModeComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._InputModeComboBox.Size = New System.Drawing.Size(101, 22)
        Me._InputModeComboBox.TabIndex = 53
        '
        '_dtrEnableToggle
        '
        Me._dtrEnableToggle.BackColor = System.Drawing.Color.Transparent
        Me._dtrEnableToggle.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._dtrEnableToggle.Checked = True
        Me._dtrEnableToggle.CheckState = System.Windows.Forms.CheckState.Checked
        Me._dtrEnableToggle.Cursor = System.Windows.Forms.Cursors.Default
        Me._dtrEnableToggle.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._dtrEnableToggle.ForeColor = System.Drawing.SystemColors.ControlText
        Me._dtrEnableToggle.Location = New System.Drawing.Point(124, 54)
        Me._dtrEnableToggle.Name = "_dtrEnableToggle"
        Me._dtrEnableToggle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._dtrEnableToggle.Size = New System.Drawing.Size(47, 19)
        Me._dtrEnableToggle.TabIndex = 52
        Me._dtrEnableToggle.Text = "DTR Enable: "
        Me._dtrEnableToggle.UseVisualStyleBackColor = True
        '
        '_discardNullToggle
        '
        Me._discardNullToggle.BackColor = System.Drawing.Color.Transparent
        Me._discardNullToggle.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._discardNullToggle.Checked = True
        Me._discardNullToggle.CheckState = System.Windows.Forms.CheckState.Checked
        Me._discardNullToggle.Cursor = System.Windows.Forms.Cursors.Default
        Me._discardNullToggle.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._discardNullToggle.ForeColor = System.Drawing.SystemColors.ControlText
        Me._discardNullToggle.Location = New System.Drawing.Point(82, 162)
        Me._discardNullToggle.Name = "_discardNullToggle"
        Me._discardNullToggle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._discardNullToggle.Size = New System.Drawing.Size(89, 19)
        Me._discardNullToggle.TabIndex = 51
        Me._discardNullToggle.Text = "Null Discard: "
        Me._discardNullToggle.UseVisualStyleBackColor = True
        '
        '_rtsEnableToggle
        '
        Me._rtsEnableToggle.BackColor = System.Drawing.Color.Transparent
        Me._rtsEnableToggle.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
        Me._rtsEnableToggle.Checked = True
        Me._rtsEnableToggle.CheckState = System.Windows.Forms.CheckState.Checked
        Me._rtsEnableToggle.Cursor = System.Windows.Forms.Cursors.Default
        Me._rtsEnableToggle.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._rtsEnableToggle.ForeColor = System.Drawing.SystemColors.ControlText
        Me._rtsEnableToggle.Location = New System.Drawing.Point(124, 245)
        Me._rtsEnableToggle.Name = "_rtsEnableToggle"
        Me._rtsEnableToggle.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._rtsEnableToggle.Size = New System.Drawing.Size(47, 16)
        Me._rtsEnableToggle.TabIndex = 50
        Me._rtsEnableToggle.Text = "RTS Enable: "
        Me._rtsEnableToggle.UseVisualStyleBackColor = True
        '
        '_recommendedTxThresholdLabel
        '
        Me._recommendedTxThresholdLabel.BackColor = System.Drawing.Color.Transparent
        Me._recommendedTxThresholdLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._recommendedTxThresholdLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._recommendedTxThresholdLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me._recommendedTxThresholdLabel.Location = New System.Drawing.Point(263, 286)
        Me._recommendedTxThresholdLabel.Name = "_recommendedTxThresholdLabel"
        Me._recommendedTxThresholdLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._recommendedTxThresholdLabel.Size = New System.Drawing.Size(72, 13)
        Me._recommendedTxThresholdLabel.TabIndex = 87
        Me._recommendedTxThresholdLabel.Text = "1"
        '
        '_recommendedCommSettingsLabel
        '
        Me._recommendedCommSettingsLabel.BackColor = System.Drawing.Color.Transparent
        Me._recommendedCommSettingsLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._recommendedCommSettingsLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._recommendedCommSettingsLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me._recommendedCommSettingsLabel.Location = New System.Drawing.Point(263, 265)
        Me._recommendedCommSettingsLabel.Name = "_recommendedCommSettingsLabel"
        Me._recommendedCommSettingsLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._recommendedCommSettingsLabel.Size = New System.Drawing.Size(72, 13)
        Me._recommendedCommSettingsLabel.TabIndex = 86
        Me._recommendedCommSettingsLabel.Text = "9600, n, 8, 1"
        Me._recommendedCommSettingsLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_recommendedRtsEnableLabel
        '
        Me._recommendedRtsEnableLabel.BackColor = System.Drawing.Color.Transparent
        Me._recommendedRtsEnableLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._recommendedRtsEnableLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._recommendedRtsEnableLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me._recommendedRtsEnableLabel.Location = New System.Drawing.Point(263, 246)
        Me._recommendedRtsEnableLabel.Name = "_recommendedRtsEnableLabel"
        Me._recommendedRtsEnableLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._recommendedRtsEnableLabel.Size = New System.Drawing.Size(72, 13)
        Me._recommendedRtsEnableLabel.TabIndex = 85
        Me._recommendedRtsEnableLabel.Text = "True"
        Me._recommendedRtsEnableLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_recommendedRxThresholdLabel
        '
        Me._recommendedRxThresholdLabel.BackColor = System.Drawing.Color.Transparent
        Me._recommendedRxThresholdLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._recommendedRxThresholdLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._recommendedRxThresholdLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me._recommendedRxThresholdLabel.Location = New System.Drawing.Point(263, 227)
        Me._recommendedRxThresholdLabel.Name = "_recommendedRxThresholdLabel"
        Me._recommendedRxThresholdLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._recommendedRxThresholdLabel.Size = New System.Drawing.Size(72, 13)
        Me._recommendedRxThresholdLabel.TabIndex = 84
        Me._recommendedRxThresholdLabel.Text = "1"
        '
        '_recommendedParityReplaceCharLabel
        '
        Me._recommendedParityReplaceCharLabel.BackColor = System.Drawing.Color.Transparent
        Me._recommendedParityReplaceCharLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._recommendedParityReplaceCharLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._recommendedParityReplaceCharLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me._recommendedParityReplaceCharLabel.Location = New System.Drawing.Point(263, 206)
        Me._recommendedParityReplaceCharLabel.Name = "_recommendedParityReplaceCharLabel"
        Me._recommendedParityReplaceCharLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._recommendedParityReplaceCharLabel.Size = New System.Drawing.Size(72, 13)
        Me._recommendedParityReplaceCharLabel.TabIndex = 83
        Me._recommendedParityReplaceCharLabel.Text = "?"
        '
        '_recommendedWriteBufferSizeLabel
        '
        Me._recommendedWriteBufferSizeLabel.BackColor = System.Drawing.Color.Transparent
        Me._recommendedWriteBufferSizeLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._recommendedWriteBufferSizeLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._recommendedWriteBufferSizeLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me._recommendedWriteBufferSizeLabel.Location = New System.Drawing.Point(263, 185)
        Me._recommendedWriteBufferSizeLabel.Name = "_recommendedWriteBufferSizeLabel"
        Me._recommendedWriteBufferSizeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._recommendedWriteBufferSizeLabel.Size = New System.Drawing.Size(72, 13)
        Me._recommendedWriteBufferSizeLabel.TabIndex = 82
        Me._recommendedWriteBufferSizeLabel.Text = "512"
        Me._recommendedWriteBufferSizeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_recommendedDiscardNullLabel
        '
        Me._recommendedDiscardNullLabel.BackColor = System.Drawing.Color.Transparent
        Me._recommendedDiscardNullLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._recommendedDiscardNullLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._recommendedDiscardNullLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me._recommendedDiscardNullLabel.Location = New System.Drawing.Point(263, 164)
        Me._recommendedDiscardNullLabel.Name = "_recommendedDiscardNullLabel"
        Me._recommendedDiscardNullLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._recommendedDiscardNullLabel.Size = New System.Drawing.Size(72, 13)
        Me._recommendedDiscardNullLabel.TabIndex = 81
        Me._recommendedDiscardNullLabel.Text = "True"
        Me._recommendedDiscardNullLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_recommendedInputModeLabel
        '
        Me._recommendedInputModeLabel.BackColor = System.Drawing.Color.Transparent
        Me._recommendedInputModeLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._recommendedInputModeLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._recommendedInputModeLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me._recommendedInputModeLabel.Location = New System.Drawing.Point(263, 143)
        Me._recommendedInputModeLabel.Name = "_recommendedInputModeLabel"
        Me._recommendedInputModeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._recommendedInputModeLabel.Size = New System.Drawing.Size(77, 13)
        Me._recommendedInputModeLabel.TabIndex = 80
        Me._recommendedInputModeLabel.Text = "0 - Text"
        '
        '_recommendedInputLengthLabel
        '
        Me._recommendedInputLengthLabel.BackColor = System.Drawing.Color.Transparent
        Me._recommendedInputLengthLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._recommendedInputLengthLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._recommendedInputLengthLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me._recommendedInputLengthLabel.Location = New System.Drawing.Point(263, 121)
        Me._recommendedInputLengthLabel.Name = "_recommendedInputLengthLabel"
        Me._recommendedInputLengthLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._recommendedInputLengthLabel.Size = New System.Drawing.Size(72, 13)
        Me._recommendedInputLengthLabel.TabIndex = 79
        Me._recommendedInputLengthLabel.Text = "0"
        '
        '__recommendedInputBufferSizeLabel
        '
        Me.__recommendedInputBufferSizeLabel.BackColor = System.Drawing.Color.Transparent
        Me.__recommendedInputBufferSizeLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.__recommendedInputBufferSizeLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.__recommendedInputBufferSizeLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.__recommendedInputBufferSizeLabel.Location = New System.Drawing.Point(263, 100)
        Me.__recommendedInputBufferSizeLabel.Name = "__recommendedInputBufferSizeLabel"
        Me.__recommendedInputBufferSizeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.__recommendedInputBufferSizeLabel.Size = New System.Drawing.Size(72, 13)
        Me.__recommendedInputBufferSizeLabel.TabIndex = 78
        Me.__recommendedInputBufferSizeLabel.Text = "1024"
        Me.__recommendedInputBufferSizeLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '__recommendedHandshakeLabel
        '
        Me.__recommendedHandshakeLabel.BackColor = System.Drawing.Color.Transparent
        Me.__recommendedHandshakeLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.__recommendedHandshakeLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.__recommendedHandshakeLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.__recommendedHandshakeLabel.Location = New System.Drawing.Point(263, 78)
        Me.__recommendedHandshakeLabel.Name = "__recommendedHandshakeLabel"
        Me.__recommendedHandshakeLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.__recommendedHandshakeLabel.Size = New System.Drawing.Size(72, 13)
        Me.__recommendedHandshakeLabel.TabIndex = 77
        Me.__recommendedHandshakeLabel.Text = "2 - RTS/CTS"
        '
        '__recommendedDtrEnableLabel
        '
        Me.__recommendedDtrEnableLabel.BackColor = System.Drawing.Color.Transparent
        Me.__recommendedDtrEnableLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.__recommendedDtrEnableLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.__recommendedDtrEnableLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.__recommendedDtrEnableLabel.Location = New System.Drawing.Point(263, 57)
        Me.__recommendedDtrEnableLabel.Name = "__recommendedDtrEnableLabel"
        Me.__recommendedDtrEnableLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.__recommendedDtrEnableLabel.Size = New System.Drawing.Size(72, 13)
        Me.__recommendedDtrEnableLabel.TabIndex = 76
        Me.__recommendedDtrEnableLabel.Text = "True"
        Me.__recommendedDtrEnableLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_recommendedPortNumberLabel
        '
        Me._recommendedPortNumberLabel.BackColor = System.Drawing.Color.Transparent
        Me._recommendedPortNumberLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._recommendedPortNumberLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._recommendedPortNumberLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me._recommendedPortNumberLabel.Location = New System.Drawing.Point(263, 36)
        Me._recommendedPortNumberLabel.Name = "_recommendedPortNumberLabel"
        Me._recommendedPortNumberLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._recommendedPortNumberLabel.Size = New System.Drawing.Size(72, 13)
        Me._recommendedPortNumberLabel.TabIndex = 75
        Me._recommendedPortNumberLabel.Text = "2"
        '
        'RecommendedSettingTextBox
        '
        Me.RecommendedSettingTextBox.AutoSize = True
        Me.RecommendedSettingTextBox.BackColor = System.Drawing.Color.Transparent
        Me.RecommendedSettingTextBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.RecommendedSettingTextBox.Font = New System.Drawing.Font("Arial", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RecommendedSettingTextBox.ForeColor = System.Drawing.Color.Blue
        Me.RecommendedSettingTextBox.Location = New System.Drawing.Point(261, 16)
        Me.RecommendedSettingTextBox.Name = "RecommendedSettingTextBox"
        Me.RecommendedSettingTextBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RecommendedSettingTextBox.Size = New System.Drawing.Size(91, 14)
        Me.RecommendedSettingTextBox.TabIndex = 74
        Me.RecommendedSettingTextBox.Text = "Recommended"
        '
        '_actualSettingsTextBoxLabel
        '
        Me._actualSettingsTextBoxLabel.AutoSize = True
        Me._actualSettingsTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._actualSettingsTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._actualSettingsTextBoxLabel.Font = New System.Drawing.Font("Arial", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._actualSettingsTextBoxLabel.ForeColor = System.Drawing.Color.Blue
        Me._actualSettingsTextBoxLabel.Location = New System.Drawing.Point(188, 16)
        Me._actualSettingsTextBoxLabel.Name = "_actualSettingsTextBoxLabel"
        Me._actualSettingsTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._actualSettingsTextBoxLabel.Size = New System.Drawing.Size(41, 14)
        Me._actualSettingsTextBoxLabel.TabIndex = 73
        Me._actualSettingsTextBoxLabel.Text = "Actual"
        '
        'TxThresholdTextBoxLabel
        '
        Me.TxThresholdTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me.TxThresholdTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.TxThresholdTextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxThresholdTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.TxThresholdTextBoxLabel.Location = New System.Drawing.Point(88, 286)
        Me.TxThresholdTextBoxLabel.Name = "TxThresholdTextBoxLabel"
        Me.TxThresholdTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.TxThresholdTextBoxLabel.Size = New System.Drawing.Size(68, 13)
        Me.TxThresholdTextBoxLabel.TabIndex = 72
        Me.TxThresholdTextBoxLabel.Text = "Tx Threshold: "
        Me.TxThresholdTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CommSettingsTextBoxLabel
        '
        Me.CommSettingsTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me.CommSettingsTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.CommSettingsTextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CommSettingsTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.CommSettingsTextBoxLabel.Location = New System.Drawing.Point(106, 265)
        Me.CommSettingsTextBoxLabel.Name = "CommSettingsTextBoxLabel"
        Me.CommSettingsTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.CommSettingsTextBoxLabel.Size = New System.Drawing.Size(50, 13)
        Me.CommSettingsTextBoxLabel.TabIndex = 71
        Me.CommSettingsTextBoxLabel.Text = "Settings: "
        Me.CommSettingsTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'RxThresholdTextBoxLabel
        '
        Me.RxThresholdTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me.RxThresholdTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.RxThresholdTextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RxThresholdTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.RxThresholdTextBoxLabel.Location = New System.Drawing.Point(75, 227)
        Me.RxThresholdTextBoxLabel.Name = "RxThresholdTextBoxLabel"
        Me.RxThresholdTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.RxThresholdTextBoxLabel.Size = New System.Drawing.Size(80, 13)
        Me.RxThresholdTextBoxLabel.TabIndex = 70
        Me.RxThresholdTextBoxLabel.Text = "Rx Threshold: "
        Me.RxThresholdTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'ParityReplaceCharTextBoxlabel
        '
        Me.ParityReplaceCharTextBoxlabel.BackColor = System.Drawing.Color.Transparent
        Me.ParityReplaceCharTextBoxlabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.ParityReplaceCharTextBoxlabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ParityReplaceCharTextBoxlabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.ParityReplaceCharTextBoxlabel.Location = New System.Drawing.Point(74, 206)
        Me.ParityReplaceCharTextBoxlabel.Name = "ParityReplaceCharTextBoxlabel"
        Me.ParityReplaceCharTextBoxlabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ParityReplaceCharTextBoxlabel.Size = New System.Drawing.Size(83, 13)
        Me.ParityReplaceCharTextBoxlabel.TabIndex = 69
        Me.ParityReplaceCharTextBoxlabel.Text = "Parity Replace: "
        Me.ParityReplaceCharTextBoxlabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'WriteBufferSizeTextBoxLabel
        '
        Me.WriteBufferSizeTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me.WriteBufferSizeTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.WriteBufferSizeTextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WriteBufferSizeTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.WriteBufferSizeTextBoxLabel.Location = New System.Drawing.Point(68, 185)
        Me.WriteBufferSizeTextBoxLabel.Name = "WriteBufferSizeTextBoxLabel"
        Me.WriteBufferSizeTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.WriteBufferSizeTextBoxLabel.Size = New System.Drawing.Size(88, 13)
        Me.WriteBufferSizeTextBoxLabel.TabIndex = 68
        Me.WriteBufferSizeTextBoxLabel.Text = "Out Buffer Size: "
        Me.WriteBufferSizeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'InputModeComboBoxLabel
        '
        Me.InputModeComboBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me.InputModeComboBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.InputModeComboBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InputModeComboBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.InputModeComboBoxLabel.Location = New System.Drawing.Point(88, 143)
        Me.InputModeComboBoxLabel.Name = "InputModeComboBoxLabel"
        Me.InputModeComboBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.InputModeComboBoxLabel.Size = New System.Drawing.Size(68, 13)
        Me.InputModeComboBoxLabel.TabIndex = 67
        Me.InputModeComboBoxLabel.Text = "Input Mode: "
        Me.InputModeComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_inputLengthTextBoxLabel
        '
        Me._inputLengthTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._inputLengthTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._inputLengthTextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._inputLengthTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._inputLengthTextBoxLabel.Location = New System.Drawing.Point(99, 118)
        Me._inputLengthTextBoxLabel.Name = "_inputLengthTextBoxLabel"
        Me._inputLengthTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._inputLengthTextBoxLabel.Size = New System.Drawing.Size(57, 13)
        Me._inputLengthTextBoxLabel.TabIndex = 66
        Me._inputLengthTextBoxLabel.Text = "Input Len: "
        Me._inputLengthTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'InputBufferSizeTextBoxLabel
        '
        Me.InputBufferSizeTextBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me.InputBufferSizeTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.InputBufferSizeTextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.InputBufferSizeTextBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.InputBufferSizeTextBoxLabel.Location = New System.Drawing.Point(78, 100)
        Me.InputBufferSizeTextBoxLabel.Name = "InputBufferSizeTextBoxLabel"
        Me.InputBufferSizeTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.InputBufferSizeTextBoxLabel.Size = New System.Drawing.Size(78, 13)
        Me.InputBufferSizeTextBoxLabel.TabIndex = 65
        Me.InputBufferSizeTextBoxLabel.Text = "In Buffer Size: "
        Me.InputBufferSizeTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_handshakeComboBoxLabel
        '
        Me._handshakeComboBoxLabel.BackColor = System.Drawing.Color.Transparent
        Me._handshakeComboBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._handshakeComboBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._handshakeComboBoxLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._handshakeComboBoxLabel.Location = New System.Drawing.Point(81, 78)
        Me._handshakeComboBoxLabel.Name = "_handshakeComboBoxLabel"
        Me._handshakeComboBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._handshakeComboBoxLabel.Size = New System.Drawing.Size(75, 13)
        Me._handshakeComboBoxLabel.TabIndex = 64
        Me._handshakeComboBoxLabel.Text = "Handshaking:"
        Me._handshakeComboBoxLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_commPortNumberTextBoxlabel
        '
        Me._commPortNumberTextBoxlabel.BackColor = System.Drawing.Color.Transparent
        Me._commPortNumberTextBoxlabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._commPortNumberTextBoxlabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._commPortNumberTextBoxlabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me._commPortNumberTextBoxlabel.Location = New System.Drawing.Point(84, 36)
        Me._commPortNumberTextBoxlabel.Name = "_commPortNumberTextBoxlabel"
        Me._commPortNumberTextBoxlabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._commPortNumberTextBoxlabel.Size = New System.Drawing.Size(72, 13)
        Me._commPortNumberTextBoxlabel.TabIndex = 63
        Me._commPortNumberTextBoxlabel.Text = "Port:"
        Me._commPortNumberTextBoxlabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        '_MessagesTabPage
        '
        Me._MessagesTabPage.Controls.Add(Me._messagesListBox)
        Me._MessagesTabPage.Location = New System.Drawing.Point(4, 22)
        Me._MessagesTabPage.Name = "_MessagesTabPage"
        Me._MessagesTabPage.Size = New System.Drawing.Size(485, 419)
        Me._MessagesTabPage.TabIndex = 3
        Me._MessagesTabPage.Text = "MESSAGES"
        Me._MessagesTabPage.UseVisualStyleBackColor = True
        '
        '_updateTimer
        '
        Me._updateTimer.Interval = 1000
        '
        '_indicatorsLayout
        '
        Me._indicatorsLayout.ColumnCount = 19
        Me._indicatorsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5.0!))
        Me._indicatorsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._indicatorsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me._indicatorsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._indicatorsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me._indicatorsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._indicatorsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._indicatorsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me._indicatorsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._indicatorsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._indicatorsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me._indicatorsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._indicatorsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._indicatorsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me._indicatorsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._indicatorsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._indicatorsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667!))
        Me._indicatorsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._indicatorsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 5.0!))
        Me._indicatorsLayout.Controls.Add(Me._notAckIndicator, 17, 0)
        Me._indicatorsLayout.Controls.Add(Me._txIndicator, 15, 0)
        Me._indicatorsLayout.Controls.Add(Me._rxIndicator, 14, 0)
        Me._indicatorsLayout.Controls.Add(Me._xoffIndicator1, 12, 0)
        Me._indicatorsLayout.Controls.Add(Me._xonIndicator, 11, 0)
        Me._indicatorsLayout.Controls.Add(Me._ctsIndicator, 9, 0)
        Me._indicatorsLayout.Controls.Add(Me._rtsIndicator, 8, 0)
        Me._indicatorsLayout.Controls.Add(Me._dtrIndicator, 6, 0)
        Me._indicatorsLayout.Controls.Add(Me._dsrIndicator, 5, 0)
        Me._indicatorsLayout.Controls.Add(Me._portOpenIndicator, 1, 0)
        Me._indicatorsLayout.Controls.Add(Me._cdIndicator, 3, 0)
        Me._indicatorsLayout.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._indicatorsLayout.Location = New System.Drawing.Point(0, 469)
        Me._indicatorsLayout.Name = "_indicatorsLayout"
        Me._indicatorsLayout.RowCount = 1
        Me._indicatorsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._indicatorsLayout.Size = New System.Drawing.Size(493, 36)
        Me._indicatorsLayout.TabIndex = 93
        '
        '_messagesListBox
        '
        Me._messagesListBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._messagesListBox.FormattingEnabled = True
        Me._messagesListBox.ItemHeight = 14
        Me._messagesListBox.Location = New System.Drawing.Point(0, 0)
        Me._messagesListBox.Name = "_messagesListBox"
        Me._messagesListBox.ScrollAlwaysVisible = True
        Me._messagesListBox.Size = New System.Drawing.Size(485, 410)
        Me._messagesListBox.TabIndex = 0
        '
        'CodeNetPanel
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.WhiteSmoke
        Me.ClientSize = New System.Drawing.Size(493, 530)
        Me.Controls.Add(Me._tabControl)
        Me.Controls.Add(Me._indicatorsLayout)
        Me.Controls.Add(Me._statusBar)
        Me.Controls.Add(Me._mainMenu)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Location = New System.Drawing.Point(467, 306)
        Me.MaximizeBox = False
        Me.Name = "CodeNetPanel"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.StartPosition = System.Windows.Forms.FormStartPosition.Manual
        Me.Text = "Code Net Communications"
        Me._mainMenu.ResumeLayout(False)
        Me._mainMenu.PerformLayout()
        Me._statusBar.ResumeLayout(False)
        Me._statusBar.PerformLayout()
        Me._tabControl.ResumeLayout(False)
        Me._dataTabPage.ResumeLayout(False)
        Me._dataLayout.ResumeLayout(False)
        Me._dataLayout.PerformLayout()
        Me._txPanel.ResumeLayout(False)
        Me._txPanel.PerformLayout()
        Me._rxPanel.ResumeLayout(False)
        Me._rxPanel.PerformLayout()
        Me._packetSettingsTabPage.ResumeLayout(False)
        Me._packetSettingsLayout.ResumeLayout(False)
        Me._packetSettingsFrame.ResumeLayout(False)
        Me._packetSettingsFrame.PerformLayout()
        Me._portSettingsTabPage.ResumeLayout(False)
        Me._portSettingsLayout.ResumeLayout(False)
        Me._settingsFrame.ResumeLayout(False)
        Me._settingsFrame.PerformLayout()
        Me._MessagesTabPage.ResumeLayout(False)
        Me._indicatorsLayout.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _packetSettingsLayout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _indicatorsLayout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _txPanel As System.Windows.Forms.Panel
    Private WithEvents _portSettingsLayout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _rxPanel As System.Windows.Forms.Panel
    Private WithEvents _dataLayout As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _MessagesTabPage As System.Windows.Forms.TabPage
    Private WithEvents _messagesListBox As System.Windows.Forms.ListBox

#End Region
End Class