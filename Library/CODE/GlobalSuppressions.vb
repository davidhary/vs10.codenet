Imports System.Diagnostics.CodeAnalysis

#Region "CA1020:AvoidNamespacesWithFewTypes"
' Namespaces should generally have more than five types. 
' Namespaces, not currently having the prescribed type count, were defined with an eye towards inclusion of additional future types.
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", 
    Scope:="namespace", Target:="isr.CodeNet", 
    Justification:="Ignoring this warning...we want these namespaces, but don't have enough classes to go in them to satisfy the rule.")> 
#End Region


<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="member", Target:="isr.CodeNet.Handshake.#RtsXonXoff", MessageId:="Xoff")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="member", Target:="isr.CodeNet.Handshake.#RtsXonXoff", MessageId:="Xon")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="member", Target:="isr.CodeNet.Handshake.#RtsXonXoff", MessageId:="Rts")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="member", Target:="isr.CodeNet.Handshake.#Rts", MessageId:="Rts")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="member", Target:="isr.CodeNet.Handshake.#XonXoff", MessageId:="Xoff")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="member", Target:="isr.CodeNet.Handshake.#XonXoff", MessageId:="Xon")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="member", Target:="isr.CodeNet.PacketState.#InNak", MessageId:="Nak")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="member", Target:="isr.CodeNet.CodeNetDevice.#ReceivedXon", MessageId:="Xon")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="member", Target:="isr.CodeNet.CodeNetDevice.#ApplyDominoAmjetDefaults()", MessageId:="Amjet")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="member", Target:="isr.CodeNet.CodeNetDevice.#ApplyDghDefaults()", MessageId:="Dgh")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="member", Target:="isr.CodeNet.CodeNetDevice.#ApplyAccutrackerDefaults()", MessageId:="Accutracker")> 
<Module: SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", Scope:="member", Target:="isr.CodeNet.CodeNetDevice.#ReceivedXoff", MessageId:="Xoff")> 



<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#dominoSelectMessage1MenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#selectAccutrackerMenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#readDghDataMenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#saveActualsAsDefaultsMenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#displayActualsMenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#dominoIdentifyMenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#applyPacketSettingsMenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#applyNewSettingsMenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#accutrackerStartMenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#dominoGetStatusMenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#clearPortMenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#selectDghSensorsMenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#dominoGetConfigMenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#readAndZeroDghMenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#closePortMenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#displayDefaultsMenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#selectDominoAmjetMenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#dominoPrintGoMenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#dominoSaveMessage1MenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#applyRxSettingsMenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#dominoEnableHead1MenuItem_Click(System.Object,System.EventArgs)")> 
<Module: SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Scope:="member", Target:="isr.CodeNet.CodeNetPanel.#openPortMenuItem_Click(System.Object,System.EventArgs)")> 


<Module: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope:="member", Target:="isr.CodeNet.My.MyLibrary.#MustBroadcast(System.Diagnostics.TraceEventType)")> 
<Module: SuppressMessage("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode", Scope:="member", Target:="isr.CodeNet.My.MyLibrary.#MustBroadcast(System.Diagnostics.TraceEventType,System.Diagnostics.TraceEventType)")> 
