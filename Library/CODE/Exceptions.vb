Imports System.Runtime.Serialization
Imports System.Security.Permissions
Imports System.Reflection
Imports System.Runtime

''' <summary>Handles general tracer exceptions.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="11/22/04" by="David" revision="1.0.1787.x">
''' Created
''' </history>
<Serializable()> Public Class TracerException
    Inherits BaseException

    Private Const _defMessage As String = "failed tracer operation."

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New(TracerException._defMessage)
    End Sub

    ''' <summary>
    ''' Constructs the class specifying a <paramref name="message">message</paramref>.
    ''' </summary>
    ''' <param name="message">
    ''' Specifies the exception message.
    ''' </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>
    ''' Constructs the class specifying a <paramref name="message">message</paramref>
    ''' and <paramref name="innerException"/>.
    ''' </summary>
    ''' <param name="message">
    ''' Specifies the exception message.
    ''' </param>
    ''' <param name="innerException">
    ''' Specifies the InnerException.
    ''' </param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>
    ''' Constructs the class using serialization <paramref name="info"/> and <paramref name="context"/>
    '''  information.
    ''' </summary>
    ''' <param name="info">
    ''' Specifies <see cref="SerializationInfo">serialization information</see>.
    ''' </param>
    ''' <param name="context">
    ''' Specifies <see cref="StreamingContext">streaming context</see> for the exception.
    ''' </param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, 
        ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

End Class

''' <summary>Handles general configuration exceptions.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="11/22/04" by="David" revision="1.0.1787.x">
''' Created
''' </history>
<Serializable()> Public Class ConfigurationException
    Inherits BaseException

    Private Const _defMessage As String = "failed configuration operation."

    ''' <summary> Default constructor. </summary>
    Public Sub New()
        MyBase.New(ConfigurationException._defMessage)
    End Sub

    ''' <summary>
    ''' Constructs the class specifying a <paramref name="message">message</paramref>.
    ''' </summary>
    ''' <param name="message">
    ''' Specifies the exception message.
    ''' </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>
    ''' Constructs the class specifying a <paramref name="message">message</paramref>
    ''' and <paramref name="innerException"/>.
    ''' </summary>
    ''' <param name="message">
    ''' Specifies the exception message.
    ''' </param>
    ''' <param name="innerException">
    ''' Specifies the InnerException.
    ''' </param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary>
    ''' Constructs the class using serialization <paramref name="info"/> and <paramref name="context"/>
    '''  information.
    ''' </summary>
    ''' <param name="info">
    ''' Specifies <see cref="SerializationInfo">serialization information</see>.
    ''' </param>
    ''' <param name="context">
    ''' Specifies <see cref="StreamingContext">streaming context</see> for the exception.
    ''' </param>
    Protected Sub New(ByVal info As Serialization.SerializationInfo, 
        ByVal context As Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub

End Class
