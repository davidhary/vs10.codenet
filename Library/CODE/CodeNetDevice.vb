Imports isr.Core.EventHandlerExtensions
''' <summary>Defines a device for serial port code net protocol communication.
''' This device is capable of implementing fairly flexible serial communication.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history date="01/18/2002" by="David" revision="1.0.0768.x">
''' Created
''' </history>
''' <history date="04/02/02" by="David" revision="1.2.02.x">
''' Add ENQ, STX, and ETX
''' </history>
''' <history date="07/31/11" by="David" revision="1.2.4229.x">
''' Check on communication w/o start of text or end of text and using CR/LF as end of message
''' That was implemented already in 2001. The CR character is appended and the LF character is dropped.
''' </history>
Public Class CodeNetDevice

    Implements IDisposable

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Constructs this class.</summary>
    <System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope")>
    Public Sub New()

        Me.New(New System.IO.Ports.SerialPort())

    End Sub

    ''' <summary>Constructs this class.</summary>
    ''' <param name="serialPort">Specifies a serial port for the code net device.</param>
    Public Sub New(ByVal serialPort As System.IO.Ports.SerialPort)

        MyBase.New()

        Me._lastActionStringBuild = New isr.Core.MyStringBuilder

        ' Set the object id to -1 to indicate that it was not set.
        ' Me.UniqueId = -1
        ' clear the last reply.
        Me._lastReply = New System.Text.StringBuilder

        ' Initialize Packet Assemblage responses
        Me._resetPacketState()

        ' create a new instance of the serial port.
        Me._serialPort = serialPort

        ' set using instruments
        Me._isUsingInstrument = True

        ' load and apply default settings
        Me._applyDefaults()

    End Sub

    ''' <summary>Calls <see cref="M:Dispose(Boolean Disposing)"/> to cleanup.</summary>
    ''' <remarks>Do not make this method Overridable (virtual) because a derived 
    '''   class should not be able to override this method.</remarks>
    Public Sub Dispose() Implements IDisposable.Dispose

        ' Do not change this code.  Put cleanup code in Dispose(Boolean) below.

        ' this disposes all child classes.
        Dispose(True)

        ' GC.SuppressFinalize is issued to take this object off the 
        ' finalization(Queue) and prevent finalization code for this 
        ' prevent from executing a second time.
        GC.SuppressFinalize(Me)

    End Sub

    Private _isDisposed As Boolean
    ''' <summary>
    ''' Gets or sets the dispose status sentinel of the base class.  This applies to the derived class
    ''' provided proper implementation.
    ''' </summary>
    Protected Property IsDisposed() As Boolean
        Get
            Return Me._isDisposed
        End Get
        Private Set(ByVal value As Boolean)
            Me._isDisposed = value
        End Set
    End Property

    ''' <summary>Cleans up unmanaged or managed and unmanaged resources.</summary>
    ''' <param name="disposing">True if this method releases both managed and unmanaged 
    '''   resources; False if this method releases only unmanaged resources.</param>
    ''' <remarks>Executes in two distinct scenarios as determined by
    '''   its disposing parameter.  If True, the method has been called directly or 
    '''   indirectly by a user's code--managed and unmanaged resources can be disposed.
    '''   If disposing equals False, the method has been called by the 
    '''   runtime from inside the finalizer and you should not reference 
    '''   other objects--only unmanaged resources can be disposed.
    ''' 
    ''' Included in this dispose is special code to dispose of the communication form
    ''' that was required to terminate references to the ActiveX control.  This will
    ''' become redundant once we use the managed serial Port driver.
    ''' 
    ''' Use this method to unload the communication form.
    '''
    ''' This class does not require explicit unloading of
    ''' its form.  Because the class is not referenced with
    ''' events in its form, the class can execute its
    ''' terminate events and unload the form there.
    '''
    ''' However, it is a good practice to unload the
    ''' form before terminating the class.
    ''' <example>
    ''' This example demonstrates how to instantiate and
    ''' terminate a code net object.
    ''' <code>
    '''   ' Instance of the CodeNet object
    '''   Private moCodeNetDevice As isr.CodeNet.CodeNetDevice
    '''
    ''' ' Create a new instance of the CodeNet class
    '''   Set moCodeNetDevice = New CodeNetDevice
    '''
    '''   ' show the main form of the code net class
    '''   moCodeNetDevice.ShowPanel
    '''
    '''   ' unload the class form
    '''   moCodeNetDevice.UnloadPanel
    '''
    '''   ' Terminate the instance of the CodeNet class.
    '''   Set moCodeNetDevice = Nothing
    ''' </code>
    ''' </example>
    ''' </remarks>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)

        If Not Me.IsDisposed Then

            Try

                If disposing Then

                    ' Free managed resources when explicitly called

                    If Me.DataReceivedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.DataReceivedEvent.GetInvocationList
                            RemoveHandler Me.DataReceived, CType(d, Global.System.EventHandler(Of System.IO.Ports.SerialDataReceivedEventArgs))
                        Next
                    End If

                    If Me.ErrorReceivedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.ErrorReceivedEvent.GetInvocationList
                            RemoveHandler Me.ErrorReceived, CType(d, Global.System.EventHandler(Of System.IO.Ports.SerialErrorReceivedEventArgs))
                        Next
                    End If

                    If Me.FinishedReceivingEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.FinishedReceivingEvent.GetInvocationList
                            RemoveHandler Me.FinishedReceiving, CType(d, Global.System.EventHandler(Of System.EventArgs))
                        Next
                    End If

                    If Me.FinishedTransmittingEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.FinishedTransmittingEvent.GetInvocationList
                            RemoveHandler Me.FinishedTransmitting, CType(d, Global.System.EventHandler(Of System.EventArgs))
                        Next
                    End If

                    If Me.PinChangedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.PinChangedEvent.GetInvocationList
                            RemoveHandler Me.PinChanged, CType(d, Global.System.EventHandler(Of System.IO.Ports.SerialPinChangedEventArgs))
                        Next
                    End If

                    If Me.SettingsChangedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.SettingsChangedEvent.GetInvocationList
                            RemoveHandler Me.SettingsChanged, CType(d, Global.System.EventHandler(Of System.EventArgs))
                        Next
                    End If

                    If Me.SignalReceivedEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.SignalReceivedEvent.GetInvocationList
                            RemoveHandler Me.SignalReceived, CType(d, Global.System.EventHandler(Of System.EventArgs))
                        Next
                    End If

                    If Me.StartedReceivingEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.StartedReceivingEvent.GetInvocationList
                            RemoveHandler Me.StartedReceiving, CType(d, Global.System.EventHandler(Of System.EventArgs))
                        Next
                    End If

                    If Me.StartedTransmittingEvent IsNot Nothing Then
                        For Each d As [Delegate] In Me.StartedTransmittingEvent.GetInvocationList
                            RemoveHandler Me.StartedTransmitting, CType(d, Global.System.EventHandler(Of System.EventArgs))
                        Next
                    End If

                    ' Just in case, if the user omitted unloading the
                    ' form, do it here.
                    If Me._panel IsNot Nothing Then

                        ' unload the form
                        Me._panel.Close()
                        System.Windows.Forms.Application.DoEvents()

                        ' terminate the instance of the serial port panel
                        Me._panel.Dispose()
                        Me._panel = Nothing

                    End If

                    If Me._serialPort IsNot Nothing Then
                        ' terminate the reference to the serial port
                        Me._serialPort.Dispose()
                        Me._serialPort = Nothing
                    End If

                End If

                ' Free shared unmanaged resources

            Finally

                ' set the sentinel indicating that the class was disposed.
                Me.IsDisposed = True

            End Try

        End If

    End Sub

    ''' <summary>This destructor will run only if the Dispose method 
    '''   does not get called. It gives the base class the opportunity to 
    '''   finalize. Do not provide destructors in types derived from this class.</summary>
    Protected Overrides Sub Finalize()
        Try
            ' Do not re-create Dispose clean-up code here.
            ' Calling Dispose(false) is optimal for readability and maintainability.
            Dispose(False)
        Finally
            ' The compiler automatically adds a call to the base class finalizer 
            ' that satisfies the rule: FinalizersShouldCallBaseClassFinalizer.
            MyBase.Finalize()
        End Try
    End Sub

#End Region

#Region " CONSTANTS "

    Public Const StartHeader As Byte = 1
    Public Const StartText As Byte = 2
    Public Const EndText As Byte = 3
    Public Const EndTransmission As Byte = 4
    Public Const Enquiry As Byte = 5
    Public Const Acknowledge As Byte = 6
    Public Const Bell As Byte = 7
    Public Const Backspace As Byte = 8
    Public Const HorizontalTab As Byte = 9
    Public Const Linefeed As Byte = 10
    Public Const VerticalTab As Byte = 11
    Public Const FormFeed As Byte = 12
    Public Const CarriageReturn As Byte = 13
    Public Const ShiftOut As Byte = 14
    Public Const ShiftIn As Byte = 15
    Public Const NegativeAcknowledge As Byte = 21
    Public Const Escape As Byte = 27 ' &H1B

#End Region

#Region " CODE NET HELPERS "

    ''' <summary>
    ''' Converts the one-character string to byte using the current culture.
    ''' </summary>
    Public Shared Function ConvertToByte(ByVal value As String) As Byte
        Return Convert.ToByte(Convert.ToChar(value, Globalization.CultureInfo.CurrentCulture))
    End Function

    ''' <summary>Converts a string with possible non-printing characters to
    ''' ASCII HEX pairs.
    ''' </summary>
    ''' <returns>Converted string.
    ''' </returns>
    ''' <param name="value">specifies the string that might include
    ''' non-printing characters.
    ''' </param>
    ''' <history>
    ''' </history>
    Public Shared Function ConvertToHexPair(ByVal value As String) As String

        Dim hexPairs As New System.Text.StringBuilder

        If String.IsNullOrWhiteSpace(value) Then
            Return String.Empty
        End If
        For Each c As Char In value.ToCharArray
            hexPairs.Append(String.Format(Globalization.CultureInfo.CurrentCulture, 
                                          "{0:X}", Convert.ToByte(c)).PadLeft(2, "0"c))
        Next
        Return hexPairs.ToString

    End Function

    ''' <summary>Converts a string with possible non-printing characters to
    ''' ASCII HEX pairs or Ascii.
    ''' </summary>
    ''' <returns>Converted string.
    ''' </returns>
    ''' <param name="value">specifies the string that might include
    ''' non-printing characters.
    ''' </param>
    ''' <history date="10/27/2009" by="David" revision="3.1.2587.x">
    ''' Change post fix and prefix to using '\'
    ''' </history>
    Public Shared Function ConvertToHexPairOrAscii(ByVal value As String) As String

        Const prefix As String = "\"
        Const postfix As String = "\"
        Const firstAsciiCode As Short = 32

        ' the last character is z excluding per and post fix,
        ' which serve as the delimiting characters of the
        ' converted hex string
        Const lastAsciiCode As Short = 122

        Dim charAscii As Integer
        Dim output As New System.Text.StringBuilder

        If String.IsNullOrWhiteSpace(value) Then
            Return String.Empty
        End If

        For Each c As Char In value.ToCharArray
            charAscii = Convert.ToByte(c)

            ' check if character is printable
            If (charAscii < firstAsciiCode) OrElse (charAscii > lastAsciiCode) Then
                ' if not, replace with an hex string
                output.Append(String.Format(Globalization.CultureInfo.CurrentCulture, 
                                            "{0}{1:X}{2}", prefix, charAscii, postfix))
            Else
                output.Append(c)
            End If
        Next

        ' return the converted string
        Return output.ToString

    End Function

    ''' <summary>
    ''' Constructs a port name such as 'COM9' from the given port number.
    ''' </summary>
     Public Shared Function BuildPortName(ByVal portNumber As Integer) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "COM{0}", portNumber)
    End Function

    ''' <summary>
    ''' Constructs a port name such as 'COM9' from the given port number.
    ''' </summary>
     Public Shared Function BuildPortName(ByVal portNumber As String) As String
        If String.IsNullOrWhiteSpace(portNumber) Then
            Throw New ArgumentNullException("portNumber")
        End If
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "COM{0}", portNumber.Trim)
    End Function

    ''' <summary>
    ''' Parses a port number from a port name such as 'COM3'.
    ''' </summary>
    ''' <param name="portName"></param>
    Public Shared Function ParsePortNumber(ByVal portName As String) As Integer
        If String.IsNullOrWhiteSpace(portName) Then
            Throw New ArgumentNullException("portName")
        End If
        ' set the values
        Dim value As Integer = 0
        If Not Integer.TryParse(portName.Trim("COM ".ToCharArray), Globalization.NumberStyles.Integer, Globalization.CultureInfo.CurrentCulture, 
                         value) Then
            Throw New ArgumentOutOfRangeException("portName", "Failed parsing port number from the given port name")
        End If
        Return value
    End Function

    ''' <summary>
    ''' Gets the first valid port number.
    ''' </summary>
    Public Shared Function FirstPortNumber() As Integer
        Dim value As Integer = 255
        For Each portName As String In My.Computer.Ports.SerialPortNames()
            Dim portNumber As Integer = ParsePortNumber(portName)
            If portNumber < value Then
                value = portNumber
            End If
        Next
        Return value
    End Function

    ''' <summary>
    ''' Gets the first valid port number based on the specified port number and offset.
    ''' If the specified port number plus offset exists, this number is used
    ''' Otherwise, the port offset relative to the first port number is returned. 
    ''' If the latter is not found, the first port number is returned.
    ''' </summary>
    Public Shared Function GetPortNumber(ByVal value As Integer, ByVal portNumberOffset As Integer) As Integer
        If FindPort(value + portNumberOffset) Then
            Return value + portNumberOffset
        Else
            value = FirstPortNumber()
            If FindPort(value + portNumberOffset) Then
                Return value + portNumberOffset
            Else
                Return value
            End If
        End If
    End Function

    ''' <summary>
    ''' Gets the last valid port number.
    ''' </summary>
    Public Shared Function LastPortNumber() As Integer
        Dim value As Integer = 0
        For Each portName As String In My.Computer.Ports.SerialPortNames()
            Dim portNumber As Integer = ParsePortNumber(portName)
            If portNumber > value Then
                value = portNumber
            End If
        Next
        Return value
    End Function

    ''' <summary>
    ''' Returns true if the specified port exists on this computer.
    ''' </summary>
    ''' <param name="value">Specifies the port number</param>
    Public Shared Function FindPort(ByVal value As Integer) As Boolean
        Return CodeNetDevice.FindPort(CodeNetDevice.BuildPortName(value))
    End Function

    ''' <summary>
    ''' Returns true if the specified port exists on this computer.
    ''' </summary>
    ''' <param name="value">Specifies the port name in the format COMx</param>
    Public Shared Function FindPort(ByVal value As String) As Boolean

        For Each portName As String In My.Computer.Ports.SerialPortNames()
            If String.Compare(portName, value, StringComparison.OrdinalIgnoreCase) = 0 Then
                Return True
            End If
        Next
        Return False

    End Function

#End Region

#Region " CODE NET PROJECT PANEL EVENT HANDLERS "

    ''' <summary>Raises the FinishedTransmitting event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnFinishedTransmitting(ByVal e As System.EventArgs)
        FinishedTransmittingEvent.SafeInvoke(Me)
    End Sub

    ''' <summary>Occurs upon state FinishedTransmittingions.</summary>
    ''' <remarks>Use this event to notify the container class that the state FinishedTransmittingions are taking place.</remarks>
    Public Event FinishedTransmitting As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the FinishedReceiving event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnFinishedReceiving(ByVal e As System.EventArgs)
        FinishedReceivingEvent.SafeInvoke(Me)
    End Sub

    ''' <summary>Occurs upon state FinishedReceivingions.</summary>
    ''' <remarks>Use this event to notify the container class that the state FinishedReceivingions are taking place.</remarks>
    Public Event FinishedReceiving As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the StartedTransmitting event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnStartedTransmitting(ByVal e As System.EventArgs)
        StartedTransmittingEvent.SafeInvoke(Me)
    End Sub

    ''' <summary>Occurs upon state StartedTransmittingions.</summary>
    ''' <remarks>Use this event to notify the container class that the state StartedTransmittingions are taking place.</remarks>
    Public Event StartedTransmitting As EventHandler(Of System.EventArgs)

    ''' <summary>Raises the StartedReceiving event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnStartedReceiving(ByVal e As System.EventArgs)
        StartedReceivingEvent.SafeInvoke(Me, e)
    End Sub

    ''' <summary>Occurs upon state StartedReceivingions.</summary>
    ''' <remarks>Use this event to notify the container class that the state StartedReceivingions are taking place.</remarks>
    Public Event StartedReceiving As EventHandler(Of System.EventArgs)

    ''' <summary>
    ''' Raises the DataReceived event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.IO.Ports.SerialDataReceivedEventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnDataReceived(ByVal e As System.IO.Ports.SerialDataReceivedEventArgs)
        DataReceivedEvent.SafeInvoke(Me, e)
    End Sub

    ''' <summary>Occurs upon state DataReceivedions.</summary>
    ''' <remarks>Use this event to notify the container class that the state DataReceivedions are taking place.</remarks>
    Public Event DataReceived As EventHandler(Of System.IO.Ports.SerialDataReceivedEventArgs)

    ''' <summary>Raises the ErrorReceived event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.IO.Ports.SerialErrorReceivedEventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnErrorReceived(ByVal e As System.IO.Ports.SerialErrorReceivedEventArgs)
        ErrorReceivedEvent.SafeInvoke(Me, e)
    End Sub

    ''' <summary>Occurs upon state ErrorReceivedions.</summary>
    ''' <remarks>Use this event to notify the container class that the state ErrorReceivedions are taking place.</remarks>
    Public Event ErrorReceived As EventHandler(Of System.IO.Ports.SerialErrorReceivedEventArgs)

    ''' <summary>Raises the PinChanged event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.IO.Ports.SerialPinChangedEventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnPinChanged(ByVal e As System.IO.Ports.SerialPinChangedEventArgs)
        PinChangedEvent.SafeInvoke(Me, e)
    End Sub

    ''' <summary>Occurs upon state PinChangedions.</summary>
    ''' <remarks>Use this event to notify the container class that the state PinChangedions are taking place.</remarks>
    Public Event PinChanged As EventHandler(Of System.IO.Ports.SerialPinChangedEventArgs)

    ''' <summary>Raises the SettingsChanged event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnSettingsChanged(ByVal e As System.EventArgs)
        SettingsChangedEvent.SafeInvoke(Me, e)
    End Sub

    ''' <summary>Occurs upon state SettingsChangedions.</summary>
    ''' <remarks>Use this event to notify the container class that the state SettingsChangedions are taking place.</remarks>
    Public Event SettingsChanged As EventHandler(Of System.EventArgs)

#End Region

#Region " EXTERNAL EVENT HANDLERS "

    ''' <summary>Raises the StartedReceiving event
    ''' </summary>
    ''' <param name="e">Specifies the <see cref="System.EventArgs">system event arguments</see>
    ''' </param>
    Protected Overridable Sub OnSignalReceived(ByVal e As System.EventArgs)
        SignalReceivedEvent.SafeInvoke(Me, e)
    End Sub

    ''' <summary>
    ''' Occurs when a new signal is received.
    ''' </summary>
    Public Event SignalReceived As EventHandler(Of System.EventArgs)

#End Region

#Region " PACKET ASSEMBLAGE "

    Private _acknowledgedByte As Byte
    ''' <summary>Gets or sets the Acknowledged (ACK) byte.</summary>
    Public Property AcknowledgedByte() As Byte
        Get
            Return Me._acknowledgedByte
        End Get
        Set(ByVal value As Byte)
            Me._acknowledgedByte = value
        End Set
    End Property

    Private _endTextSubstitute As String
    ''' <summary>Gets or sets the string to use for replacing End-Of-Text characters.</summary>
    Public Property EndTextSubstitute() As String
        Get
            Return Me._endTextSubstitute
        End Get
        Set(ByVal value As String)
            Me._endTextSubstitute = value
        End Set
    End Property

    Private _endTransmissionByte1 As Byte
    ''' <summary>Gets or sets the End of Transmission (EOT) byte 1.</summary>
    Public Property EndTransmissionByte1() As Byte
        Get
            Return Me._endTransmissionByte1
        End Get
        Set(ByVal value As Byte)
            Me._endTransmissionByte1 = value
        End Set
    End Property

    Private _endTransmissionByte2 As Byte
    ''' <summary>Gets or sets the End of Transmission (EOT) byte 2.</summary>
    Public Property EndTransmissionByte2() As Byte
        Get
            Return Me._endTransmissionByte2
        End Get
        Set(ByVal value As Byte)
            Me._endTransmissionByte2 = value
        End Set
    End Property

    Private _endTextByte As Byte
    ''' <summary>Gets or sets the End of Text (ETX) byte.</summary>
    Public Property EndTextByte() As Byte
        Get
            Return Me._endTextByte
        End Get
        Set(ByVal value As Byte)
            Me._endTextByte = value
        End Set
    End Property

    Private _enquiryByte As Byte
    ''' <summary>Gets or sets the enquiry (ENQ) byte.</summary>
    Public Property EnquiryByte() As Byte
        Get
            Return Me._enquiryByte
        End Get
        Set(ByVal value As Byte)
            Me._enquiryByte = value
        End Set
    End Property

    Private _expectedPacketLength As Integer
    ''' <summary>Gets or sets the expected length of the receive string can be used
    ''' to detect end of text. If zero, await for end-of-text characters.</summary>
    Public Property ExpectedPacketLength() As Integer
        Get
            Return Me._expectedPacketLength
        End Get
        Set(ByVal value As Integer)
            Me._expectedPacketLength = value
        End Set
    End Property

    Private _isReplaceEndText As Boolean
    ''' <summary>Gets or sets the condition for replacing the End-Of-Text characters.</summary>
    Public Property IsReplaceEndText() As Boolean
        Get
            Return Me._isReplaceEndText
        End Get
        Set(ByVal value As Boolean)
            Me._isReplaceEndText = value
        End Set
    End Property

    Private _notAcknowledgedByte As Byte
    ''' <summary>Gets or sets the Not Acknowledged (ACK) byte.</summary>
    Public Property NotAcknowledgedByte() As Byte
        Get
            Return Me._notAcknowledgedByte
        End Get
        Set(ByVal value As Byte)
            Me._notAcknowledgedByte = value
        End Set
    End Property

    Private _notAcknowledgedLength As Integer
    ''' <summary>Gets or sets the expected length of the NAK message.  This
    ''' has the following values:
    ''' <list type="bullet">
    ''' <item><description>-1 no NAK message expected.</description></item>
    ''' <item><description>0 wait for End-Of-Test.</description></item>
    ''' <item><description>0 wait for End-Of-Test.</description></item>
    ''' </list>
    ''' </summary>
    Public Property NotAcknowledgedLength() As Integer
        Get
            Return Me._notAcknowledgedLength
        End Get
        Set(ByVal value As Integer)
            Me._notAcknowledgedLength = value
        End Set
    End Property

    ''' <summary>Gets or sets the name of the state.
    ''' </summary>
    Public ReadOnly Property PacketStateName() As String
        Get
            Select Case Me.PacketState
                Case PacketState.AwaitingPacket
                    Return "Ready"
                Case PacketState.InPacket
                    Return "Receiving"
                Case PacketState.InNak
                    Return "Nak"
                Case Else
                    Return "N/A"
            End Select
        End Get
    End Property

    ''' <summary>Resets the packet state
    ''' Called in case of an error or timeout.
    ''' </summary>
    ''' <history date="04/02/02" by="David" revision="1.2.02.x">
    ''' Add ENQ, STX, and ETX
    ''' </history>
    Private Sub _resetPacketState()

        Me._hasPacket = False
        Me._lastMessageSent = ""
        Me._notAcknowledgedMessage = ""
        Me._packetState = PacketState.AwaitingPacket
        Me._receivedXon = False
        Me._receivedXoff = False
        Me._receivedAcknowledged = False
        Me._receivedNotAcknowledged = False
        Me._receivedEnquiry = False
        Me._receivedSubPacketRaw = ""
        Me._receivedSubPacket = ""
        Me._receivedPacket = ""
        Me._receivedSubPacketToShow = ""

    End Sub

    ''' <summary>Resets the packet state
    ''' Called in case of an error or timeout.
    ''' </summary>
    ''' <history date="04/02/02" by="David" revision="1.2.02.x">
    ''' Add ENQ, STX, and ETX
    ''' </history>
    Public Sub ResetPacketState()

        Me._resetPacketState()

    End Sub

    Private _startTransmissionByte As Byte
    ''' <summary>Gets or sets the Start of Transmission (SOT) byte.</summary>
    Public Property StartTransmissionByte() As Byte
        Get
            Return Me._startTransmissionByte
        End Get
        Set(ByVal value As Byte)
            Me._startTransmissionByte = value
        End Set
    End Property

    Private _startTextByte As Byte
    ''' <summary>Gets or sets the Start of Text (STX) byte.</summary>
    Public Property StartTextByte() As Byte
        Get
            Return Me._startTextByte
        End Get
        Set(ByVal value As Byte)
            Me._startTextByte = value
        End Set
    End Property

#End Region

#Region " PACKET ASSEMBLAGE RESPONSES "

    Private _hasPacket As Boolean
    ''' <summary>Gets or sets the condition for has a complete packet.</summary>
    Public Property HasPacket() As Boolean
        Get
            Return Me._hasPacket
        End Get
        Set(ByVal value As Boolean)
            Me._hasPacket = value
        End Set
    End Property

    Private _lastMessageSent As String
    ''' <summary>Gets or sets the last message sent.</summary>
    Public Property LastMessageSent() As String
        Get
            Return Me._lastMessageSent
        End Get
        Set(ByVal value As String)
            Me._lastMessageSent = value
        End Set
    End Property

    Private _notAcknowledgedMessage As String
    ''' <summary>Gets or sets the Nak message.</summary>
    Public Property NotAcknowledgedMessage() As String
        Get
            Return Me._notAcknowledgedMessage
        End Get
        Set(ByVal value As String)
            Me._notAcknowledgedMessage = value
        End Set
    End Property

    Private _packetState As PacketState
    ''' <summary>Gets or sets the packet state.</summary>
    Public Property PacketState() As PacketState
        Get
            Return Me._packetState
        End Get
        Set(ByVal value As PacketState)
            Me._packetState = value
        End Set
    End Property

    Private _receivedAcknowledged As Boolean
    ''' <summary>Gets or sets the condition for Acknowledged (ACK) was received.</summary>
    Public Property ReceivedAcknowledged() As Boolean
        Get
            Return Me._receivedAcknowledged
        End Get
        Set(ByVal value As Boolean)
            Me._receivedAcknowledged = value
        End Set
    End Property

    Private _receivedNotAcknowledged As Boolean
    ''' <summary>Gets or sets the condition for Not Acknowledged (NAK) was received.</summary>
    Public Property ReceivedNotAcknowledged() As Boolean
        Get
            Return Me._receivedNotAcknowledged
        End Get
        Set(ByVal value As Boolean)
            Me._receivedNotAcknowledged = value
        End Set
    End Property

    Private _receivedEnquiry As Boolean
    ''' <summary>Gets or sets the condition for Enquiry (ENQ) was received.</summary>
    Public Property ReceivedEnquiry() As Boolean
        Get
            Return Me._receivedEnquiry
        End Get
        Set(ByVal value As Boolean)
            Me._receivedEnquiry = value
        End Set
    End Property

    Private _receivedPacket As String
    ''' <summary>Gets or sets the receive packet.</summary>
    Public Property ReceivedPacket() As String
        Get
            Return Me._receivedPacket
        End Get
        Set(ByVal value As String)
            Me._receivedPacket = value
        End Set
    End Property

    Private _receivedSubPacket As String
    ''' <summary>Gets or sets the Processed sub-packet.</summary>
    Public Property ReceivedSubPacket() As String
        Get
            Return Me._receivedSubPacket
        End Get
        Set(ByVal value As String)
            Me._receivedSubPacket = value
        End Set
    End Property

    Private _receivedSubPacketRaw As String
    ''' <summary>Gets or sets the received sub-packet.</summary>
    Public Property ReceivedSubPacketRaw() As String
        Get
            Return Me._receivedSubPacketRaw
        End Get
        Set(ByVal value As String)
            Me._receivedSubPacketRaw = value
        End Set
    End Property

    Private _receivedSubPacketToShow As String
    ''' <summary>Gets or sets the current sub packet to display.</summary>
    Public Property ReceivedSubPacketToShow() As String
        Get
            Return Me._receivedSubPacketToShow
        End Get
        Set(ByVal value As String)
            Me._receivedSubPacketToShow = value
        End Set
    End Property

    Private _receivedXon As Boolean
    ''' <summary>Gets or sets the condition for XON was received.</summary>
    Public Property ReceivedXon() As Boolean
        Get
            Return Me._receivedXon
        End Get
        Set(ByVal value As Boolean)
            Me._receivedXon = value
        End Set
    End Property

    Private _receivedXoff As Boolean
    ''' <summary>Gets or sets the condition for XOFF was received.</summary>
    Public Property ReceivedXoff() As Boolean
        Get
            Return Me._receivedXoff
        End Get
        Set(ByVal value As Boolean)
            Me._receivedXoff = value
        End Set
    End Property

    Private _timedOut As Boolean
    ''' <summary>Gets or sets the condition for timed out.</summary>
    Public Property TimedOut() As Boolean
        Get
            Return Me._timedOut
        End Get
        Set(ByVal value As Boolean)
            Me._timedOut = value
        End Set
    End Property

#End Region

#Region " PACKET HANDLERS "

    ''' <summary>Appends a character to the packet.
    ''' </summary>
    ''' <param name="inputChar">Current input character.
    ''' </param>
    ''' <param name="isNewPacket">Specifies if a new packet.
    ''' </param>
    ''' <history>
    ''' </history>
    Private Sub appendCharacter(ByVal inputChar As String, ByVal isNewPacket As Boolean)

        Dim displayedChar As String

        ' convert the character for display
        displayedChar = CodeNetDevice.ConvertToHexPairOrAscii(inputChar)

        If isNewPacket Then

            ' clear the packet flag
            Me.HasPacket = False

            ' initialize the packet
            Me.ReceivedPacket = inputChar

            ' initialize the sub packet
            Me.ReceivedSubPacket = inputChar

            ' update data for display
            Me.ReceivedSubPacketToShow = displayedChar

        Else

            ' Append to the packet
            Me.ReceivedPacket = Me.ReceivedPacket & inputChar

            ' append to the sub packet
            Me.ReceivedSubPacket = Me.ReceivedSubPacket & inputChar

            ' update data for display
            Me.ReceivedSubPacketToShow = Me.ReceivedSubPacketToShow & displayedChar

        End If

    End Sub

    Private _signalId As ReceiveSignalId
    ''' <summary>
    ''' Gets the signal derived from the character
    ''' </summary>
    Public ReadOnly Property SignalId() As ReceiveSignalId
        Get
            Return Me._signalId
        End Get
    End Property

    ''' <summary>Assembles the received packet.
    '''   Use this method to assemble a packet that has
    '''   been received in pieces from the port.
    '''   This particular routine will work on packets
    '''   terminated by [CR][LF]:
    ''' </summary>
    ''' <param name="newMessage">Specifies the current input string that must  be included in the packet.
    ''' </param>
    ''' <history>
    ''' </history>
    Private Sub assemblePacket(ByVal newMessage As String)

        If String.IsNullOrWhiteSpace(newMessage) Then
            Return
        End If

        ' a single received character
        For Each receivedChar As Char In newMessage.ToCharArray()

            ' process the character
            Me._signalId = interpretChar(receivedChar)

            ' send the end of packet signals only after the packet is assembled.
            If Me.SignalId <> ReceiveSignalId.None AndAlso Me.SignalId <> ReceiveSignalId.EndOfPacket1 AndAlso Me.SignalId <> ReceiveSignalId.EndOfPacket2 Then
                Me.OnSignalReceived(System.EventArgs.Empty)
            End If

            ' check if we have modem receptions
            ' if not connected, check if we have received
            ' modem characters only if we have modem mode
            ' me.ReceivedPacket = handleModemMessage(mme.ReceivedPacket )

            ' add the character and make a state transition based
            ' on the current state and state transition signal
            Select Case Me.PacketState

                Case PacketState.AwaitingPacket

                    processAwaitingPacketState(receivedChar, SignalId)

                Case PacketState.InPacket

                    processInPacketState(receivedChar, SignalId)

                Case PacketState.InNak

                    processInNakState(receivedChar, SignalId)

            End Select

            If Me.SignalId = ReceiveSignalId.EndOfPacket1 OrElse Me.SignalId = ReceiveSignalId.EndOfPacket2 Then
                Me.OnSignalReceived(System.EventArgs.Empty)
            End If

        Next

    End Sub

    ''' <summary>Terminates the connection.
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Sub HangUp()

        If Me._serialPort.IsOpen() Then
            If Me.IsUsingInstrument Then
                Me._serialPort.WriteLine("ATH0")
            End If
            Call ClosePort()
        End If

    End Sub

    ''' <summary>Interprets a single character.
    ''' </summary>
    ''' <returns>return a signal to be used by the state machine
    ''' </returns>
    ''' <param name="inputChar">the current input character.
    ''' </param>
    ''' <history date="04/02/02" by="David" revision="1.2.02.x">
    ''' Add ENQ, STX, and ETX
    ''' </history>
    Private Function interpretChar(ByVal inputChar As String) As ReceiveSignalId

        Const xionValue As Byte = &H11
        Const xioffValue As Byte = &H13

        Dim signalId As ReceiveSignalId

        ' convert to Ascii
        Dim inputCharAsciiValue As Byte
        inputCharAsciiValue = CodeNetDevice.ConvertToByte(inputChar)

        ' Check receipt of characters starting with SOT
        If inputCharAsciiValue = Me.StartTransmissionByte Then

            signalId = ReceiveSignalId.StartOfPacket

        ElseIf inputCharAsciiValue = Me.StartTextByte Then

            signalId = ReceiveSignalId.StartOfPacket

            ' check if we have ENQ message
        ElseIf inputCharAsciiValue = Me.EnquiryByte Then

            signalId = ReceiveSignalId.Enq

            ' check if we have ACK message
        ElseIf inputCharAsciiValue = Me.AcknowledgedByte Then

            signalId = ReceiveSignalId.Ack

            ' check if we have NAK message
        ElseIf inputCharAsciiValue = Me.NotAcknowledgedByte Then

            signalId = ReceiveSignalId.Nak

            ' check if we have X Off message
        ElseIf inputCharAsciiValue = xionValue Then

            signalId = ReceiveSignalId.Xon

            ' check if we have X On message
        ElseIf inputCharAsciiValue = xioffValue Then

            signalId = ReceiveSignalId.Xoff

            ' check if we have end-of-text
        ElseIf inputCharAsciiValue = Me.EndTransmissionByte1 Then

            signalId = ReceiveSignalId.EndOfPacket1

            ' check if we have end-of-text
        ElseIf inputCharAsciiValue = Me.EndTextByte Then

            signalId = ReceiveSignalId.EndOfPacket1

            ' check if we have end-of-text 2
        ElseIf inputCharAsciiValue = Me.EndTransmissionByte2 Then

            signalId = ReceiveSignalId.EndOfPacket2

        Else

            ' flag signal as just another character
            signalId = ReceiveSignalId.NewCharacter

        End If

        Return signalId

    End Function

    ''' <summary>Processes modem messages, if any
    ''' </summary>
    ''' <returns>Parsed message to report.
    ''' </returns>
    ''' <param name="modemMessage">
    ''' </param>
    ''' <param name="errorMessage">Specifies the status string.
    ''' </param>
    ''' <history>
    ''' </history>
    Public Function ParseModemMessage(ByVal modemMessage As String, ByVal errorMessage As String) As String
        If modemMessage Is Nothing Then
            Throw New ArgumentNullException("modemMessage")
        End If
        If errorMessage Is Nothing Then
            Throw New ArgumentNullException("errorMessage")
        End If

        Static isDoneOnce As Boolean
        ' Auto answer: Bad Logic.  Will never happened
        If modemMessage.IndexOf("RING", StringComparison.OrdinalIgnoreCase) > 0 AndAlso isDoneOnce Then
            If Me.IsUsingInstrument Then
                Me._serialPort.Write("ATA")
            End If
            isDoneOnce = False
            Return "RING"
        End If

        If modemMessage.IndexOf("CONNECT", StringComparison.OrdinalIgnoreCase) > 0 Then
            Return "Connected"
        End If

        If modemMessage.IndexOf("NO CARRIER", StringComparison.OrdinalIgnoreCase) > 0 Then
            Return "There is no carrier.  Please make sure that all your cables are properly connected."
        End If

        If modemMessage.IndexOf("ERROR", StringComparison.OrdinalIgnoreCase) > 0 Then
            Return "An Input Error occurred, it was " & errorMessage
        End If

        If modemMessage.IndexOf("BUSY", StringComparison.OrdinalIgnoreCase) > 0 Then
            Return "The number is busy.  Please try again later."
        End If

        If modemMessage.IndexOf("OK", StringComparison.OrdinalIgnoreCase) > 0 Then
            Return "OK"
        End If

        Return ""

    End Function

    ''' <summary>Processes the packet signal when in the receive awaiting packet state
    ''' </summary>
    ''' <param name="inputChar">Specifies the received character.
    ''' </param>
    ''' <param name="signalId">Specifies the signal returned by the character.
    ''' </param>
    ''' <history date="04/02/02" by="David" revision="1.2.02.x">
    ''' Add ENQ, STX, and ETX
    ''' </history>
    Private Sub processAwaitingPacketState(ByVal inputChar As String, ByVal signalId As ReceiveSignalId)

        Select Case signalId

            Case ReceiveSignalId.Ack

                ' turn on the Ack flag and stay put
                Me.ReceivedAcknowledged = True
                Me.ReceivedNotAcknowledged = False

            Case ReceiveSignalId.Enq

                ' turn on the ENQ flag
                Me.ReceivedEnquiry = True

            Case ReceiveSignalId.NewCharacter

                ' check if we must be waiting for a start of
                ' packet or not.
                If Me.StartTransmissionByte = 0 Then

                    ' if we are not waiting for a start of packet,
                    ' this is the start of the packet
                    appendCharacter(inputChar, True)

                    ' move to the InPacket state
                    Me.PacketState = PacketState.InPacket

                End If

            Case ReceiveSignalId.EndOfPacket1, ReceiveSignalId.EndOfPacket2

                ' if we get an end of message before starting one
                ' we just ignore it and still wait for the next
                ' character.

            Case ReceiveSignalId.Nak

                ' turn off the NAK, ENQ, and ACK flags
                Me.ReceivedAcknowledged = False
                Me.ReceivedNotAcknowledged = False
                Me.ReceivedEnquiry = False

                ' increment the NAK count
                Me._notAcknowledgedLength = 0

                ' start the NAK message
                Me.NotAcknowledgedMessage = String.Empty ' CStr(Nothing)

                ' check if we need to receive a NAK message
                If Me.NotAcknowledgedLength < 0 Then
                    ' if negative,

                    ' turn of NAK flag
                    Me.ReceivedNotAcknowledged = True

                    ' await no message- state in the same state

                ElseIf Me.NotAcknowledgedLength >= 0 Then

                    ' if 0, wait for end of text
                    ' otherwise, wait for expected length

                    ' go to in-NAK state
                    Me.PacketState = PacketState.InNak

                End If

            Case ReceiveSignalId.StartOfPacket

                ' this is the start of the packet
                appendCharacter(inputChar, True)

                ' move to the InPacket state
                Me.PacketState = PacketState.InPacket

            Case ReceiveSignalId.Xon

                ' turn on the XON flag
                Me.ReceivedXoff = False
                Me.ReceivedXon = True

            Case ReceiveSignalId.Xoff

                ' turn on the Xoff flag
                Me.ReceivedXon = False
                Me.ReceivedXoff = True

        End Select

    End Sub

    ''' <summary>Processes the packet signal when in the InNak state
    ''' </summary>
    ''' <returns>True if the Clear was acknowledged and processed.
    ''' </returns>
    ''' <param name="inputChar">Specifies the received character.
    ''' </param>
    ''' <param name="signalId">Specifies the signal returned by the character.
    ''' </param>
    ''' <history date="04/02/02" by="David" revision="1.2.02.x">
    ''' Add ENQ, STX, and ETX
    ''' </history>
    Private Function processInNakState(ByVal inputChar As String, ByVal signalId As ReceiveSignalId) As String

        processInNakState = String.Empty

        Select Case signalId

            Case ReceiveSignalId.Ack, ReceiveSignalId.Enq, ReceiveSignalId.EndOfPacket2, ReceiveSignalId.Nak, ReceiveSignalId.None, ReceiveSignalId.StartOfPacket

                ' ignore these states

            Case ReceiveSignalId.NewCharacter

                ' increment the NAK count
                Me._notAcknowledgedLength += 1

                ' increment the NAK message
                Me.NotAcknowledgedMessage = Me.NotAcknowledgedMessage & inputChar

                ' check if Nak message is completed
                If (Me.NotAcknowledgedLength > 0) AndAlso
                    (Me._notAcknowledgedLength >= Me.NotAcknowledgedLength) Then

                    ' if message is done, turn on the Nak flag
                    Me.ReceivedNotAcknowledged = True

                    ' update the received message
                    Me._lastReply.Append(CodeNetDevice.ConvertToHexPairOrAscii(Me.NotAcknowledgedMessage))

                    ' and go wait for a new packet
                    Me.PacketState = PacketState.AwaitingPacket

                End If

            Case ReceiveSignalId.EndOfPacket1

                ' we assume that this is the end of the NAK message

                ' if message is done, turn on the Nak flag
                Me.ReceivedNotAcknowledged = True

                ' update the received message
                Me._lastReply.Append(CodeNetDevice.ConvertToHexPairOrAscii(Me.NotAcknowledgedMessage))

                ' and go wait for a new packet
                Me.PacketState = PacketState.AwaitingPacket

            Case ReceiveSignalId.Xon

                ' turn on the XON flag
                Me.ReceivedXoff = False
                Me.ReceivedXon = True

            Case ReceiveSignalId.Xoff

                ' turn on the Xoff flag
                Me.ReceivedXon = False
                Me.ReceivedXoff = True

        End Select

    End Function

    ''' <summary>Processes the packet signal when in the receive InPacket state
    ''' </summary>
    ''' <param name="inputChar">Specifies the received character.
    ''' </param>
    ''' <param name="signalId">Specifies the signal returned by the character.
    ''' </param>
    ''' <history date="04/02/02" by="David" revision="1.2.02.x">
    ''' Add ENQ, STX, and ETX
    ''' </history>
    Private Function processInPacketState(ByVal inputChar As String, ByVal signalId As ReceiveSignalId) As String

        processInPacketState = String.Empty

        Select Case signalId

            Case ReceiveSignalId.Ack, ReceiveSignalId.Nak, ReceiveSignalId.None, ReceiveSignalId.StartOfPacket, ReceiveSignalId.Enq, ReceiveSignalId.EndOfPacket2
                ' ignore these signals.

            Case ReceiveSignalId.NewCharacter

                ' add the character to the packet
                appendCharacter(inputChar, False)

                ' check if we have received a full packet

                If Me.ExpectedPacketLength > 0 Then

                    If Me.ReceivedPacket.Length >= Me.ExpectedPacketLength Then

                        ' if we have a full packet,

                        ' tell us that we have a completed packet
                        Me.HasPacket = True

                        ' go wait for a new package
                        Me.PacketState = PacketState.AwaitingPacket

                    End If

                End If

            Case ReceiveSignalId.EndOfPacket1

                ' check if we need to replace the character
                If Me.IsReplaceEndText Then

                    ' if so, substitute the character
                    inputChar = Me.EndTextSubstitute

                End If

                ' add the character to the packet
                appendCharacter(inputChar, False)

                ' tell us that we have a completed packet
                Me.HasPacket = True

                ' go wait for a new package
                Me.PacketState = PacketState.AwaitingPacket

            Case ReceiveSignalId.Xon

                ' turn on the XON flag
                Me.ReceivedXoff = False
                Me.ReceivedXon = True

            Case ReceiveSignalId.Xoff

                ' turn on the Xoff flag
                Me.ReceivedXon = False
                Me.ReceivedXoff = True

            Case Else

        End Select

    End Function

#End Region

#Region " PORT "

    ''' <summary>
    ''' Returns the port settings in the good old format.
    ''' </summary>
    Public Property PortSettings() As String
        Get
            Dim stopBits As String = CStr(CInt(Me._serialPort.StopBits))
            If stopBits = "3" Then
                stopBits = "1.5"
            End If
            Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0},{1},{2},{3}", 
                                 Me._serialPort.BaudRate, Me._serialPort.Parity.ToString().Substring(0, 1), 
                                 Me._serialPort.DataBits, stopBits)
        End Get
        Set(ByVal value As String)
            If value Is Nothing Then
                Throw New ArgumentNullException("value")
            End If
            Dim settings() As String = value.Split(","c)
            Me._serialPort.BaudRate = CInt(settings(0))
            Select Case settings(1)
                Case "E", "e"
                    Me._serialPort.Parity = IO.Ports.Parity.Even
                Case "M", "m"
                    Me._serialPort.Parity = IO.Ports.Parity.Mark
                Case "N", "n"
                    Me._serialPort.Parity = IO.Ports.Parity.None
                Case "O", "o"
                    Me._serialPort.Parity = IO.Ports.Parity.Odd
                Case "S", "s"
                    Me._serialPort.Parity = IO.Ports.Parity.Space
                Case Else
                    Me._serialPort.Parity = IO.Ports.Parity.None
            End Select
            Me._serialPort.DataBits = CInt(settings(2))
            Select Case settings(3)
                Case "1"
                    Me._serialPort.StopBits = IO.Ports.StopBits.One
                Case "1.5"
                    Me._serialPort.StopBits = IO.Ports.StopBits.OnePointFive
                Case "2"
                    Me._serialPort.StopBits = IO.Ports.StopBits.Two
                Case Else
                    Me._serialPort.StopBits = IO.Ports.StopBits.One
            End Select
        End Set

    End Property

    ''' <summary>Sets auto answer mode
    ''' </summary>
    Public Sub AutoAnswer()

        ' Open port:
        Me.OpenPort()

        ' These two settings ensure that the modem will answer:
        Me._serialPort.DtrEnable = True
        If Me.IsUsingInstrument Then
            Me._serialPort.WriteLine("ATE1S0=1")
        End If

    End Sub

    ''' <summary>Clears the port.
    ''' </summary>
    Public Sub ClearPort()

        ' clear the port
        If Me.IsPortOpen Then
            Me._serialPort.DiscardInBuffer()
            Me._serialPort.DiscardOutBuffer()
        End If

        ' clear the transmission flags
        Me.ResetPacketState()

    End Sub

    ''' <summary>Closes the port.
    ''' </summary>
    Public Sub ClosePort()

        ' clear the port
        Me.ClearPort()

        ' close the port
        If Me.IsUsingInstrument Then
            Me._serialPort.Close()
        End If

    End Sub

    ''' <summary>Dials a modem to connect.
    ''' </summary>
    ''' <param name="numberToDial">
    ''' </param>
    Public Sub DialModem(ByVal numberToDial As String)

        If Me.IsUsingInstrument Then
            Dim dialMessage As String = "ATDT" & numberToDial & Convert.ToChar(10)
            Me._serialPort.Write(dialMessage)
        End If

    End Sub

    Private WithEvents _serialPort As System.IO.Ports.SerialPort
    ''' <summary>
    ''' Gets or sets reference to the 
    ''' <see cref="System.IO.Ports.SerialPort">serial port</see>.
    ''' </summary>
    Public ReadOnly Property SerialPort() As System.IO.Ports.SerialPort
        Get
            Return Me._serialPort
        End Get
    End Property

    ''' <summary>Gets or sets the communication port number.
    ''' </summary>
    Public Property PortName() As String
        Get
            Return Me._serialPort.PortName
        End Get
        Set(ByVal Value As String)
            ' Set the communication port number.
            Me._serialPort.PortName = Value
        End Set
    End Property

    ''' <summary>Gets or sets the communication port number.
    ''' </summary>
    Public Property PortNumber() As Integer
        Get
            Return CodeNetDevice.ParsePortNumber(Me.PortName)
        End Get
        Set(ByVal Value As Integer)
            ' Set the communication port number.
            Me.PortName = CodeNetDevice.BuildPortName(Value)
        End Set
    End Property

    ''' <summary>Gets or sets the handshake mode of the port.
    ''' </summary>
    Public Property Handshaking() As System.IO.Ports.Handshake
        Get
            Return Me._serialPort.Handshake
        End Get
        Set(ByVal Value As System.IO.Ports.Handshake)
            Me._serialPort.Handshake = Value
        End Set
    End Property

    Private _isConnected As Boolean
    ''' <summary>Gets or sets the condition for connected.</summary>
    Public Property IsConnected() As Boolean
        Get
            Return Me._isConnected
        End Get
        Set(ByVal value As Boolean)
            Me._isConnected = value
        End Set
    End Property

    ''' <summary>Gets or sets the open status of the port.
    ''' </summary>
    Public ReadOnly Property IsPortOpen() As Boolean
        Get

            If Me.IsUsingInstrument Then
                ' Check if the port is open.
                Return Me._serialPort.IsOpen
            Else
                Return True
            End If

        End Get
    End Property

    Private _isUsingInstrument As Boolean
    ''' <summary>Gets or sets true if we using the hardware.</summary>
    Public Property IsUsingInstrument() As Boolean
        Get
            Return Me._isUsingInstrument
        End Get
        Set(ByVal value As Boolean)
            Me._isUsingInstrument = value
        End Set
    End Property

    ''' <summary>Opens the port.
    ''' </summary>
    Public Sub OpenPort()

        If Me.IsUsingInstrument Then

            ' check if the port is open
            If Not Me._serialPort.IsOpen Then

                ' Otherwise, open the port.
                Me._serialPort.Open()

                ' save start time
                Me._timePortOpened = DateTime.Now

            End If

        Else

            ' save start time
            Me._timePortOpened = DateTime.Now

        End If

        ' set packet state to wait for a packet.
        Me.PacketState = PacketState.AwaitingPacket

    End Sub

    Private _lastReply As System.Text.StringBuilder
    ''' <summary>
    ''' Gets the last reply received after the last send.
    ''' </summary>
    Public ReadOnly Property LastReply() As String
        Get
            Return Me._lastReply.ToString
        End Get
    End Property

    Private _lastActionStringBuild As isr.Core.MyStringBuilder
    ''' <summary>
    ''' Get the last action string builder.
    ''' </summary>
    Private ReadOnly Property LastActionStringBuilder() As isr.Core.MyStringBuilder
        Get
            Return Me._lastActionStringBuild
        End Get
    End Property

    ''' <summary>
    ''' Gets the last action message for interpreting failures or status.
    ''' </summary>
    Public ReadOnly Property LastActionMessage() As String
        Get
            Return Me.LastActionStringBuilder.ToString
        End Get
    End Property

    ''' <summary>Sends a string.
    ''' </summary>
    ''' <returns>Returns true if the send command was
    ''' acknowledge or false if the send command failed
    ''' to acknowledge.  In the latter case, the error
    ''' code is located in NotAcknowledgedMessage property.
    ''' </returns>
    ''' <param name="value">Specifies the data to send.
    ''' </param>
    Public Function Send(ByVal value As String) As Boolean
        Return Send(value, False, 0, False)
    End Function

    ''' <summary>Sends a string.
    ''' </summary>
    ''' <returns>Returns true if the send command was
    ''' acknowledge or false if the send command failed
    ''' to acknowledge.  In the latter case, the error
    ''' code is located in NotAcknowledgedMessage property.
    ''' </returns>
    ''' <param name="value">Specifies the data to send.
    ''' </param>
    ''' <param name="isAwaitAck">Set true to wait for an acknowledgment from the port
    ''' that the sent command was received or failed.
    ''' </param>
    ''' <param name="timeout">Specifies the timeout for awaiting a reply.
    ''' </param>
    ''' <param name="isThrowExceptions">
    ''' </param>
    ''' <history date="08/28/01" by="David" revision="1.1.0000.x">
    ''' Add hardware handshake
    ''' </history>
    ''' <history date="04/02/02" by="David" revision="1.2.02.x">
    ''' Add ENQ, STX, and ETX
    ''' </history>
    Public Function Send(ByVal value As String, ByVal isAwaitAck As Boolean, 
                         ByVal timeout As Integer, ByVal isThrowExceptions As Boolean) As Boolean

        Me.LastActionStringBuilder.Clear()

        If String.IsNullOrWhiteSpace(value) Then
            Me.LastActionStringBuilder.Append("Attempting to send an empty message.")
            Return False
        End If

        ' clear status flags
        Me.TimedOut = False
        Me.ReceivedAcknowledged = False
        Me.ReceivedNotAcknowledged = False
        Me.ReceivedEnquiry = False

        If isAwaitAck Then
            Me.ResetPacketState()
        End If

        If Not Me._serialPort.IsOpen Then
            OpenPort()
        End If

        ' set the send string
        Me.LastMessageSent = value

        ' allow the panel to update the display.
        Me.OnStartedTransmitting(System.EventArgs.Empty)

        If Me.IsUsingInstrument Then
            Me._serialPort.Write(Me.LastMessageSent)
        Else
            Me.ReceivedAcknowledged = True
            Me.HasPacket = True
            Me.ReceivedEnquiry = True
        End If

        If isAwaitAck Then

            ' now wait for the port to acknowledge
            Dim timer As System.Diagnostics.Stopwatch = System.Diagnostics.Stopwatch.StartNew()
            Do
                Threading.Thread.Sleep(1)
                System.Windows.Forms.Application.DoEvents()
                Me.TimedOut = timer.ElapsedMilliseconds > timeout
            Loop Until Me.ReceivedAcknowledged OrElse Me.ReceivedNotAcknowledged OrElse Me.TimedOut

            ' check if timeout
            If Me.ReceivedAcknowledged Then

            ElseIf Me.TimedOut Then

                Me.LastActionStringBuilder.AppendLine("Timeout awaiting Acknowledgement.")
                If isThrowExceptions Then
                    Throw New BaseException(Me.LastActionMessage)
                End If
                Return False

            ElseIf Me.ReceivedNotAcknowledged Then

                Me.LastActionStringBuilder.AppendLine("Receiver reported NAK (busy).")
                If isThrowExceptions Then
                    Throw New BaseException(Me.LastActionMessage)
                End If
                Return False

            End If

            ' respond end of sending.
            Me.OnFinishedTransmitting(System.EventArgs.Empty)

        Else

            ' respond end of sending.
            Me.OnFinishedTransmitting(System.EventArgs.Empty)

        End If

        Return True

    End Function

    Private _timePortOpened As DateTime
    ''' <summary>Gets the time the port was opened.</summary>
    Public ReadOnly Property TimePortOpened() As DateTime
        Get
            Return Me._timePortOpened
        End Get
    End Property

#End Region

#Region " PORT EVENTS HANDLERS "

    ''' <summary>
    ''' Assembles any new characters.
    ''' </summary>
    Private Sub _serialPort_DataReceived(ByVal sender As Object, ByVal e As System.IO.Ports.SerialDataReceivedEventArgs) Handles _serialPort.DataReceived

        Select Case e.EventType

            Case IO.Ports.SerialData.Chars

                Me.OnStartedReceiving(e)

                ' read the buffer
                Me.ReceivedSubPacketRaw = Me._serialPort.ReadExisting

                ' assemble the packet
                assemblePacket(Me.ReceivedSubPacketRaw)

                Me.OnFinishedReceiving(e)

            Case IO.Ports.SerialData.Eof

        End Select

        Me.OnDataReceived(e)

    End Sub

    Private Sub _serialPort_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles _serialPort.Disposed
    End Sub

    Private Sub _serialPort_ErrorReceived(ByVal sender As Object, ByVal e As System.IO.Ports.SerialErrorReceivedEventArgs) Handles _serialPort.ErrorReceived
        Me.OnErrorReceived(e)
    End Sub

    Private Sub _serialPort_PinChanged(ByVal sender As Object, ByVal e As System.IO.Ports.SerialPinChangedEventArgs) Handles _serialPort.PinChanged

        Select Case e.EventType

            Case IO.Ports.SerialPinChange.CDChanged

                Me.IsConnected = Me._serialPort.CDHolding

        End Select

        Me.OnPinChanged(e)
    End Sub

#End Region

#Region " PANEL "

    ''' <summary>Gets or sets a new instance of the communication form is needed
    ''' to use multiple instances of this class.</summary>
    Private _panel As CodeNetPanel

    ''' <summary>Close the CodeNet communication screen.
    ''' </summary>
    Public Sub ClosePortPanel()
        If Me._panel IsNot Nothing Then
            Me._panel.Close()
        End If
    End Sub

    ''' <summary>Displays the CodeNet communication screen.
    ''' </summary>
    Public Sub ShowPortPanel()

        If Me._panel Is Nothing OrElse Me._panel.IsDisposed Then
            ' instantiate the CodeNetPanel form
            Me._panel = New CodeNetPanel(Me)
        End If
        Me._panel.Show()

    End Sub

    ''' <summary>Displays the CodeNet communication screen.
    ''' </summary>
    Public Sub ShowPortPanel(ByVal ownerForm As Windows.Forms.IWin32Window)

        If Me._panel Is Nothing OrElse Me._panel.IsDisposed Then
            Me._panel = New CodeNetPanel(Me)
        End If
        Me._panel.Show(ownerForm)
    End Sub

#End Region

#Region " SETTINGS MANAGEMENT "

    ''' <summary>Sets default packet and module settings for the Accutracker.
    ''' </summary>
    ''' <remarks>
    ''' Use this method to set properties to communicate with
    ''' the Accutracker.
    '''
    ''' The Accutracker has two types of responses, for
    ''' commands or data.
    '''
    ''' The module receives the 'S' command to start a blood
    ''' pressure measurement.  The module responds with an 'A'   to acknowledge.  Otherwise, the S command must be
    ''' resent in 5 seconds.
    '''
    ''' The module data message begins with a less-than sign.  It then sends its data string, which terminates
    ''' with a greater-than sign.
    '''
    ''' The host must then send an 'A' to acknowledge.
    '''
    ''' The accutracker uses RTC/CTS to communicate with the
    ''' host:
    '''
    ''' DTR:  When DTREnable is set to True, the Data Terminal
    ''' Ready line is set to high (on) when the port is
    ''' opened, and low (off) when the port is closed.
    ''' When DTREnable is set to False, the Data Terminal
    ''' Ready always remains low.
    '''
    ''' RTS:  When RTSEnable is set to True, the Request To
    ''' Send line is set to high (on) when the port is
    ''' opened, and low (off) when the port is closed.  The
    ''' Request To Send line is used in RTS/CTS hardware
    ''' handshaking. The RTSEnable property allows you to
    ''' manually poll the Request To Send line if you need
    ''' to determine its state.
    ''' </remarks>
    ''' <history date="04/02/02" by="David" revision="1.2.02.x">
    ''' Add ENQ, STX, and ETX
    ''' </history>
    Public Sub ApplyAccutrackerDefaults()

        ' set default assemble characters
        Me.IsReplaceEndText = False
        Me.EndTextSubstitute = String.Empty ' CStr(Nothing)
        Me.StartTransmissionByte = Convert.ToByte("<"c)
        Me.EndTransmissionByte1 = Convert.ToByte(">"c)
        Me.AcknowledgedByte = Convert.ToByte("A"c)
        Me.NotAcknowledgedByte = 0
        Me.NotAcknowledgedLength = 0
        Me.ExpectedPacketLength = 0
        Me.StartTextByte = 0
        Me.EndTextByte = 0
        Me.EnquiryByte = 0

        ' set port handshaking to none
        Me._serialPort.Handshake = IO.Ports.Handshake.None

        ' assert DTR
        Me._serialPort.DtrEnable = False
        Me._serialPort.RtsEnable = True

    End Sub

    ''' <summary>Reads the default settings from the configuration file 
    ''' and applies them to the port.
    ''' </summary>
    ''' <history date="04/02/02" by="David" revision="1.2.0002.x">
    ''' Add ENQ, STX, and ETX
    ''' </history>
    Private Sub _applyDefaults()

        Me._serialPort.PortName = CodeNetDevice.BuildPortName(My.Settings.PortNumber)
        Me._serialPort.DtrEnable = My.Settings.DtrEnable
        Me._serialPort.Handshake = My.Settings.Handshake
        Me._serialPort.DiscardNull = My.Settings.DiscardNull
        Me._serialPort.ParityReplace = My.Settings.ParityReplace
        Me._serialPort.ReadBufferSize = My.Settings.ReadBufferSize
        Me._serialPort.ReceivedBytesThreshold = My.Settings.ReceivedBytesThreshold
        Me._serialPort.RtsEnable = My.Settings.RtsEnable
        Me._serialPort.WriteBufferSize = CShort(My.Settings.WriteBufferSize)

        '  TO_DO:
        ' Me._serialPort.InputLen = CShort(My.Settings.InputLen)
        ' Me._serialPort.InputMode = My.Settings.InputMode
        ' Me._serialPort.Encoding = System.Text.Encoding.ASCII
        ' Me._serialPort.Settings = My.Settings.Settings

        Me._endTransmissionByte1 = My.Settings.EndTransmissionByte1
        Me._endTransmissionByte2 = My.Settings.EndTransmissionByte2
        Me._isReplaceEndText = My.Settings.IsReplaceEndText
        Me._endTextSubstitute = My.Settings.EndTextSubstitute
        Me._startTransmissionByte = My.Settings.StartTransmissionByte
        Me._acknowledgedByte = My.Settings.AcknowledgedByte
        Me._notAcknowledgedByte = My.Settings.NotAcknowledgedByte
        Me._notAcknowledgedLength = My.Settings.NotAcknowledgedLength
        Me._expectedPacketLength = My.Settings.ExpectedPacketLength
        Me._startTextByte = My.Settings.StartTextByte
        Me._endTextByte = My.Settings.EndTextByte
        Me._enquiryByte = My.Settings.EnquiryByte

    End Sub

    ''' <summary>Reads the default settings from the configuration file 
    ''' and applies them to the port.
    ''' </summary>
    ''' <history date="04/02/02" by="David" revision="1.2.02.x">
    ''' Add ENQ, STX, and ETX
    ''' </history>
    Public Sub ApplyDefaults()

        ' true if port was open when setting were applied
        Dim wasOpen As Boolean

        ' check if the serial port is open
        If Me._serialPort.IsOpen() Then
            ' if so, close and mark it as 'WasOpen'
            If Me.IsUsingInstrument Then
                Me._serialPort.Close()
            End If
            wasOpen = True
        Else
            ' the port was not open
            wasOpen = False
        End If

        ' now apply the settings
        Me._applyDefaults()

        ' check if the port was open
        If wasOpen = True Then
            ' if so, open the port
            If Me.IsUsingInstrument Then
                Me._serialPort.Open()
            End If
        End If

    End Sub

    ''' <summary>Applies the factory defaults port settings.
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Sub ApplyPortFactoryDefaults()

        ' true if port was open when setting were applied
        Dim wasOpen As Boolean

        ' check if the serial port is open
        If Me._serialPort.IsOpen() Then
            ' if so, close and mark it as 'WasOpen'
            If Me.IsUsingInstrument Then
                Me._serialPort.Close()
            End If
            wasOpen = True
        Else
            ' the port was not open
            wasOpen = False
        End If

        '  TO_DO:
        ' Me._serialPort.EOFEnable = False
        ' Me._serialPort.InputLen = 0
        ' Me._serialPort.InputMode = MSCommLib.InputModeConstants.comInputModeText
        ' Me._serialPort.Settings = "9600,n,8,1"
        ' Me._serialPort.SThreshold = 1
        ' Me._serialPort.InputMode = MSCommLib.InputModeConstants.comInputModeText

        ' set default settings for the port
        Me._serialPort.PortName = CodeNetDevice.BuildPortName(1)
        Me._serialPort.DtrEnable = True
        Me._serialPort.Handshake = IO.Ports.Handshake.None
        Me._serialPort.ReadBufferSize = 1024
        Me._serialPort.DiscardNull = True
        Me._serialPort.WriteBufferSize = 512
        Me._serialPort.ParityReplace = Convert.ToByte("?"c)
        Me._serialPort.ReceivedBytesThreshold = 1
        Me._serialPort.RtsEnable = False

        ' check if the port was open
        If wasOpen = True Then
            ' if so, open the port
            If Me.IsUsingInstrument Then
                Me._serialPort.Open()
            End If
        End If

        ' display the actual settings
        Me.OnSettingsChanged(System.EventArgs.Empty)

    End Sub

    ''' <summary>Sets default packet and module
    ''' settings for the DGH sensor modules.
    ''' </summary>
    ''' <remarks>
    ''' Use this method to set properties to communicate with
    ''' the DGH sensors.
    '''
    ''' The DGH module response message begin with an
    ''' asterisk '*' or a question mark '?' prompt.
    '''
    ''' The '*' indicates acknowledgement of a valid command.
    '''
    ''' The '?' precedes an error message of variable length.
    '''
    ''' All response messages are terminated with CR.
    ''' </remarks>
    ''' <history date="04/02/02" by="David" revision="1.2.0002.x">
    ''' Add ENQ, STX, and ETX
    ''' </history>
    Public Sub ApplyDghDefaults()

        ' set default assemble characters
        Me.EndTransmissionByte1 = CodeNetDevice.CarriageReturn
        Me.EndTransmissionByte2 = CodeNetDevice.Linefeed
        Me.IsReplaceEndText = False
        Me.EndTextSubstitute = String.Empty ' CStr(Nothing)
        Me.StartTransmissionByte = Convert.ToByte("*"c)
        Me.AcknowledgedByte = CodeNetDevice.Acknowledge
        Me.NotAcknowledgedByte = Convert.ToByte("?"c)
        Me.NotAcknowledgedLength = 0
        Me.ExpectedPacketLength = 0
        Me.StartTextByte = 0
        Me.EndTextByte = 0
        Me.EnquiryByte = 0

        ' set port handshaking to none
        Me._serialPort.Handshake = IO.Ports.Handshake.None

    End Sub

    ''' <summary>Sets default packet and module
    '''  settings for the Domino-Amjet Printers.
    ''' </summary>
    ''' <remarks>
    ''' Use this method to set properties to communicate with
    ''' the Domino-Amjet Printers.
    '''
    ''' The Domino-Amjet printer use a 'CodeNet' protocol.
    ''' All messages commence with an Escape [Esc] (1Bh) and
    ''' end with End-Of-Text [EOT] (04h).
    '''
    ''' A positive [ACK] (6h) or negative [NACK] (15h) is
    ''' returned by the printer to indicate reception and
    ''' interpretation of a command.
    '''
    ''' A negative acknowledgement is followed by a 3 ASCII
    ''' digit code error.
    '''
    ''' The default RTS Domino handshaking fail with MSCOMM.
    ''' A special printer cable must be constructed to
    ''' work around the RTS handshake required by the printer.
    ''' Connect this cable as follows:
    '''  Computer   Printer
    '''  Pin(s) - Pin(6)
    '''  In       2 - 3      Out
    '''  Out      3 - 2      In
    '''  DTR  6 - 4 x 6 - 4  DSR
    '''  GND      5 - 5      GND
    '''  DSR  4 - 6 x 4 - 6  DTR
    '''  RTS  8 - 7 x 8 - 7  CTS
    '''  CTS  7 - 8 x 7 - 8  RTS
    '''  RING     9 x
    ''' </remarks>
    ''' <history date="08/28/01" by="David" revision="1.1.00.x">
    ''' Default to hardware handshake
    ''' </history>
    ''' <history date="09/13/01" by="David" revision="1.2.00.x">
    ''' Default to no handshaking.  Document cable.
    ''' </history>
    ''' <history date="04/02/02" by="David" revision="1.2.02.x">
    ''' Add ENQ, STX, and ETX
    ''' </history>
    Public Sub ApplyDominoAmjetDefaults()

        ' set default assemble characters
        Me.EndTransmissionByte1 = CodeNetDevice.EndTransmission
        Me.EndTransmissionByte2 = CodeNetDevice.CarriageReturn
        Me.IsReplaceEndText = False
        Me.EndTextSubstitute = String.Empty ' CStr(Nothing)
        Me.StartTransmissionByte = CodeNetDevice.Escape
        Me.AcknowledgedByte = CodeNetDevice.Acknowledge
        Me.NotAcknowledgedByte = CodeNetDevice.NegativeAcknowledge
        Me.NotAcknowledgedLength = 3 ' 3 character error code
        Me.ExpectedPacketLength = 0
        Me.StartTextByte = 0
        Me.EndTextByte = 0
        Me.EnquiryByte = 0

        ' set port handshaking to none
        Me._serialPort.Handshake = IO.Ports.Handshake.None

    End Sub

    ''' <summary>Sets factory defaults values for the packet management.
    ''' </summary>
    Public Sub ApplyPacketsFactoryDefaults()

        ' set default assemble characters
        Me.EndTransmissionByte1 = CodeNetDevice.CarriageReturn
        Me.EndTransmissionByte2 = CodeNetDevice.Linefeed
        Me.IsReplaceEndText = False
        Me.EndTextSubstitute = Convert.ToChar(Me.EndTransmissionByte1) & Convert.ToChar(Me.EndTransmissionByte2)
        Me.StartTransmissionByte = CodeNetDevice.Escape
        Me.AcknowledgedByte = CodeNetDevice.Acknowledge
        Me.NotAcknowledgedByte = CodeNetDevice.NegativeAcknowledge
        Me.NotAcknowledgedLength = 3
        Me.ExpectedPacketLength = 0
        Me.StartTextByte = CodeNetDevice.StartText
        Me.EndTextByte = CodeNetDevice.EndText
        Me.EnquiryByte = CodeNetDevice.Enquiry

    End Sub

    ''' <summary>Applies the recommended settings to the port.
    '''   Applies the settings entered in the
    '''   label boxes to the control.  Settings are not saved
    '''   to the configuration file.
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Sub ApplyRecommendedSetting()

        ' true if port was open when setting were applied
        Dim wasOpen As Boolean

        ' check if the serial port is open
        If Me._serialPort.IsOpen() Then
            ' if so, close and mark it as 'WasOpen'
            If Me.IsUsingInstrument Then
                Me._serialPort.Close()
            End If
            wasOpen = True
        Else
            ' the port was not open
            wasOpen = False
        End If

        ' set recommended settings for the port
        Me._serialPort.PortName = CodeNetDevice.BuildPortName(2)
        Me._serialPort.DtrEnable = True
        '_serialPort.EOFEnable = False
        Me._serialPort.Handshake = IO.Ports.Handshake.RequestToSend
        Me._serialPort.ReadBufferSize = 1024
        Me._serialPort.DiscardNull = True
        Me._serialPort.WriteBufferSize = 512
        Me._serialPort.ParityReplace = Convert.ToByte("?"c)
        Me._serialPort.ReceivedBytesThreshold = 1
        Me._serialPort.RtsEnable = False
        '_serialPort.InputLen = 0
        '_serialPort.InputMode = MSCommLib.InputModeConstants.comInputModeText
        '_serialPort.Settings = "9600,n,8,1"
        '_serialPort.SThreshold = 1

        ' set default assemble characters
        Me.EndTransmissionByte1 = CodeNetDevice.CarriageReturn
        Me.EndTransmissionByte2 = CodeNetDevice.Linefeed
        Me.IsReplaceEndText = True
        Me.EndTextSubstitute = String.Empty
        Me.StartTransmissionByte = CodeNetDevice.Escape
        Me.AcknowledgedByte = CodeNetDevice.Acknowledge
        Me.NotAcknowledgedByte = CodeNetDevice.NegativeAcknowledge
        Me.NotAcknowledgedLength = 3
        Me.ExpectedPacketLength = 0
        Me.StartTextByte = CodeNetDevice.StartText
        Me.EndTextByte = CodeNetDevice.EndText
        Me.EnquiryByte = CodeNetDevice.Enquiry

        ' check if the port was open
        If wasOpen = True Then
            ' if so, open the port
            If Me.IsUsingInstrument Then
                Me._serialPort.Open()
            End If
        End If

        ' display the actual settings
        Me.OnSettingsChanged(System.EventArgs.Empty)

    End Sub

    ''' <summary>
    ''' Loads the settings.
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Sub LoadSettings()

        ' set the loading options
        Me._settingSource = My.Settings.SettingsSource

        ' Find out which option setting the user picked:
        If Me._settingSource = isr.CodeNet.SettingsSource.Defaults Then

            ' load and apply the default settings
            Me.ApplyDefaults()

        ElseIf Me._settingSource = isr.CodeNet.SettingsSource.Recommended Then

            ' if recommended, apply recommended settings
            Me.ApplyRecommendedSetting()

            Me.OnSettingsChanged(System.EventArgs.Empty)

        End If

    End Sub

    Private _settingSource As Nullable(Of isr.CodeNet.SettingsSource)
    ''' <summary>Gets or sets the value of the Setting Source property of this class.
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Property SettingsSource() As isr.CodeNet.SettingsSource
        Get
            If Not Me._settingSource.HasValue Then
                Me._settingSource = My.Settings.SettingsSource
            End If
            Return Me._settingSource.Value
        End Get
        Set(ByVal Value As isr.CodeNet.SettingsSource)
            ' save the setting source
            Me._settingSource = Value
        End Set
    End Property

    ''' <summary>Saves the current settings to the configuration file as the new defaults.
    ''' </summary>
    ''' <history date="04/02/02" by="David" revision="1.2.02.x">
    ''' Add ENQ, STX, and ETX
    ''' </history>
    Public Sub SaveSettings()

        My.Settings.PortNumber = CodeNetDevice.ParsePortNumber(Me._serialPort.PortName)
        My.Settings.DtrEnable = Me._serialPort.DtrEnable
        My.Settings.Handshake = Me._serialPort.Handshake
        My.Settings.ReadBufferSize = Me._serialPort.ReadBufferSize
        My.Settings.DiscardNull = Me._serialPort.DiscardNull
        My.Settings.WriteBufferSize = Me._serialPort.WriteBufferSize
        My.Settings.ParityReplace = Me._serialPort.ParityReplace
        My.Settings.ReceivedBytesThreshold = Me._serialPort.ReceivedBytesThreshold
        My.Settings.RtsEnable = Me._serialPort.RtsEnable

        'My.Settings.InputLen = Me._serialPort.InputLen
        'My.Settings.InputMode = Me._serialPort.InputMode
        'My.Settings.Settings = Me._serialPort.Settings
        'My.Settings.SThreshold = Me._serialPort.SThreshold

        My.Settings.EndTransmissionByte1 = Me.EndTransmissionByte1
        My.Settings.EndTransmissionByte2 = Me.EndTransmissionByte2
        My.Settings.IsReplaceEndText = Me.IsReplaceEndText
        My.Settings.EndTextSubstitute = Me.EndTextSubstitute
        My.Settings.StartTransmissionByte = Me.StartTransmissionByte
        My.Settings.AcknowledgedByte = Me.AcknowledgedByte
        My.Settings.NotAcknowledgedByte = Me.NotAcknowledgedByte
        My.Settings.NotAcknowledgedLength = Me.NotAcknowledgedLength
        My.Settings.ExpectedPacketLength = Me.ExpectedPacketLength
        My.Settings.StartTextByte = Me.StartTextByte
        My.Settings.EndTextByte = Me.EndTextByte
        My.Settings.EnquiryByte = Me.EnquiryByte
        If Me._settingSource.HasValue Then
            My.Settings.SettingsSource = Me._settingSource.Value
        End If
        My.Settings.Save()

    End Sub

#End Region

End Class

''' <summary>Enumerates the receive signals.
''' </summary>
Public Enum ReceiveSignalId
  None
  NewCharacter
  StartOfPacket
  EndOfPacket1
  EndOfPacket2
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Ack")> 
  Ack
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Nak")> 
  Nak
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Xon")> 
  Xon
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Xoff")> 
  Xoff
  <CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId:="Enq")> 
  Enq
End Enum
