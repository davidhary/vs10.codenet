#Region " TYPES "

''' <summary>Enumerates the packet states.
''' </summary>
Public Enum PacketState
    <System.ComponentModel.Description("None")> None
    <System.ComponentModel.Description("Awaiting Packet")> AwaitingPacket
    <System.ComponentModel.Description("In Packet")> InPacket
    <System.ComponentModel.Description("In Nak")> InNak
End Enum

''' <summary>Enumerates the source for the settings.
''' </summary>
Public Enum SettingsSource
    <System.ComponentModel.Description("None")> None
    <System.ComponentModel.Description("Defaults")> Defaults
    <System.ComponentModel.Description("Recommended")> Recommended
End Enum

''' <summary>Enumerates the handshaking.
''' </summary>
Public Enum Handshake
    <System.ComponentModel.Description("None")> None = IO.Ports.Handshake.None
    <System.ComponentModel.Description("Xon Xoff")> XonXoff = IO.Ports.Handshake.XOnXOff
    <System.ComponentModel.Description("Request To Send")> Rts = IO.Ports.Handshake.RequestToSend
    <System.ComponentModel.Description("Rts Xon Xoff")> RtsXonXoff = IO.Ports.Handshake.RequestToSendXOnXOff
End Enum

#End Region

