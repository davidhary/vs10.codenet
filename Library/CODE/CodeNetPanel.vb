Imports System.Windows.Forms

''' <summary>Defines the communication screen module for the CodeNet class.
''' </summary>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.<para>
''' Licensed under The MIT License. </para><para>
''' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
''' BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
''' NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
''' DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
''' OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
''' </para> </license>
''' <history>
''' 04/02/02  David Hary  1.2.02  Add ENQ, STX, and ETX
''' </history>
''' <history date="01/18/01" by="David" revision="1.0.0383.x">
''' Created
''' </history>
Friend Class CodeNetPanel
    Inherits System.Windows.Forms.Form

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Loads this module.
    ''' </summary>
    ''' <param name="device">Specifies reference to an instance of the
    ''' <see cref="isr.CodeNet.CodeNetDevice">ISR CodeNet class</see>.
    ''' </param>
    ''' <history>
    ''' </history>
    Public Sub New(ByVal device As isr.CodeNet.CodeNetDevice)

        MyBase.New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        ' onInstantiate()

        ' set reference to the class if this is the first time
        If Me._codeNetDevice Is Nothing Then
            Me._codeNetDevice = device
        End If

        propertySetter = New isr.Core.ThreadSafePropertySetter(Me)

        ' set colors
        Dim onColor As Integer = System.Drawing.Color.FromArgb(0, 255, 0).ToArgb
        Dim offColor As Integer = System.Drawing.Color.FromArgb(0, 127, 0).ToArgb

        Me._cdIndicator.OnColor = onColor
        Me._cdIndicator.OffColor = offColor
        Me._ctsIndicator.OnColor = onColor
        Me._ctsIndicator.OffColor = offColor
        Me._dsrIndicator.OnColor = onColor
        Me._dsrIndicator.OffColor = offColor
        Me._dtrIndicator.OnColor = onColor
        Me._dtrIndicator.OffColor = offColor
        Me._notAckIndicator.OnColor = onColor
        Me._notAckIndicator.OffColor = offColor
        Me._portOpenIndicator.OnColor = onColor
        Me._portOpenIndicator.OffColor = offColor
        Me._rtsIndicator.OnColor = onColor
        Me._rtsIndicator.OffColor = offColor
        Me._rxIndicator.OnColor = onColor
        Me._rxIndicator.OffColor = offColor
        Me._txIndicator.OnColor = onColor
        Me._txIndicator.OffColor = offColor
        Me._xoffIndicator1.OnColor = onColor
        Me._xoffIndicator1.OffColor = offColor
        Me._xonIndicator.OnColor = onColor
        Me._xonIndicator.OffColor = offColor

    End Sub

    ''' <summary>
    ''' Prepares to unload this module.
    ''' </summary>
    ''' <history>
    ''' </history>
    Private Sub onDisposeManagedResources()

        ' disable the timer
        Me._updateTimer.Enabled = False

        ' terminate the reference to the class
        Me._codeNetDevice = Nothing

    End Sub

#Region " DROP SHADOW "

    ''' <summary>
    ''' Defines the Drop Shadow constant.
    ''' </summary>
    Private Const CS_DROPSHADOW As Integer = 131072

    ''' <summary>
    ''' Adds a drop shadow parameter.
    ''' </summary>
    ''' <remarks>
    ''' From Code Project: http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx
    ''' </remarks>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Demand, Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)> 
        Get
            Dim cp As Windows.Forms.CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
            Return cp
        End Get
    End Property

#End Region

#End Region

#Region " ACCUTRACKER HANDLERS "

    Private Sub selectAccutrackerMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _selectAccutrackerMenuItem.Click

        Try

            ' clear checks
            Me._selectAccutrackerMenuItem.Checked = False
            Me._selectDghSensorsMenuItem.Checked = False
            Me._selectDominoAmjetMenuItem.Checked = False

            Me._accutrackerDevceMenu.Visible = False
            Me._dghDeviceMenu.Visible = False
            Me._dominoDeviceMenu1.Visible = False

            ' select the accutracker device
            Me._codeNetDevice.ApplyAccutrackerDefaults()
            Me._selectAccutrackerMenuItem.Checked = True
            Me._accutrackerDevceMenu.Visible = True

            ' Do not interpret as Hex
            Me.IsInterpretCmdAsHex = False

            ' display as actual
            displayActualSettings()

        Catch ex As Exception

            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED SELECTING MENU. Details: {0}", ex)

        End Try

    End Sub

    Private Sub accutrackerStartMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _accutrackerStartMenuItem.Click

        Try

            Dim trialCount As Short
            trialCount = 0
            Do
                ' increment number of attempts
                trialCount += 1S
                ' send message requesting Ack or Nak
                Me._codeNetDevice.Send("S", True, 5000, True)
                Do
                    System.Windows.Forms.Application.DoEvents()
                Loop Until Me._codeNetDevice.ReceivedAcknowledged OrElse Me._codeNetDevice.ReceivedNotAcknowledged OrElse Me._codeNetDevice.TimedOut
            Loop Until trialCount >= 5 OrElse Me._codeNetDevice.ReceivedAcknowledged

            ' check if we have a measurement
            Dim endWaitTime As Date
            If Me._codeNetDevice.ReceivedAcknowledged Then

                ' if so, get data

                ' mark end of wait time in 60 seconds
                endWaitTime = DateTime.Now.Add(TimeSpan.FromSeconds(60))
                Do

                    System.Windows.Forms.Application.DoEvents()

                    ' check if we have data
                Loop Until Me._codeNetDevice.HasPacket OrElse DateTime.Now > endWaitTime

                If Me._codeNetDevice.HasPacket Then
                    ' acknowledge
                    Me._codeNetDevice.Send("A")
                End If

            End If

            ' display as actual
            displayActualSettings()

        Catch ex As Exception
            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED STARTING . Details: {0}", ex)
        End Try

    End Sub

#End Region

#Region " ACTIONS MENU HANDLERS "

    Private Sub actionsClearMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _actionsClearMenuItem.Click
        Me._terminalTextBox.Text = String.Empty
    End Sub

    Private Sub actionsExitMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _actionsExitMenuItem.Click
        ' do not close anything
        Me.Hide()
    End Sub

    ''' <summary>
    ''' Sends the Send Text Box text.
    ''' </summary>
      Private Sub actionsSendMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _actionsSendMenuItem.Click
        Me._codeNetDevice.Send(Me._sendTextBox.Text)
    End Sub

#End Region

#Region " DOMINO MENU HANDLERS "

    ''' <summary>
    ''' Enable1 head 1
    ''' </summary>
    ''' <history>
    ''' 01/24/01  David Hary  1.0.01 Add messages for Domain to save, select, enable, status, and print
    ''' 01/25/01  David Hary  1.0.02 Remove vbCr at the end of message
    ''' </history>
    Private Sub dominoEnableHead1MenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _dominoEnableHead1MenuItem.Click

        Try

            Dim escChar As String
            escChar = Convert.ToChar(27)

            Dim eotChar As String
            eotChar = Convert.ToChar(4)

            Dim queryCommand As String

            queryCommand = escChar & "Q1Y" ' Enable head 1
            queryCommand = queryCommand & eotChar
            Me._codeNetDevice.Send(queryCommand, True, 1000, True)

            ' display as actual
            displayActualSettings()

        Catch ex As Exception
            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED  ENABLING DOMINO HEAD 1. Details: {0}", ex)
        End Try

    End Sub

    Private Sub dominoGetConfigMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _dominoGetConfigMenuItem.Click

        Try

            Dim escChar As String
            escChar = Convert.ToChar(27)

            Dim eotChar As String
            eotChar = Convert.ToChar(4)

            Dim queryCommand As String

            queryCommand = escChar & "B?" & eotChar
            Me._codeNetDevice.Send(queryCommand, True, 1000, True)

            ' display as actual
            displayActualSettings()

        Catch ex As Exception

            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED GETTING DOMINO CONFIGURATION. Details: {0}", ex)

        End Try

    End Sub

    Private Sub dominoGetStatusMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _dominoGetStatusMenuItem.Click

        Try

            Dim escChar As String
            escChar = Convert.ToChar(27)

            Dim eotChar As String
            eotChar = Convert.ToChar(4)

            Dim queryCommand As String

            queryCommand = escChar & "1C?" & eotChar
            Me._codeNetDevice.Send(queryCommand, True, 1000, True)

            ' display as actual
            displayActualSettings()

        Catch ex As Exception

            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED GETTING DOMINO STATUS. Details: {0}", ex)

        End Try

    End Sub

    Private Sub dominoIdentifyMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _dominoIdentifyMenuItem.Click

        Try

            Dim escChar As String
            escChar = Convert.ToChar(27)

            Dim eotChar As String
            eotChar = Convert.ToChar(4)

            Dim queryCommand As String

            queryCommand = escChar & "A?" & eotChar
            Me._codeNetDevice.Send(queryCommand, True, 1000, True)

            ' display as actual
            displayActualSettings()

        Catch ex As Exception

            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED GETTING DOMINO IDENTITY. Details: {0}", ex)

        End Try

    End Sub

    Private Sub dominoPrintGoMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _dominoPrintGoMenuItem.Click

        Try

            Dim escChar As String
            escChar = Convert.ToChar(27)

            Dim eotChar As String
            eotChar = Convert.ToChar(4)

            Dim queryCommand As String

            queryCommand = escChar & "N1" ' Go
            queryCommand = queryCommand & eotChar
            Me._codeNetDevice.Send(queryCommand, True, 1000, True)

            ' display as actual
            displayActualSettings()

        Catch ex As Exception

            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED PRINTING GO. Details: {0}", ex)

        End Try

    End Sub

    Private Sub dominoSaveMessage1MenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _dominoSaveMessage1MenuItem.Click

        Try

            Dim escChar As String
            escChar = Convert.ToChar(27)

            Dim eotChar As String
            eotChar = Convert.ToChar(4)

            Dim queryCommand As String

            queryCommand = escChar & "S001" ' store
            queryCommand = queryCommand & "David" ' print David
            '      queryCommand = queryCommand & "B"           ' Print B
            '      queryCommand = queryCommand & escChar & "u2"   ' size 2
            '      queryCommand = queryCommand & "E"           ' print E
            '      queryCommand = queryCommand & escChar & "u3"   ' size 3
            '      queryCommand = queryCommand & "I"           ' print I
            '      queryCommand = queryCommand & escChar & "u1"   ' size 1
            '      queryCommand = queryCommand & escChar & "r"    ' line feed
            '      queryCommand = queryCommand & "Duncan"      ' print Duncan
            queryCommand = queryCommand & eotChar
            Me._codeNetDevice.Send(queryCommand, True, 1000, True)

            ' display as actual
            displayActualSettings()

        Catch ex As Exception

            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED SAVING MESSAGE. Details: {0}", ex)

        End Try

    End Sub

    Private Sub dominoSelectMessage1MenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _dominoSelectMessage1MenuItem.Click

        Try

            Dim escChar As String
            escChar = Convert.ToChar(27)

            Dim eotChar As String
            eotChar = Convert.ToChar(4)

            Dim queryCommand As String

            queryCommand = escChar & "P1001" ' Select head 1 message 1
            queryCommand = queryCommand & eotChar
            Me._codeNetDevice.Send(queryCommand, True, 1000, True)

            ' display as actual
            displayActualSettings()

        Catch ex As Exception

            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED SELECTING MESSAGE 1. Details: {0}", ex)

        End Try

    End Sub

    Private Sub selectDominoAmjetMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _selectDominoAmjetMenuItem.Click

        Try

            ' clear checks
            Me._selectAccutrackerMenuItem.Checked = False
            Me._selectDghSensorsMenuItem.Checked = False
            Me._selectDominoAmjetMenuItem.Checked = False

            Me._accutrackerDevceMenu.Visible = False
            Me._dghDeviceMenu.Visible = False
            Me._dominoDeviceMenu1.Visible = False

            ' select the DOMINO device

            Me._codeNetDevice.ApplyDominoAmjetDefaults()
            Me._selectDominoAmjetMenuItem.Checked = True
            Me._dominoDeviceMenu1.Visible = True

            ' Interpret as Hex
            Me.IsInterpretCmdAsHex = True

            ' display as actual
            displayActualSettings()


        Catch ex As Exception

            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED DOMINO MENU ITEM. Details: {0}", ex)

        End Try

    End Sub

#End Region

#Region " DGH MENU HANDLERS "

    Private Sub selectDghSensorsMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _selectDghSensorsMenuItem.Click

        Try

            ' clear checks
            Me._selectAccutrackerMenuItem.Checked = False
            Me._selectDghSensorsMenuItem.Checked = False
            Me._selectDominoAmjetMenuItem.Checked = False

            Me._accutrackerDevceMenu.Visible = False
            Me._dghDeviceMenu.Visible = False
            Me._dominoDeviceMenu1.Visible = False

            ' select the DGH device

            Me._codeNetDevice.ApplyDghDefaults()
            Me._selectDghSensorsMenuItem.Checked = True
            Me._dghDeviceMenu.Visible = True

            ' Do not interpret as Hex
            Me.IsInterpretCmdAsHex = False

            ' display as actual
            displayActualSettings()


        Catch ex As Exception

            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED DGH MENU. Details: {0}", ex)

        End Try

    End Sub

    Private Sub readAndZeroDghMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _readAndZeroDghMenuItem.Click

        Try

            Me._codeNetDevice.Send("$1RZ" & Convert.ToChar(13), True, 1000, True)

            ' display as actual
            displayActualSettings()


        Catch ex As Exception

            ' display the error message
            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED READING DGH. Details: {0}", ex)

        End Try

    End Sub

    Private Sub readDghDataMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _readDghDataMenuItem.Click

        Try

            Me._codeNetDevice.Send("$1RD" & Convert.ToChar(13), True, 1000, True)

            ' display as actual
            displayActualSettings()

        Catch ex As Exception

            ' display the error message
            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED READING DGH. Details: {0}", ex)

        End Try

    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs after the form is closed.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>This event is a notification that the form has already gone away before
    ''' control is returned to the calling method (in case of a modal form).  Use this
    ''' method to delete any temporary files that were created or dispose of any objects
    ''' not disposed with the closing event.
    ''' </remarks>
    Private Sub form_Closed(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Closed

    End Sub

    ''' <summary>Occurs before the form is closed</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
    ''' <remarks>Use this method to optionally cancel the closing of the form.
    ''' Because the form is not yet closed at this point, this is also the best 
    ''' place to serialize a form's visible properties, such as size and 
    ''' location. Finally, dispose of any form level objects especially those that
    ''' might needs access to the form and thus should not be terminated after the
    ''' form closed.
    ''' </remarks>
    Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing

        ' disable the timer if any
        ' actionTimer.Enabled = False
        ' My.Application.DoEvents()
        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
        Try
            If Me._codeNetDevice IsNot Nothing Then
                RemoveHandler Me._codeNetDevice.DataReceived, AddressOf Me._codeNetDevice_DataReceived
                RemoveHandler Me._codeNetDevice.ErrorReceived, AddressOf Me._codeNetDevice_ErrorReceived
                RemoveHandler Me._codeNetDevice.FinishedReceiving, AddressOf Me._codeNetDevice_FinishedReceiving
                RemoveHandler Me._codeNetDevice.FinishedTransmitting, AddressOf Me._codeNetDevice_FinishedTransmitting
                RemoveHandler Me._codeNetDevice.PinChanged, AddressOf Me._codeNetDevice_PinChanged
                RemoveHandler Me._codeNetDevice.SettingsChanged, AddressOf Me._codeNetDevice_SettingsChanged
                RemoveHandler Me._codeNetDevice.StartedReceiving, AddressOf Me._codeNetDevice_StartedReceiving
                RemoveHandler Me._codeNetDevice.StartedTransmitting, AddressOf Me._codeNetDevice_StartedTransmitting
            End If
            If Me._updateTimer IsNot Nothing Then
                RemoveHandler Me._updateTimer.Tick, AddressOf updateTimer_Tick
            End If

        Finally
            Me.Cursor = System.Windows.Forms.Cursors.Default
        End Try

    End Sub

    ''' <summary>
    ''' Sets the form caption
    ''' </summary>
    Private Function updateCaption() As Boolean

        If Me._codeNetDevice Is Nothing OrElse Me._codeNetDevice.SerialPort Is Nothing OrElse
            String.IsNullOrWhiteSpace(Me._codeNetDevice.SerialPort.PortName) Then
            Me.Text = My.Application.Info.Title & " " & My.Application.Info.Version.ToString
        Else
            Me.Text = My.Application.Info.Title & " " & Me._codeNetDevice.SerialPort.PortName & " " & My.Application.Info.Version.ToString
        End If
        Return True
    End Function

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

        Try

            ' instantiate form objects
            ' Me.instantiateObjects()

            ' set tool tips
            ' initializeUserInterface()

            ' set the form caption
            updateCaption()

            ' select the data tab
            Me._dataTabPage.Select()

            ' Display the form reflecting the current settings;
            If Me._codeNetDevice.SettingsSource = isr.CodeNet.SettingsSource.Recommended Then
                Me._autoLoadDefaultSettingsMenuItem.Checked = False
                Me._autoLoadRxSettingsMenuItem.Checked = True
            ElseIf Me._codeNetDevice.SettingsSource = isr.CodeNet.SettingsSource.Defaults Then
                Me._autoLoadDefaultSettingsMenuItem.Checked = True
                Me._autoLoadRxSettingsMenuItem.Checked = False
            End If

            ' display the current port settings
            Me.displayActualSettings()

            ' Center the form
            Me.CenterToScreen()

            ' enable the display timer
            Me._updateTimer.Enabled = True

            ' ready
            Me._statusPanel.Text = "Ready"

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " CODE NET DEVICE "

    ''' <summary>Gets or sets the instance of the CodeNet object.</summary>
    Private WithEvents _codeNetDevice As isr.CodeNet.CodeNetDevice


#End Region

#Region " PORT "

    ''' <summary>Clears the port.
    ''' </summary>
    ''' <returns>True if the Clear was acknowledged and processed.
    ''' </returns>
    ''' <history>
    ''' </history>
    Private Function ClearSerialPort() As Boolean

        Dim promptResult As Windows.Forms.DialogResult

        ' check if the port is open
        Dim newMessage As String
        If Me._codeNetDevice.SerialPort.IsOpen Then

            ' if so, check if we have data in the output buffer
            If Me._codeNetDevice.SerialPort.BytesToWrite <= 0 Then

                ' if no data in the buffer, clear the port

                ' cancel will clear the port
                promptResult = Windows.Forms.DialogResult.Cancel

            Else

                ' if so, alert the operator

                ' if we have data in the buffer, alert the
                ' operator if this form is open
                newMessage = "There is still data in the output buffer. If you clear the port now, the data will be lost.  Press Retry to wait for the data to clear, Cancel to clear the port."

                promptResult = Windows.Forms.MessageBox.Show(newMessage, "PORT HAS DATA", 
                                                             Windows.Forms.MessageBoxButtons.OKCancel, 
                                                             Windows.Forms.MessageBoxIcon.Exclamation, 
                                                             Windows.Forms.MessageBoxDefaultButton.Button1, 
                                                             Windows.Forms.MessageBoxOptions.DefaultDesktopOnly)
            End If ' OutBufferCount = 0

            ' check the return value
            If promptResult = Windows.Forms.DialogResult.Cancel Then

                ' clear the port
                Me._codeNetDevice.ClearPort()

                ' show Message
                Me._statusPanel.Text = "Port cleared"

            Else

                ' return not=acknowledged
                Return False

            End If

        End If

        ' return acknowledge
        Return True

    End Function

    ''' <summary>Closes the port.
    '''   This method checks first for errors or data in the
    '''   buffer and then closes the port.
    ''' </summary>
    ''' <returns>Returns true if the Close was acknowledged and processed.
    ''' </returns>
    ''' <history>
    ''' </history>
    Friend Function ClosePort() As Boolean

        Dim promptResults As Windows.Forms.DialogResult

        ' check if the port is open
        Dim newMessage As String

        If Me._codeNetDevice.SerialPort.IsOpen Then

            ' if so, check if we have data in the output buffer
            If Me._codeNetDevice.SerialPort.BytesToWrite <= 0 Then

                ' if no data in the buffer, close the port

                ' cancel will close the port
                promptResults = Windows.Forms.DialogResult.Cancel

            Else

                ' check if this form is visible
                If Me.Visible Then

                    ' if so, alert the operator

                    ' if we have data in the buffer, alert the
                    ' operator if this form is open
                    newMessage = "There is still data in the output buffer. If you close the port now, the data will be lost.  Press Retry to wait for the data to clear, Cancel to close the port."

                    promptResults = Windows.Forms.MessageBox.Show(newMessage, "DATA IN BUFFER", 
                                                                  Windows.Forms.MessageBoxButtons.OKCancel, 
                                                                  Windows.Forms.MessageBoxIcon.Exclamation, Windows.Forms.MessageBoxDefaultButton.Button1, 
                                                                   Windows.Forms.MessageBoxOptions.DefaultDesktopOnly)

                Else

                    promptResults = Windows.Forms.DialogResult.Cancel

                End If

            End If ' OutBufferCount = 0

            ' check the return value
            If promptResults = Windows.Forms.DialogResult.Cancel Then

                ' if cancel,
                Me._codeNetDevice.ClosePort()

                ' show message
                Me._statusPanel.Text = "Port closed"

            Else

                ' return not=acknowledged
                Return False

            End If

        Else

            ' if port already closed, say so
            Me._statusPanel.Text = "The port is already closed!"

        End If

        ' return acknowledge
        Return True

    End Function

    ''' <summary>Dials a modem to connect.
    ''' </summary>
    ''' <param name="numberToDial">
    ''' </param>
    ''' <history>
    ''' </history>
    Private Sub dialModem(ByVal numberToDial As String)

        Try

            ' display status
            Me._statusPanel.Text = "Dialing..."

            ' Dial the modem:
            Me._codeNetDevice.DialModem(numberToDial)

        Catch ex As Exception

            If Me.Visible Then
                Windows.Forms.MessageBox.Show(ex.Message, "FAILED DIALING", Windows.Forms.MessageBoxButtons.OK, 
                                              Windows.Forms.MessageBoxIcon.Exclamation, 
                                               Windows.Forms.MessageBoxDefaultButton.Button1, 
                                                Windows.Forms.MessageBoxOptions.DefaultDesktopOnly)
            Else
                ' raise the error
                Throw
            End If

        End Try


    End Sub

    ''' <summary>Terminates the connection.
    ''' </summary>
    ''' <history>
    ''' </history>
    Private Sub hangUp()

        ' display status
        Me._statusPanel.Text = "Hanging up"

        Me._codeNetDevice.HangUp()

        ' Update status
        Me._statusPanel.Text = "Port Closed"

    End Sub

    ''' <summary>
    ''' Toggles interpreting commands as hex strings.
    ''' </summary>
    Public Property IsInterpretCmdAsHex() As Boolean
        Get
            Return Me._interpretTxAsHexMenuItem.Checked
        End Get
        Set(ByVal Value As Boolean)
            Me._interpretTxAsHexMenuItem.Checked = Value
        End Set
    End Property

    ''' <summary>Sets auto answer mode
    ''' </summary>
    Private Sub modemAutoAnswer()

        Me._codeNetDevice.AutoAnswer()

        ' Update status
        Me._statusPanel.Text = "Port Open - Answer Mode"

    End Sub

    ''' <summary>opens the port.
    ''' </summary>
    ''' <history>
    ''' </history>
    Public Sub OpenPort()

        If Me._codeNetDevice.IsUsingInstrument AndAlso Not Me._codeNetDevice.SerialPort.IsOpen Then

            ' Otherwise, open the port.
            Me._codeNetDevice.OpenPort()

        End If

        If Me.Visible Then

            ' check if the port is open
            If Me._codeNetDevice.SerialPort.IsOpen Then

                ' display status
                Me._statusPanel.Text = "Port opened"

            Else

                ' display status
                Me._statusPanel.Text = "Port failed to open"

            End If

            ' update the caption in case the port name changed.
            Me.updateCaption()

        End If


    End Sub

    ''' <summary>Updates the received data text box
    ''' </summary>
    Private Sub updateReceiveTextBox(ByVal newMessage As String)

        ' Display in text box if selected:
        If Me.Visible Then

            ' limit text box to Max Len  characters:
            If Me._receiveTextBox.Text.Length + newMessage.Length > _receiveTextBox.MaxLength Then
                Me._receiveTextBox.Text = Me._receiveTextBox.Text.Substring(Me._receiveTextBox.MaxLength - newMessage.Length - 1)
            End If

            ' Display text:
            Me._receiveTextBox.SelectionStart = Me._receiveTextBox.Text.Length
            Me._receiveTextBox.SelectedText = newMessage

        End If

    End Sub

#End Region

#Region " PORT CONTROL HANDLERS "

    Private Sub dtrIndicator_Click(ByVal Sender As System.Object, ByVal e As EventArgs) Handles _dtrIndicator.Click
        Me._codeNetDevice.SerialPort.DtrEnable = Not Me._codeNetDevice.SerialPort.DtrEnable
    End Sub

    Private Sub rtsIndicator_Click(ByVal Sender As System.Object, ByVal e As EventArgs) Handles _rtsIndicator.Click
        Me._codeNetDevice.SerialPort.RtsEnable = Not Me._codeNetDevice.SerialPort.RtsEnable
    End Sub

    Private Sub interpretTxAsHexMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _interpretTxAsHexMenuItem.Click
        Me._interpretTxAsHexMenuItem.Checked = Not Me._interpretTxAsHexMenuItem.Checked
    End Sub

    Private Sub clearPortMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _clearPortMenuItem.Click
        Try
            ClearSerialPort()
        Catch ex As Exception
            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED CLEARING THE PORT. Details: {0}", ex)
        End Try
    End Sub

    Private Sub closePortMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _closePortMenuItem.Click
        Try
            ClosePort()
        Catch ex As Exception
            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED CLOSING PORT. Details: {0}", ex)
        End Try
    End Sub

    Private Sub openPortMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _openPortMenuItem.Click
        Try
            Me.OpenPort()
        Catch ex As Exception
            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED OPENING PORT. Details: {0}", ex)
        End Try
    End Sub

    Private Sub showPortEventsMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _showPortEventsMenuItem.Click
        Me._showPortEventsMenuItem.Checked = Not Me._showPortEventsMenuItem.Checked
    End Sub

    Private Sub showReceivedDataMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _showReceivedDataMenuItem.Click
        Me._showReceivedDataMenuItem.Checked = Not Me._showReceivedDataMenuItem.Checked
    End Sub

#End Region

#Region " SETTINGS HANDLERS "

    Private _actualSettingsLevel As SettingsLevel
    Private Property actualSettingsLevel() As SettingsLevel
        Get
            Return Me._actualSettingsLevel
        End Get
        Set(ByVal value As SettingsLevel)
            If Me._actualSettingsLevel <> value Then
                Me._actualSettingsTextBoxLabel.Text = value.ToString
            Else
            End If
            Me._actualSettingsLevel = value
        End Set
    End Property

    ''' <summary>Loads and applies the default settings.
    ''' Reads the default settings from the configuration file and apply them to the port.
    ''' </summary>
    ''' <history>
    ''' </history>
    Private Sub applyDefaults()

        ' apply the defaults
        Me._codeNetDevice.ApplyDefaults()

        ' display the actual settings
        If Me.Visible Then
            Me.displayActualSettings()
            ' update the caption in case the port name changed.
            Me.updateCaption()
        End If

    End Sub

    ''' <summary>Applies the current settings to the port.
    '''   Applies the settings entered in the text
    '''   boxes to the control.  Settings are not saved to the
    '''   configuration file.
    ''' </summary>
    ''' <history>
    ''' </history>
    Private Sub applyNewSettings()

        ' true if port was open when setting were applied
        Dim wasOpen As Boolean

        ' check if the serial port is open
        If Me._codeNetDevice.SerialPort.IsOpen() Then
            ' if so, close and mark it as 'WasOpen'
            If Me._codeNetDevice.IsUsingInstrument Then
                Me._codeNetDevice.SerialPort.Close()
            End If
            wasOpen = True
        Else
            ' the port was not open
            wasOpen = False
        End If

        ' now apply the settings
        Me._codeNetDevice.PortName = CodeNetDevice.BuildPortName(Me._portNumberTextBox.Text)
        Me._codeNetDevice.SerialPort.DtrEnable = Me._dtrEnableToggle.Checked
        Me._codeNetDevice.SerialPort.Handshake = CType(Me._handshakingComboBox.SelectedIndex, IO.Ports.Handshake)
        Me._codeNetDevice.SerialPort.ReadBufferSize = CInt(Me._inputBufferSizeTextBox.Text)
        'TO_DO: deprecated Me._serialPort.InputLength = CShort(Me._inputLengthTextBox.Text)
        'TO_DO: deprecated Me._serialPort.InputMode = CType(Me._InputModeComboBox.SelectedIndex, MSCommLib.InputModeConstants)
        Me._codeNetDevice.SerialPort.DiscardNull = Me._discardNullToggle.Checked
        Me._codeNetDevice.SerialPort.WriteBufferSize = CShort(Me._writeBufferSizeTextBox.Text)
        Me._codeNetDevice.SerialPort.ParityReplace = CodeNetDevice.ConvertToByte(Me._parityReplaceCharTextBox.Text)
        Me._codeNetDevice.SerialPort.ReceivedBytesThreshold = CInt(Me._rxThresholdTextBox.Text)
        Me._codeNetDevice.SerialPort.RtsEnable = Me._rtsEnableToggle.Checked
        Me._codeNetDevice.PortSettings = Me._portSettingsTextBox.Text
        ' TO_DO: deprecated Me._serialPort.SThreshold = CShort(Me._txThresholdTextBox.Text)

        ' check if the port was open
        If wasOpen = True Then
            ' if so, open the port
            If Me._codeNetDevice.IsUsingInstrument Then
                Me._codeNetDevice.SerialPort.Open()
            End If
        End If

        ' display the actual settings
        If Me.Visible Then
            ' update the caption in case the port name changed.
            Me.updateCaption()
            Me.displayActualSettings()
        End If

    End Sub

    ''' <summary>Applies the current packet settings to the port.
    '''   Applies the settings entered in the text
    '''   boxes to the control.  Settings are not saved to the
    '''   configuration file.
    ''' </summary>
    ''' <history>
    ''' 04/02/02  David Hary  1.2.02  Add ENQ, STX, and ETX
    ''' </history>
    Private Sub applyPacketSettings()

        ' true if port was open when setting were applied
        Dim wasOpen As Boolean

        ' check if the serial port is open
        If Me._codeNetDevice.SerialPort.IsOpen() Then
            ' if so, close and mark it as 'WasOpen'
            If Me._codeNetDevice.IsUsingInstrument Then
                Me._codeNetDevice.SerialPort.Close()
            End If
            wasOpen = True
        Else
            ' the port was not open
            wasOpen = False
        End If

        ' apply packet settings
        Me._codeNetDevice.AcknowledgedByte = CByte(Me._acknowledgeByteTextBox.Text)
        Me._codeNetDevice.EndTransmissionByte1 = CByte(Me._endOfPacketByte1TextBox.Text)
        Me._codeNetDevice.EndTransmissionByte2 = CByte(Me._endPacketByte2TextBox.Text)
        Me._codeNetDevice.IsReplaceEndText = Me._replaceEotChecklBox.Checked
        Me._codeNetDevice.EndTextSubstitute = Me._eotSubstitueTextBox.Text
        Me._codeNetDevice.NotAcknowledgedByte = CByte(Me._notAcknowledgeByteTextBox.Text)
        Me._codeNetDevice.StartTransmissionByte = CByte(Me._startPacketByteTextBox.Text)
        Me._codeNetDevice.NotAcknowledgedLength = CInt(Me._nakMessageLengthTextBox.Text)
        Me._codeNetDevice.ExpectedPacketLength = CInt(Me._packetLengthTextBox.Text)
        Me._codeNetDevice.StartTextByte = CByte(Me._startOfTextByteTextBox.Text)
        Me._codeNetDevice.EndTextByte = CByte(Me._endOfTextByteTextBox.Text)
        Me._codeNetDevice.EnquiryByte = CByte(Me._enquiryByteTextBox.Text)

        ' check if the port was open
        If wasOpen = True Then
            ' if so, open the port
            If Me._codeNetDevice.IsUsingInstrument Then
                Me._codeNetDevice.SerialPort.Open()
            End If
        End If

        ' display the actual settings
        If Me.Visible Then
            ' update the caption in case the port name changed.
            Me.updateCaption()
            Me.displayActualSettings()
        End If

    End Sub

    ''' <summary>Applies the recommended settings to the port.
    '''   Applies the settings entered in the
    '''   label boxes to the control.  Settings are not saved
    '''   to the configuration file.
    ''' </summary>
    ''' <history>
    ''' </history>
    Friend Sub DisplayRecommendedSetting()

        ' now display the recommended settings
        Me._recommendedPortNumberLabel.Text = CStr(CodeNetDevice.ParsePortNumber(Me._codeNetDevice.PortName))
        Me.__recommendedDtrEnableLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, 
                                                      "{0:True/False}", Me._codeNetDevice.SerialPort.DtrEnable)
        Me.__recommendedHandshakeLabel.Text = Me._codeNetDevice.SerialPort.Handshake.ToString()
        Me.__recommendedInputBufferSizeLabel.Text = CStr(Me._codeNetDevice.SerialPort.ReadBufferSize)
        ' deprecated
        Me._recommendedInputLengthLabel.Text = CStr(0) ' Deprecated - no longer used. 
        ' Me._recommendedInputModeLabel.Text = Me._serialPort.InputMode.ToString
        Me._recommendedDiscardNullLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, 
                                                      "{0:True/False}", Me._codeNetDevice.SerialPort.DiscardNull)
        Me._recommendedWriteBufferSizeLabel.Text = CStr(Me._codeNetDevice.SerialPort.WriteBufferSize)
        Me._recommendedParityReplaceCharLabel.Text = Convert.ToChar(Me._codeNetDevice.SerialPort.ParityReplace)
        Me._recommendedRxThresholdLabel.Text = CStr(Me._codeNetDevice.SerialPort.ReceivedBytesThreshold)
        Me._recommendedRtsEnableLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, 
                                                      "{0:True/False}", Me._codeNetDevice.SerialPort.RtsEnable)
        '_recommendedPortSettingsLabel.Text = Me._serialPort.Settings
        '_recommendedTxThresholdLabel.Text = CStr(Me._serialPort.SThreshold)

    End Sub

    ''' <summary>Displays the current port settings.
    ''' </summary>
    ''' <history>
    ''' 04/02/02  David Hary  1.2.02  Add ENQ, STX, and ETX
    ''' </history>
    Friend Sub displayActualSettings()

        ' indicate that we now have actual settings
        Me.actualSettingsLevel = SettingsLevel.Actual

        ' display the port settings.
        Me._portNumberTextBox.Text = CodeNetDevice.ParsePortNumber(Me._codeNetDevice.PortName).ToString(Globalization.CultureInfo.CurrentCulture)
        Me._dtrEnableToggle.Checked = Me._codeNetDevice.SerialPort.DtrEnable
        Me._handshakingComboBox.SelectedIndex = Me._codeNetDevice.SerialPort.Handshake
        Me._inputBufferSizeTextBox.Text = Me._codeNetDevice.SerialPort.ReadBufferSize.ToString(Globalization.CultureInfo.CurrentCulture)
        Me._inputLengthTextBox.Text = "0" ' Deprecated - no longer used Me._serialPort.InputLen.ToString(Globalization.CultureInfo.CurrentCulture)
        '_InputModeComboBox.SelectedIndex = Me._serialPort.InputMode
        Me._discardNullToggle.Checked = Me._codeNetDevice.SerialPort.DiscardNull
        Me._writeBufferSizeTextBox.Text = Me._codeNetDevice.SerialPort.WriteBufferSize.ToString(Globalization.CultureInfo.CurrentCulture)
        Me._parityReplaceCharTextBox.Text = Convert.ToChar(Me._codeNetDevice.SerialPort.ParityReplace)
        Me._rxThresholdTextBox.Text = Me._codeNetDevice.SerialPort.ReceivedBytesThreshold.ToString(Globalization.CultureInfo.CurrentCulture)
        Me._rtsEnableToggle.Checked = Me._codeNetDevice.SerialPort.RtsEnable
        Me._portSettingsTextBox.Text = Me._codeNetDevice.PortSettings
        'TO_DO: deprecated - remove
        Me._txThresholdTextBox.Text = "1" ' Me._serialPort.SThreshold.ToString(Globalization.CultureInfo.CurrentCulture)

        ' display packet settings here
        Me._acknowledgeByteTextBox.Text = Me._codeNetDevice.AcknowledgedByte.ToString(Globalization.CultureInfo.CurrentCulture)
        Me._endOfPacketByte1TextBox.Text = Me._codeNetDevice.EndTransmissionByte1.ToString(Globalization.CultureInfo.CurrentCulture)
        Me._endPacketByte2TextBox.Text = Me._codeNetDevice.EndTransmissionByte2.ToString(Globalization.CultureInfo.CurrentCulture)
        Me._replaceEotChecklBox.Checked = Me._codeNetDevice.IsReplaceEndText
        Me._eotSubstitueTextBox.Text = Me._codeNetDevice.EndTextSubstitute
        Me._notAcknowledgeByteTextBox.Text = Me._codeNetDevice.NotAcknowledgedByte.ToString(Globalization.CultureInfo.CurrentCulture)
        Me._startPacketByteTextBox.Text = Me._codeNetDevice.StartTransmissionByte.ToString(Globalization.CultureInfo.CurrentCulture)
        Me._nakMessageLengthTextBox.Text = Me._codeNetDevice.NotAcknowledgedLength.ToString(Globalization.CultureInfo.CurrentCulture)
        Me._packetLengthTextBox.Text = Me._codeNetDevice.ExpectedPacketLength.ToString(Globalization.CultureInfo.CurrentCulture)
        Me._startOfTextByteTextBox.Text = Me._codeNetDevice.StartTextByte.ToString(Globalization.CultureInfo.CurrentCulture)
        Me._endOfTextByteTextBox.Text = Me._codeNetDevice.EndTextByte.ToString(Globalization.CultureInfo.CurrentCulture)
        Me._enquiryByteTextBox.Text = Me._codeNetDevice.EnquiryByte.ToString(Globalization.CultureInfo.CurrentCulture)

    End Sub

    ''' <summary>Reads and displays the default settings.
    ''' </summary>
    ''' <history>
    ''' 04/02/02  David Hary  1.2.02  Add ENQ, STX, and ETX
    ''' </history>
    Private Sub displayDefaults()

        ' show the settings
        Me._portNumberTextBox.Text = CStr(My.Settings.PortNumber)
        Me._dtrEnableToggle.Checked = My.Settings.DtrEnable

        Me._handshakingComboBox.Enabled = False
        Me._handshakingComboBox.SelectedIndex = My.Settings.Handshake
        Me._handshakingComboBox.Enabled = True

        Me._inputBufferSizeTextBox.Text = CStr(My.Settings.ReadBufferSize)
        Me._inputLengthTextBox.Text = CStr(0) ' Deprecated ' My.Settings.BytesToRead)

        '_InputModeComboBox.Enabled = False
        '_InputModeComboBox.SelectedIndex = My.Settings.Encoding
        '_InputModeComboBox.Enabled = True

        Me._discardNullToggle.Checked = My.Settings.DiscardNull
        Me._writeBufferSizeTextBox.Text = CStr(My.Settings.WriteBufferSize)
        Me._parityReplaceCharTextBox.Text = CStr(My.Settings.ParityReplace)
        Me._rxThresholdTextBox.Text = CStr(My.Settings.ReceivedBytesThreshold)
        Me._rtsEnableToggle.Checked = My.Settings.RtsEnable
        Me._portSettingsTextBox.Text = My.Settings.PortSettings
        'TO_DO:  deprecated - remove
        Me._txThresholdTextBox.Text = "1" ' CStr(My.Settings.SThreshold)

        ' indicate that we have defaults on screen
        Me.actualSettingsLevel = SettingsLevel.[Defaults]

        ' display packet settings here
        Me._acknowledgeByteTextBox.Text = CStr(My.Settings.AcknowledgedByte)
        Me._endOfPacketByte1TextBox.Text = CStr(My.Settings.EndTransmissionByte1)
        Me._endPacketByte2TextBox.Text = CStr(My.Settings.EndTransmissionByte2)
        Me._replaceEotChecklBox.Checked = My.Settings.IsReplaceEndText
        Me._eotSubstitueTextBox.Text = CStr(My.Settings.EndTextSubstitute)
        Me._notAcknowledgeByteTextBox.Text = CStr(My.Settings.NotAcknowledgedByte)
        Me._startPacketByteTextBox.Text = CStr(My.Settings.StartTransmissionByte)
        Me._nakMessageLengthTextBox.Text = CStr(My.Settings.NotAcknowledgedLength)
        Me._packetLengthTextBox.Text = CStr(My.Settings.ExpectedPacketLength)
        Me._startOfTextByteTextBox.Text = CStr(My.Settings.StartTextByte)
        Me._endOfTextByteTextBox.Text = CStr(My.Settings.EndTextByte)
        Me._enquiryByteTextBox.Text = CStr(My.Settings.EnquiryByte)

    End Sub

    Private _packetSettingsLevel As SettingsLevel
    Private Property packetSettingsLevel() As SettingsLevel
        Get
            Return Me._packetSettingsLevel
        End Get
        Set(ByVal value As SettingsLevel)
            If Me._packetSettingsLevel <> value Then
                Me.PacketActualLabel.Text = value.ToString
            Else
            End If
            Me._packetSettingsLevel = value
        End Set
    End Property

    ''' <summary>
    ''' Enumerates the status of the settings.
    ''' </summary>
    Private Enum SettingsLevel
        None
        [New]
        [Actual]
        [Defaults]
    End Enum

#End Region

#Region " SETTINGS CONTROL HANDLERS "

    Private Sub actualSettingsChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _portNumberTextBox.TextChanged,
                                            _portSettingsTextBox.TextChanged, _dtrEnableToggle.CheckStateChanged, _handshakingComboBox.SelectedValueChanged,
                                            _inputBufferSizeTextBox.TextChanged, _discardNullToggle.CheckStateChanged, _writeBufferSizeTextBox.TextChanged,
                                            _parityReplaceCharTextBox.TextChanged, _rtsEnableToggle.CheckStateChanged, _InputModeComboBox.SelectedValueChanged,
                                            _rxThresholdTextBox.TextChanged, _txThresholdTextBox.TextChanged
        ' indicate that we now have new settings        
        Me.actualSettingsLevel = SettingsLevel.New
        ' deprecated   and Me._inputLengthTextBox.TextChanged, 
    End Sub

    Private Sub packetSettingsChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _acknowledgeByteTextBox.TextChanged,
                                        _endOfPacketByte1TextBox.TextChanged, _endPacketByte2TextBox.TextChanged, _endOfTextByteTextBox.TextChanged,
                                        _enquiryByteTextBox.TextChanged, _eotSubstitueTextBox.TextChanged, _nakMessageLengthTextBox.TextChanged,
                                        _notAcknowledgeByteTextBox.TextChanged, _packetLengthTextBox.TextChanged, _replaceEotChecklBox.CheckStateChanged,
                                        _startPacketByteTextBox.TextChanged, _startOfTextByteTextBox.TextChanged
        ' indicate that we now have new settings
        Me.packetSettingsLevel = SettingsLevel.New

    End Sub

    Private Sub saveActualsAsDefaultsMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _saveActualsAsDefaultsMenuItem.Click
        Try
            ' save actual settings as defaults
            Me._codeNetDevice.SaveSettings()
        Catch ex As Exception
            ' display the error message
            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED SAVING ACTUAL SETTINGS AS DEFAULTS. Details: {0}", ex)
        End Try
    End Sub

#End Region

#Region " SETTINGS MENU HANDLERS "

    Private Sub applyNewSettingsMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _applyNewSettingsMenuItem.Click

        Try

            ' apply the new settings
            applyNewSettings()

            ' display as actual
            displayActualSettings()

        Catch ex As Exception

            ' display the error message
            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED APPLYING NEW SETTINGS. Details: {0}", ex)

        End Try

    End Sub

    Private Sub applyPacketSettingsMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _applyPacketSettingsMenuItem.Click

        Try

            Call applyPacketSettings()

        Catch ex As Exception

            ' display the error message
            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED APPLYING PACKET SETTINGS. Details: {0}", ex)

        End Try

    End Sub

    Private Sub applyRxSettingsMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _applyRxSettingsMenuItem.Click

        Try

            ' apply the recommended settings
            Me._codeNetDevice.ApplyRecommendedSetting()

            ' display as actual
            Call displayActualSettings()

        Catch ex As Exception

            ' display the error message
            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED APPLYING RECEIVE SETTINGS. Details: {0}", ex)

        End Try

    End Sub

    Private Sub autoLoadDefaultSettingsMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _autoLoadDefaultSettingsMenuItem.Click

        Me._autoLoadDefaultSettingsMenuItem.Checked = Not Me._autoLoadDefaultSettingsMenuItem.Checked
        If Me._autoLoadDefaultSettingsMenuItem.Checked Then
            Me._autoLoadRxSettingsMenuItem.Checked = False
        End If

        If Me._autoLoadDefaultSettingsMenuItem.Checked Then

            Me._codeNetDevice.SettingsSource = isr.CodeNet.SettingsSource.Defaults

        ElseIf Me._autoLoadRxSettingsMenuItem.Checked Then

            Me._codeNetDevice.SettingsSource = isr.CodeNet.SettingsSource.Recommended

        Else

            Me._codeNetDevice.SettingsSource = isr.CodeNet.SettingsSource.None

        End If

        ' update the configuration file
        Me._codeNetDevice.SaveSettings()

    End Sub

    Private Sub autoLoadRecSettingsMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _autoLoadRxSettingsMenuItem.Click

        Me._autoLoadRxSettingsMenuItem.Checked = Not Me._autoLoadRxSettingsMenuItem.Enabled
        If Me._autoLoadRxSettingsMenuItem.Checked Then
            Me._autoLoadDefaultSettingsMenuItem.Checked = False
        End If

        If Me._autoLoadDefaultSettingsMenuItem.Checked Then

            Me._codeNetDevice.SettingsSource = isr.CodeNet.SettingsSource.Defaults

        ElseIf Me._autoLoadRxSettingsMenuItem.Checked Then

            Me._codeNetDevice.SettingsSource = isr.CodeNet.SettingsSource.Recommended

        Else

            Me._codeNetDevice.SettingsSource = isr.CodeNet.SettingsSource.None

        End If

        ' update the configuration file
        Me._codeNetDevice.SaveSettings()

    End Sub

    Private Sub displayActualsMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _displayActualsMenuItem.Click

        Try

            ' display actual settings
            Call Me.displayActualSettings()

        Catch ex As Exception

            ' display the error message
            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED DISPLAYING ACTUAL SETTINGS. Details: {0}", ex)

        End Try

    End Sub

    Private Sub displayDefaultsMenuItem_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _displayDefaultsMenuItem.Click

        Try

            ' load default settings
            Call displayDefaults()

        Catch ex As Exception

            ' display the error message
            addMessage(TraceEventType.Error, "EXCEPTION OCCURRED DISPLAYING DEFAULT SETTINGS. Details: {0}", ex)

        End Try

    End Sub

#End Region

#Region " TIMER AND PORT HANDLERS "

    Dim propertySetter As isr.Core.ThreadSafePropertySetter

    Private Sub safeAppendText(ByVal control As TextBox, ByVal value As String)

        ' limit text box to TerminalTextBox.MaxLength characters:
        If control.Text.Length + value.Length > control.MaxLength Then
            propertySetter.SetControlText(control, control.Text.Substring(control.MaxLength - value.Length - 1))
        End If

        ' display the events
        propertySetter.SetCtrlProperty(Of Integer)(control, "SelectionStart", control.Text.Length)
        propertySetter.SetCtrlProperty(Of String)(control, "SelectedText", value)

    End Sub

    Private Sub _codeNetDevice_DataReceived(ByVal sender As Object, ByVal e As System.IO.Ports.SerialDataReceivedEventArgs) Handles _codeNetDevice.DataReceived

        Select Case e.EventType

            Case IO.Ports.SerialData.Chars

            Case IO.Ports.SerialData.Eof

                If Me._showPortEventsMenuItem.Checked Then

                    ' display the events
                    Me.safeAppendText(Me._terminalTextBox, String.Format(Globalization.CultureInfo.CurrentCulture,
                                                                      "Data Received = '{0}'{1}", e.EventType, Environment.NewLine))
                End If

        End Select

    End Sub

    Private Sub _codeNetDevice_ErrorReceived(ByVal sender As Object, ByVal e As System.IO.Ports.SerialErrorReceivedEventArgs) Handles _codeNetDevice.ErrorReceived
        If Me._showPortEventsMenuItem.Checked Then
            ' display the events
            Me.safeAppendText(Me._terminalTextBox, String.Format(Globalization.CultureInfo.CurrentCulture,
                                                              "Error Received = '{0}'{1}", e.EventType, Environment.NewLine))
        End If

        Select Case e.EventType

            Case IO.Ports.SerialError.Frame

                propertySetter.SetCtrlProperty(Of String)(Me._statusPanel, "Text", "Framing")

            Case IO.Ports.SerialError.Overrun

                propertySetter.SetCtrlProperty(Of String)(Me._statusPanel, "Text", "Overrun")

            Case IO.Ports.SerialError.RXOver

                propertySetter.SetCtrlProperty(Of String)(Me._statusPanel, "Text", "OverFlow")

            Case IO.Ports.SerialError.RXParity

                propertySetter.SetCtrlProperty(Of String)(Me._statusPanel, "Text", "Parity")

            Case IO.Ports.SerialError.TXFull

                propertySetter.SetCtrlProperty(Of String)(Me._statusPanel, "Text", "TX Full")

        End Select
    End Sub

    Private Sub _codeNetDevice_FinishedReceiving(ByVal sender As Object, ByVal e As System.EventArgs) Handles _codeNetDevice.FinishedReceiving
        Me._receiveTextBox.Text = Me._codeNetDevice.LastReply
        If Me._showReceivedDataMenuItem.Checked Then
            Me.safeAppendText(Me._terminalTextBox, CodeNetDevice.ConvertToHexPairOrAscii(Me._codeNetDevice.ReceivedSubPacketRaw))
        End If
        ' turn off receive light
        propertySetter.SetCtrlProperty(Of Boolean)(Me._rxIndicator, "IsOn", False)
    End Sub

    Private Sub _codeNetDevice_FinishedTransmitting(ByVal sender As Object, ByVal e As System.EventArgs) Handles _codeNetDevice.FinishedTransmitting

        ' turn off send light
        Me._rxIndicator.IsOn = False

        ' Update status
        Me._statusPanel.Text = "Done Transmitting"

    End Sub

    Private Sub _codeNetDevice_PinChanged(ByVal sender As Object, ByVal e As System.IO.Ports.SerialPinChangedEventArgs) Handles _codeNetDevice.PinChanged

        If Me._showPortEventsMenuItem.Checked Then
            ' display the events
            Me.safeAppendText(Me._terminalTextBox, String.Format(Globalization.CultureInfo.CurrentCulture,
                                                              "Pin Changed = '{0}'{1}", e.EventType, Environment.NewLine))
        End If

        Select Case e.EventType

            Case IO.Ports.SerialPinChange.Break

                propertySetter.SetCtrlProperty(Of String)(Me._statusPanel, "Text", "Break")

            Case IO.Ports.SerialPinChange.CDChanged

                propertySetter.SetCtrlProperty(Of Boolean)(Me._cdIndicator, "IsOn", Me._codeNetDevice.IsConnected)
                propertySetter.SetCtrlProperty(Of String)(Me._statusPanel, "Text", isr.Core.IIf(Of String)(Me._codeNetDevice.IsConnected, "Connected", "Disconnected"))

            Case IO.Ports.SerialPinChange.CtsChanged

                propertySetter.SetCtrlProperty(Of Boolean)(Me._ctsIndicator, "IsOn", Me._codeNetDevice.SerialPort.CtsHolding)

            Case IO.Ports.SerialPinChange.DsrChanged

                propertySetter.SetCtrlProperty(Of String)(Me._statusPanel, "Text", "DSR Changed")

            Case IO.Ports.SerialPinChange.Ring

                propertySetter.SetCtrlProperty(Of String)(Me._statusPanel, "Text", "Ring")

        End Select

    End Sub

    Private Sub _codeNetDevice_SettingsChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles _codeNetDevice.SettingsChanged

        Me.displayActualSettings()
        Me.DisplayRecommendedSetting()

    End Sub

    Private Sub _codeNetDevice_StartedReceiving(ByVal sender As Object, ByVal e As System.EventArgs) Handles _codeNetDevice.StartedReceiving
        ' turn on receive light
        propertySetter.SetCtrlProperty(Of Boolean)(Me._rxIndicator, "IsOn", True)
    End Sub

    Private Sub _codeNetDevice_StartedTransmitting(ByVal sender As Object, ByVal e As System.EventArgs) Handles _codeNetDevice.StartedTransmitting

        ' turn on send light
        Me._rxIndicator.IsOn = True

        Me._receiveTextBox.Clear()

        ' display the command as necessary
        If Me.IsInterpretCmdAsHex Then
            Me._sendTextBox.Text = CodeNetDevice.ConvertToHexPair(Me._codeNetDevice.LastMessageSent)
        Else
            Me._sendTextBox.Text = CodeNetDevice.ConvertToHexPairOrAscii(Me._codeNetDevice.LastMessageSent)
        End If


    End Sub

    ''' <summary>Updates the display.
    '''   This timer updates the status of the various
    '''   "lights" in the UI.
    ''' </summary>
    ''' <history>
    ''' </history>
    Private Sub updateTimer_Tick(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles _updateTimer.Tick

        Dim elapsedTimeMessage As String
        Dim packetStateMessage As String

        ' Open:
        Me._portOpenIndicator.IsOn = Me._codeNetDevice.SerialPort.IsOpen

        ' NAK:
        Me._notAckIndicator.IsOn = Me._codeNetDevice.ReceivedNotAcknowledged

        ' RTS:
        If Me._codeNetDevice.SerialPort.IsOpen Then

            Me._rtsIndicator.IsOn = Me._codeNetDevice.SerialPort.RtsEnable

            ' set elapsed time
            Dim elapsedTime As TimeSpan = DateTime.Now.Subtract(Me._codeNetDevice.TimePortOpened)
            elapsedTimeMessage = elapsedTime.ToString()
            ' String.Format(Globalization.CultureInfo.CurrentCulture, "{0:00}:{1:00}:{2:00}", Math.Floor(elapsedTime.TotalHours), Math.Floor(elapsedTime.TotalMinutes), Math.Floor(elapsedTime.TotalSeconds))
            packetStateMessage = Me._codeNetDevice.PacketStateName

            ' DSR
            Me._dsrIndicator.IsOn = Me._codeNetDevice.SerialPort.DsrHolding

            ' CTS:
            Me._ctsIndicator.IsOn = Me._codeNetDevice.SerialPort.CtsHolding

            ' CD:
            Me._cdIndicator.IsOn = Me._codeNetDevice.SerialPort.CDHolding

        Else

            Me._rtsIndicator.IsOn = False
            elapsedTimeMessage = String.Empty
            packetStateMessage = String.Empty

        End If

        ' DTR
        Me._dtrIndicator.IsOn = Me._codeNetDevice.SerialPort.DtrEnable

        ' display elapsed time
        Me._timePanel.Text = elapsedTimeMessage

        ' show packet state
        Me._statePanel.Text = packetStateMessage

    End Sub

#End Region

#Region " MESSAGES "

    ''' <summary>Adds message to the list box and data logger.
    ''' </summary>
    Private Function addMessage(ByVal broadcastLevel As Diagnostics.TraceEventType, ByVal format As String, ByVal ParamArray args() As Object) As String

        format = String.Format(Globalization.CultureInfo.CurrentCulture, "{0} {1:HHmmss.fff}: {2}", broadcastLevel.ToString.Substring(0, 2), DateTime.Now, format)
        format = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
        ' add the message to the list
        Me._messagesListBox.Items.Insert(0, format)

        If Me._messagesListBox.Items.Count > 100 Then
            Do While Me._messagesListBox.Items.Count > 75
                Me._messagesListBox.Items.RemoveAt(Me._messagesListBox.Items.Count - 1)
            Loop
            Me._messagesListBox.Items.Add(String.Format(Globalization.CultureInfo.CurrentCulture,
                                                   "Date {0}", DateTime.Now.ToShortDateString))
        End If
        Return format

    End Function

#End Region
End Class