<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> 
Partial Class SquareLed
#Region "Windows Form Designer generated code "
    <System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
        MyBase.New()
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing Then
            If Me.ClickEvent IsNot Nothing Then
                For Each d As [Delegate] In Me.ClickEvent.GetInvocationList
                    RemoveHandler Me.Click, CType(d, Global.System.EventHandler(Of Global.System.EventArgs))
                Next
            End If
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Private WithEvents indicatorLabel As System.Windows.Forms.Label
    Private WithEvents captionLabel As System.Windows.Forms.Label
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.captionLabel = New System.Windows.Forms.Label
        Me.indicatorLabel = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'captionLabel
        '
        Me.captionLabel.BackColor = System.Drawing.Color.Transparent
        Me.captionLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.captionLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.captionLabel.ForeColor = System.Drawing.SystemColors.ControlText
        Me.captionLabel.Location = New System.Drawing.Point(0, 0)
        Me.captionLabel.Name = "captionLabel"
        Me.captionLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.captionLabel.Size = New System.Drawing.Size(33, 13)
        Me.captionLabel.TabIndex = 0
        Me.captionLabel.Text = "TITLE"
        Me.captionLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'indicatorLabel
        '
        Me.indicatorLabel.BackColor = System.Drawing.Color.Green
        Me.indicatorLabel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.indicatorLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me.indicatorLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.indicatorLabel.ForeColor = System.Drawing.Color.Red
        Me.indicatorLabel.Location = New System.Drawing.Point(8, 16)
        Me.indicatorLabel.Name = "indicatorLabel"
        Me.indicatorLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.indicatorLabel.Size = New System.Drawing.Size(13, 13)
        Me.indicatorLabel.TabIndex = 1
        '
        'SquareLed
        '
        Me.Controls.Add(Me.indicatorLabel)
        Me.Controls.Add(Me.captionLabel)
        Me.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "SquareLed"
        Me.Size = New System.Drawing.Size(33, 33)
        Me.ResumeLayout(False)

    End Sub

#End Region
End Class